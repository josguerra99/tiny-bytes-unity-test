using UnityEngine;

namespace Bullet
{
    [RequireComponent(typeof(Rigidbody2D))]
    [RequireComponent(typeof(Collider2D))]
    public abstract class BulletBase : MonoBehaviour
    {
        [SerializeField] protected Rigidbody2D _rigidbody2D;
        [SerializeField] protected Collider2D _collider2D;

        /// <summary>
        /// Event raised when this bullet ends it life cycle
        /// (After a hit or a miss)
        /// </summary>
        public System.Action OnFinalize { get; set; }

        /// <summary>
        /// It will called after a bullet hits but it will not be reseted
        /// after the bullet finalizes (useful when creating effects or audio)
        /// </summary>
        public System.Action<BulletHitInfo> OnPersistentHit { get; set; }

        /// <summary>
        /// It will be called after a bullet hits any gameObject
        /// and it will be reseted after the bullet finalizes
        /// </summary>
        public System.Action<BulletHitInfo> OnHit { get; set; }

        public System.Action OnMiss { get; set; }

        /// <summary>
        /// How far does the object need to be, to be counted as a miss
        /// (Use the square of the value you actually want to use)
        /// </summary>

        protected readonly float _sqrDistanceToMiss = 2f * 2f;
        protected float _speed;
        protected float _damage;
        protected Vector2 _startPosition;
        protected Vector2 _direction;

        /// <summary>
        /// Initializes the bullet's data
        /// </summary>
        /// <param name="startPosition">Position where the bullet should start</param>
        /// <param name="direction">Direction of the shot</param>
        /// <param name="speed">Speed of the bullet</param>
        /// <param name="damage">Damage to do</param>
        public virtual void Initialize(Vector2 startPosition, Vector2 direction, float speed, float damage)
        {
            _speed = speed;
            _damage = damage;
            _direction = direction;
            _startPosition = startPosition;

            if (_rigidbody2D == null) _rigidbody2D = GetComponent<Rigidbody2D>();
            if (_collider2D == null) _collider2D = GetComponent<Collider2D>();

            _rigidbody2D.velocity = Vector2.zero;
            transform.position = _startPosition;
            _rigidbody2D.isKinematic = false;

            Shoot();
        }

        protected virtual void Shoot()
        {
            _rigidbody2D.AddForce(_direction * _speed, ForceMode2D.Impulse);
        }

        /// <summary>
        /// Raised when a bullet hits anything
        /// </summary>
        /// <param name="hitInfo">Information of the hit</param>
        public abstract void Hit(BulletHitInfo hitInfo);

        /// <summary>
        /// Manages the logic when a bullet miss (it didn't hit anything)
        /// </summary>
        public abstract void Miss();

        protected virtual void LateUpdate()
        {
            CheckForOutOfBoundsMiss();
        }

        /// <summary>
        /// Checks if the bullet is out of the play area then we sould count that
        /// as a miss
        /// </summary>
        protected virtual void CheckForOutOfBoundsMiss()
        {
            if (Time.frameCount % 20 == 0)
            {
                if (PlayArea.Instance.SqrDistanceToBounds(transform.position) >= _sqrDistanceToMiss)
                    Miss();
            }
        }

        protected virtual void OnCollisionEnter2D(Collision2D collision)
        {
            //Converts the collision into a BulletHitInfo struct and then it calls
            //HandleHit that is the method in charge of the logic of bullet hits
            var target = collision.collider.GetComponentInParent<TargetBase>();
            var hitInfo = new BulletHitInfo()
            {
                hitPosition = collision.contacts[0].point,
                hitNormal = collision.contacts[0].normal,
                target = target
            };

            Hit(hitInfo);
        }

        /// <summary>
        /// Called after the bullet hits or miss a target
        /// </summary>
        public abstract void FinalizeBullet();
    }
}