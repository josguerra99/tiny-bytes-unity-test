using System;
using UnityEngine;

namespace Bullet
{
    /// <summary>
    /// Its used when a bullet hits an objects
    /// It plays a particle effect in the hit position
    /// </summary>

    [RequireComponent(typeof(ParticleSystem))]
    public class BulletHitEffect : MonoBehaviour
    {
        /// <summary>
        /// Particle effect to play
        /// </summary>
        [SerializeField] private ParticleSystem _particle;

        [SerializeField] private Utilities.Audio.RandomAudioPlayer _audioPlayer;

        private BulletBase _bullet;

        private void Awake()
        {
            _bullet = GetComponentInParent<BulletBase>();
            _bullet.OnPersistentHit += HandleHit;
        }

        private void FinalizeParticle()
        {
            if (_bullet != null)
                transform.SetParent(_bullet.transform);
            else
                Destroy(gameObject);
        }

        /// <summary>
        /// Plays the effect
        /// </summary>
        /// <param name="info">Hit information</param>
        public void HandleHit(BulletHitInfo info)
        {
            transform.SetParent(null);
            transform.position = info.hitPosition;
            transform.forward = info.hitNormal;
            gameObject.SetActive(true);
            _particle.Play();
            _audioPlayer.Play();
        }

        private void OnParticleSystemStopped()
        {
            FinalizeParticle();
        }
    }
}