namespace Bullet
{
    /// <summary>
    /// Spawns bullets using a pool
    /// </summary>
    public class BulletSpawner : GameObjectPoolBase<BulletBase>
    {
        public BulletSpawner(BulletBase prefab) : base(prefab)
        {
        }

        protected override void DestroyFunction(BulletBase obj)
        {
            if (obj != null)
                obj.FinalizeBullet();
            base.DestroyFunction(obj);
        }
    }
}