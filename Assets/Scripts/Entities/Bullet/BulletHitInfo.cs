using UnityEngine;

/// <summary>
/// Information that is passed on every bullet hit
/// </summary>
public struct BulletHitInfo
{
    /// <summary>
    /// Target that this bullet hit (or null if it didn't hit any)
    /// </summary>

    public TargetBase target;

    /// <summary>
    /// Damage done to the target if it exists
    /// </summary>
    public float damageDone;

    /// <summary>
    /// Health reamaining in the target (if exists) after the bullets hits
    /// </summary>
    public float targetRemainingHealth;

    public Vector2 hitPosition;
    public Vector2 hitNormal;
}