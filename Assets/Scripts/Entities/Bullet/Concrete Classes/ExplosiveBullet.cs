using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Bullet
{
    public class ExplosiveBullet : BulletBase
    {
        private float radius = 20f;

        public override void FinalizeBullet()
        {
        }

        public override void Hit(BulletHitInfo hitInfo)
        {
            HandleObjectsInRadius(hitInfo);
        }

        public override void Miss()
        {
            OnMiss?.Invoke();
            FinalizeBullet();
        }

        private void HitTarget(BulletHitInfo info)
        {
        }

        private void HandleObjectsInRadius(BulletHitInfo info)
        {
            TargetBase lastTargetToHit = null;
            HashSet<TargetBase> targets = new HashSet<TargetBase>();

            var colliders = Physics2D.OverlapCircleAll(transform.position, radius);

            for (int i = 0; i < colliders.Length; i++)
            {
                var collider = colliders[i];
                var target = collider.GetComponentInParent<TargetBase>();

                if (target != null && !targets.Contains(target))
                {
                    lastTargetToHit = target;
                    targets.Add(target);

                    HitTarget(new BulletHitInfo()
                    {
                        hitPosition = target.transform.position,
                        target = target,
                        hitNormal = info.hitNormal,
                        damageDone = info.damageDone
                    });
                }
            }
            info.target = lastTargetToHit;

            if (targets.Count == 0)
            {
                Miss();
            }
            else
            {
                FinalizeBullet();
            }
        }
    }
}