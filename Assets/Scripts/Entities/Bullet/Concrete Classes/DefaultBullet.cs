using UnityEngine;

namespace Bullet
{
    public class DefaultBullet : BulletBase
    {
        [SerializeField] private TrailRenderer _trail;

        public override void Initialize(Vector2 startPosition, Vector2 direction, float speed, float damage)
        {
            if (_trail == null)
                _trail = GetComponentInChildren<TrailRenderer>();

            base.Initialize(startPosition, direction, speed, damage);

            if (_trail != null)
                _trail.Clear();
        }

        public override void Hit(BulletHitInfo hitInfo)
        {
            if (hitInfo.target)
                hitInfo.targetRemainingHealth = hitInfo.target.Health - _damage;

            OnHit?.Invoke(hitInfo);
            OnPersistentHit?.Invoke(hitInfo);

            if (hitInfo.target)
                hitInfo.target.TakeDamage(_damage);

            FinalizeBullet();
        }

        public override void Miss()
        {
            OnMiss?.Invoke();
            FinalizeBullet();
        }

        public override void FinalizeBullet()
        {
            _rigidbody2D.velocity = Vector2.zero;
            _rigidbody2D.isKinematic = true;
            gameObject.SetActive(false);
            OnFinalize?.Invoke();
            OnHit = null;
            OnMiss = null;
            OnFinalize = null;
        }
    }
}