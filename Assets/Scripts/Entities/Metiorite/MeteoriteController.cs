using UnityEngine;
using System.Collections;

namespace Meteorite
{
    public class MeteoriteController : TargetBase
    {
        [SerializeField] private float _maxHealth;
        [SerializeField] private Vector2 _speedRange;
        [SerializeField] private Vector2 _moveDirection = -Vector2.up;
        [SerializeField] private Vector2 _rotationSpeedRange;
        [SerializeField] private Vector3 _rotateAxis = Vector3.forward;
        [SerializeField] private Rigidbody2D _rigidbody2D;
        [SerializeField] private ParticleSystem _deathParticleVFX;
        [SerializeField] private Utilities.Audio.RandomAudioPlayer _audioPlayer;

        private float _deathDuration = 1f;

        /// <summary>
        /// Raised when this object has completed its use
        /// </summary>
        public System.Action OnDespawn { get; set; }

        /// <summary>
        /// Multiplies the speed by this value (this can be used
        /// on the game manager for making the game more difficult)
        /// </summary>
        public float SpeedMultiplier { get; set; } = 1f;

        public override float MaxHealth => _maxHealth;

        private float _rotationSpeed;
        private float _speed;
        private WaitForSeconds _deathDelay;
        private readonly float _sqrDistanceToDespawn = 5 * 5;

        private void Awake()
        {
            _deathDelay = new WaitForSeconds(_deathParticleVFX.main.duration);
        }

        public override void Initialize()
        {
            base.Initialize();

            transform.GetChild(0).gameObject.SetActive(true);
            _rigidbody2D.isKinematic = false;

            _rotationSpeed = Random.Range(_rotationSpeedRange.x, _rotationSpeedRange.y);
            _speed = Random.Range(_speedRange.x, _speedRange.y) * SpeedMultiplier;
            gameObject.SetActive(true);
        }

        /// <summary>
        /// Modifies the rigidbody's velocity
        /// (This must be called from FixedUpdate)
        /// </summary>
        protected void Move()
        {
            _rigidbody2D.velocity = _moveDirection * _speed;
        }

        /// <summary>
        /// Rotates the transform depending on the _rotationSpeed
        /// And Time.deltaTime (meaning that this has to be called from
        /// the Upate function)
        /// </summary>
        protected void RotateEffect()
        {
            transform.GetChild(0).Rotate(_rotateAxis, Time.deltaTime * _rotationSpeed, Space.Self);
        }

        private void Update()
        {
            RotateEffect();
        }

        private void FixedUpdate()
        {
            Move();
        }

        protected override void HandleDeath()
        {
            StartCoroutine(HandleDeathCoroutine());
        }

        private IEnumerator HandleDeathCoroutine()
        {
            _rigidbody2D.isKinematic = true;
            _audioPlayer.Play();
            transform.GetChild(0).gameObject.SetActive(false);
            _deathParticleVFX.Play();
            yield return _deathDelay;
            base.HandleDeath();
        }

        private void LateUpdate()
        {
            CheckIfOutOfBounds();
        }

        /// <summary>
        /// Checks if the game object is out of bounds, if it is then the metorite should
        /// be despawned
        /// </summary>
        private void CheckIfOutOfBounds()
        {
            if (Time.frameCount % 20 == 0)
            {
                if (PlayArea.Instance.SqrDistanceToBounds(transform.position) >= _sqrDistanceToDespawn)
                    base.HandleDeath();
            }
        }

        private void OnCollisionEnter2D(Collision2D collision)
        {
            var target = collision.gameObject.GetComponentInParent<TargetBase>();

            if (target != null && !(target is MeteoriteController))
            {
                TakeDamage(Health);
                target.TakeDamage(target.Health);
            }
        }
    }
}