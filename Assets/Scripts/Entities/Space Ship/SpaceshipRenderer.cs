using System;
using UnityEngine;

namespace Spaceship
{
    public class SpaceshipRenderer : MonoBehaviour
    {
        [SerializeField] protected SpaceShipController _spaceShip;
        [SerializeField] private float _maxHorizontalRotation = 40f;
        [SerializeField] private float _maxVerticalRotation = 40f;
        [SerializeField] private float _maxSpeed = 7f;
        [SerializeField] protected Renderer _emissionRenderer;
        [SerializeField] protected float _maxEmission = 4.3f;
        [SerializeField] protected Transform _turretContainer;
        [SerializeField] protected SpaceshipRendererDeathHandler _deathEffects;

        private Material _emissionMaterial;
        private float _sqrMaxSpeed;
        private Color _emissionColor;
        private readonly string _emissionTag = "_EmissionColor";

        protected virtual void Awake()
        {
            _emissionMaterial = _emissionRenderer.material;
            _sqrMaxSpeed = _maxSpeed * _maxSpeed;
            _emissionColor = _emissionMaterial.GetColor(_emissionTag);

            if (_spaceShip == null)
                _spaceShip = GetComponentInParent<SpaceShipController>();

            if (_spaceShip == null)
                Destroy(this);
        }

        private void HandleDeath()
        {
            _deathEffects.Play();
            _spaceShip.OnDeath -= HandleDeath;
        }

        protected virtual void Start()
        {
            _spaceShip.OnDeath += HandleDeath;

            SetTurretContainer();
        }

        /// <summary>
        /// Gets the turret controller from the spaceship
        /// and then it sets it inside the renderer's container
        /// </summary>
        protected virtual void SetTurretContainer()
        {
            _spaceShip.Turret.transform.SetParent(_turretContainer);
            _spaceShip.Turret.transform.localPosition = Vector3.zero;
            _spaceShip.Turret.transform.localRotation = Quaternion.identity;
        }

        private void Update()
        {
            HandleMove();
        }

        /// <summary>
        /// Changes the renderer based on the spaceships velocity
        /// </summary>
        private void HandleMove()
        {
            float percentageSpeedX = Mathf.Clamp(_spaceShip.Velocity.x / _maxSpeed, -1f, 1f);
            float percentageSpeedY = Mathf.Clamp(_spaceShip.Velocity.y / _maxSpeed, -1f, 1f);

            var targetRotation = Quaternion.Euler(percentageSpeedY * _maxVerticalRotation,
                                                  -percentageSpeedX * _maxHorizontalRotation,
                                                  0f);

            transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, Time.deltaTime * 20f);

            float magnitude = _spaceShip.Velocity.sqrMagnitude;
            float intensity = Mathf.Clamp01(magnitude / _sqrMaxSpeed);
            _emissionMaterial.SetColor(_emissionTag, _emissionColor * intensity * _maxEmission);
        }
    }
}