using UnityEngine;

namespace Spaceship
{
    public class SpaceshipDualTurretRenderer : SpaceshipRenderer
    {
        [SerializeField] protected Transform _secondaryTurretContainer;

        protected override void SetTurretContainer()
        {
            base.SetTurretContainer();

            if (_spaceShip is SpaceshipDualTurretController dual)
            {
                dual.SecondaryTurret.transform.SetParent(_secondaryTurretContainer);
                dual.SecondaryTurret.transform.localPosition = Vector3.zero;
                dual.SecondaryTurret.transform.localRotation = Quaternion.identity;
                Destroy(dual.SecondaryTurret.gameObject.GetComponent<Turret.TurretAudioBase>());
            }
        }
    }
}