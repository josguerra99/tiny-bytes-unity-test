using Turret;

namespace Spaceship
{
    public class SpaceshipDualTurretController : SpaceShipController
    {
        public TurretControllerBase SecondaryTurret { get; protected set; }

        public override void SetTurret(TurretCreatorAsset turretAsset, int turretLevel)
        {
            base.SetTurret(turretAsset, turretLevel);

            if (SecondaryTurret != null)
            {
                SecondaryTurret.OnShoot -= HandleTurretShot;
                Destroy(Turret.gameObject);
            }

            SecondaryTurret = CreateTurret(turretAsset, turretLevel);
            SecondaryTurret.OnShoot += HandleTurretShot;
        }

        public override void PressTurretTrigger()
        {
            base.PressTurretTrigger();
            SecondaryTurret.PressTrigger();
        }

        public override void ReleaseTurretTrigger()
        {
            base.ReleaseTurretTrigger();
            SecondaryTurret.ReleaseTheTrigger();
        }
    }
}