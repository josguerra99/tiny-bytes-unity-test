using UnityEngine;
using Utilities;
using Spaceship;

namespace Spaceship
{
    [CreateAssetMenu(menuName = "Game/Spaceship/DB", fileName = "DB_Spaceships")]
    public class SpaceshipDB : ScriptableDBBase<SpaceshipCreatorAsset>
    {
    }
}