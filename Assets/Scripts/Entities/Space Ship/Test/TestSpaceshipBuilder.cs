using UnityEngine;
using Turret;

namespace Spaceship
{
    public class TestSpaceshipBuilder : MonoBehaviour
    {
        public bool onStart = true;
        public SpaceshipCreatorAsset spaceship;
        public TurretCreatorAsset turret;
        public int spaceshipLevel;
        public int turretLevel;

        private void Start()
        {
            if (onStart)
                CreateSpaceship();
        }

        public SpaceShipController CreateSpaceship()
        {
            return CreateBuilder().Build();
        }

        public SpaceshipBuilder CreateBuilder()
        {
            return SpaceshipBuilder.
                        Start().
                        SetSpaceshipCreatorAsset(spaceship, spaceshipLevel).
                        SetTurretCreatorAsset(turret, turretLevel).
                        SetInput(SpaceshipBuilder.InputType.Player);
        }
    }
}