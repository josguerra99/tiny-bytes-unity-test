using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Spaceship
{
    [CustomEditor(typeof(SpaceshipCreatorAsset))]
    public class SpaceshipCreatorEditor : ContainerScriptableObjectEditor<SpaceshipAsset>
    {
        protected SpaceshipCreatorAsset _script;
        protected override ContainerScriptableObject<SpaceshipAsset> Script => _script;

        protected override void OnEnable()
        {
            base.OnEnable();
            _script = target as SpaceshipCreatorAsset;
        }
    }
}