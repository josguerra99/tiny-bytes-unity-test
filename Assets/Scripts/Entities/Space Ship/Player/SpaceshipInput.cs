using UnityEngine;

namespace Spaceship
{
    /// <summary>
    /// Gets the player's input and uses it to control an spaceship
    /// </summary>
    [RequireComponent(typeof(SpaceShipController))]
    public class SpaceshipInput : MonoBehaviour
    {
        [SerializeField] private SpaceShipController _spaceshipController;
        private Camera cam;
        [SerializeField] private bool useMobile = true;

        private void Reset()
        {
            _spaceshipController = GetComponent<SpaceShipController>();
        }

        private void Awake()
        {
            if (_spaceshipController == null)
                _spaceshipController = GetComponent<SpaceShipController>();

            cam = Camera.main;
        }

        private void Update()
        {
            if (!useMobile)
                HandleInputUpdate();
            else
                HandleMobileInput();
        }

        /// <summary>
        /// Gets the input and controls  the spaceship with it using
        /// the keyboard
        /// </summary>
        private void HandleInputUpdate()
        {
            float horizontalInput = Input.GetAxis("Horizontal");
            float verticalInput = Input.GetAxis("Vertical");

            bool shootInput = Input.GetKey(KeyCode.Space);

            Vector2 moveAmount = new Vector2(horizontalInput, verticalInput);

            if (moveAmount.sqrMagnitude >= 1f)
                moveAmount.Normalize();

            _spaceshipController.Move(moveAmount);

            if (shootInput)
                _spaceshipController.PressTurretTrigger();
            else
                _spaceshipController.ReleaseTurretTrigger();
        }

        /// <summary>
        /// Gets a touch position and moves the spaceship towards it
        /// </summary>
        private void HandleMobileInput()
        {
            _spaceshipController.PressTurretTrigger();
            _spaceshipController.ReleaseTurretTrigger();

            if (Input.GetMouseButton(0) || Input.touchCount > 0)
            {
                Vector2 inputPosition = Input.mousePosition;
                Vector2 playerPosition = cam.WorldToScreenPoint(_spaceshipController.transform.position);
                float distance = Vector2.Distance(inputPosition, playerPosition);

                Vector2 direction = (inputPosition - playerPosition).normalized;
                float distancePercentage = Mathf.Clamp01(distance / 50f);

                _spaceshipController.Move(direction * distancePercentage);
            }
            else
            {
                _spaceshipController.Move(Vector2.zero);
            }
        }
    }
}