using System;
using UnityEngine;
using Turret;
using Bullet;

namespace Spaceship
{
    /// <summary>
    /// Moves the spaceship around and handles shooting
    /// </summary>
    [RequireComponent(typeof(Rigidbody2D))]
    public class SpaceShipController : TargetBase
    {
        [SerializeField] private Rigidbody2D _rigidbody2D;
        [SerializeField] private SpaceshipAsset _asset;

        public virtual SpaceshipAsset Asset => _asset;
        public override float MaxHealth => _asset.Health;
        public virtual float Speed => Asset.MovementSpeed * 0.01f;
        public Rigidbody2D Rigidbody2D => _rigidbody2D;
        public TurretControllerBase Turret { get; protected set; }

        /// <summary>
        /// Raised when the spacehsip hits a target
        /// </summary>
        public Action<BulletHitInfo> OnBulletHit { get; set; }

        /// <summary>
        /// Current velocity of the spaceship
        /// </summary>
        public Vector2 Velocity { get; protected set; }

        private Vector2 _moveDirection;

        public virtual void Initialize(SpaceshipAsset asset)
        {
            if (!IsAssetCompatible(asset))
            {
                Debug.LogError("Asset (scriptable object) is not compatible");
                return;
            }
            _asset = asset;
            Initialize();
        }

        public override void Initialize()
        {
            if (_rigidbody2D == null)
                _rigidbody2D = GetComponent<Rigidbody2D>();

            base.Initialize();
        }

        private void Start()
        {
            if (PlayArea.Instance == null)
            {
                Debug.LogError("Play Area instance doesn't exists, please create one. This gameObject will be destroyed");
                Destroy(gameObject);
            }
        }

        /// <summary>
        /// Adds the turret to this spaceship
        /// </summary>
        /// <param name="turretAsset">Turret to add</param>
        public virtual void SetTurret(TurretCreatorAsset turretAsset, int turretLevel)
        {
            if (Turret != null)
            {
                Turret.OnShoot -= HandleTurretShot;
                Destroy(Turret.gameObject);
            }

            Turret = CreateTurret(turretAsset, turretLevel);
            Turret.OnShoot += HandleTurretShot;
        }

        /// <summary>
        /// Creates a turret from a TurretCreatorAsset
        /// </summary>
        /// <param name="turretAsset"></param>
        /// <param name="turretLevel"></param>
        /// <returns></returns>
        protected TurretControllerBase CreateTurret(TurretCreatorAsset turretAsset, int turretLevel)
        {
            var turret = turretAsset.CreateTurret(turretLevel);
            turret.transform.SetParent(transform);
            turret.transform.localPosition = Vector3.zero;
            turret.transform.localRotation = Quaternion.identity;
            turret.transform.localScale = Vector3.one;
            turret.gameObject.SetLayerRecursively(gameObject.layer);
            return turret;
        }

        /// <summary>
        /// When the turret shoots a bullet , it will start liseting for the bullet hit
        /// event, if the bulle hits a target then it will raise "OnBulletHit" event
        /// </summary>
        /// <param name="bullet">Bullet to listen</param>
        /// <param name="startPosition">Bullet start position</param>
        /// <param name="direction">Bullet direction</param>
        protected void HandleTurretShot(BulletBase bullet, Vector2 startPosition, Vector2 direction)
        {
            void handleBulletHit(BulletHitInfo info)
            {
                if (info.target != null)
                    OnBulletHit?.Invoke(info);

                bullet.OnHit -= handleBulletHit;
                bullet.OnMiss -= handleBulletMiss;
            }

            void handleBulletMiss()
            {
                bullet.OnHit -= handleBulletHit;
                bullet.OnMiss -= handleBulletMiss;
            }

            bullet.OnHit += handleBulletHit;
            bullet.OnMiss += handleBulletMiss;
        }

        /// <summary>
        /// Moves this transform on the desired direction
        /// </summary>
        /// <param name="direction">Direction (input) to move</param>
        public void Move(Vector2 direction)
        {
            this._moveDirection = direction;
            Debug.DrawRay(transform.position, direction, Color.cyan);
        }

        private void FixedUpdate()
        {
            MoveTowardsDirection();
        }

        /// <summary>
        /// Moves the spaceships towards the last move direction
        /// </summary>
        private void MoveTowardsDirection()
        {
            Vector2 lastPosition = transform.position;
            Vector2 position = transform.position;
            position += _moveDirection * Speed;
            position = PlayArea.Instance.ClosestPoint(position);

            if (PlayArea.Instance.IsInsidePlayArea(transform.position))
                _rigidbody2D.MovePosition(position);

            Velocity = (position - lastPosition) / Time.fixedDeltaTime;
        }

        public virtual void PressTurretTrigger()
        {
            Turret.PressTrigger();
        }

        public virtual void ReleaseTurretTrigger()
        {
            Turret.ReleaseTheTrigger();
        }

        protected override void HandleDeath()
        {
            base.HandleDeath();
            Destroy(gameObject);
        }

        /// <summary>
        /// Checks if the asset(data container) is compatible with
        /// the spaceship
        /// </summary>
        /// <param name="asset"></param>
        /// <returns></returns>
        protected virtual bool IsAssetCompatible(SpaceshipAsset asset) => true;
    }
}