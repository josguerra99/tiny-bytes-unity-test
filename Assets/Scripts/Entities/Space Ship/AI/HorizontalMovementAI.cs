using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Spaceship.AI
{
    public class HorizontalMovementAI : SpaceshipAIBehaviourBase
    {
        private Vector2 targetPosition;

        protected override void Start()
        {
            base.Start();
            targetPosition = PlayArea.Instance.Bounds.size;
            targetPosition.y = transform.position.y;
        }

        protected override void UpdateAIBehaviour()
        {
            base.UpdateAIBehaviour();
            Move();
        }

        protected virtual void Move()
        {
            float distance = Vector2.Distance(targetPosition, transform.position);

            if (distance <= 2f)
            {
                targetPosition.x *= -1f;
            }

            Vector2 position = transform.position;
            Vector2 direction = (targetPosition - position).normalized;
            _spaceship.Move(direction);
        }
    }
}