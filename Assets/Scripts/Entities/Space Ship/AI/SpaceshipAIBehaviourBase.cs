using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Spaceship.AI
{
    [RequireComponent(typeof(SpaceShipController))]
    public class SpaceshipAIBehaviourBase : MonoBehaviour
    {
        [SerializeField] protected SpaceShipController _spaceship;
        [SerializeField] protected SpaceshipAIAsset _aiAsset;

        private void Awake()
        {
            if (_spaceship == null)
                _spaceship = GetComponent<SpaceShipController>();
        }

        protected virtual void Start()
        {
            if (_spaceship.Asset is SpaceshipAIAsset ai)
            {
                _aiAsset = ai;
                _startShootingTime = Time.time;
            }
            else
            {
                Debug.LogError("Spaceship Asset (Scriptable Object) is not an AI asset, this object will be destroyed");
                Destroy(gameObject);
            }
        }

        protected virtual void Update()
        {
            UpdateAIBehaviour();
        }

        private float _lastShootTime = -Mathf.Infinity;
        private float _startShootingTime = 0;
        private float _lastHeatUpTime = -Mathf.Infinity;
        private bool _isHeatedUp = false;

        protected virtual void HandleTriggerPress()
        {
            //Checks if the turret is currently heated up
            //(Turret is cooling down)

            if (_isHeatedUp)
            {
                if (Time.time - _lastHeatUpTime <= _aiAsset.TurretCooldown)
                {
                    return;
                }
                else
                {
                    _isHeatedUp = false;
                    _startShootingTime = Time.time;
                }
            }

            //Check if the turret should be heated up
            if (Time.time - _startShootingTime >= _aiAsset.TurretTimeToHeatUp)
            {
                _isHeatedUp = true;
                _lastHeatUpTime = Time.time;
                return;
            }

            if (Time.time - _lastShootTime >= _aiAsset.TriggerSpeed)
            {
                _spaceship.PressTurretTrigger();
                _spaceship.ReleaseTurretTrigger();
                _lastShootTime = Time.time;
            }
        }

        protected virtual void UpdateAIBehaviour()
        {
            HandleTriggerPress();
        }
    }
}