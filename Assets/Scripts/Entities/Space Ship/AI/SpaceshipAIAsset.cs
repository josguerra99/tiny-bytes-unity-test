using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Spaceship.AI
{
    [CreateAssetMenu(fileName = "Spaceship AI", menuName = "Game/Spaceship/AI")]
    public class SpaceshipAIAsset : SpaceshipAsset
    {
        [SerializeField] protected float _triggerSpeed;

        [SerializeField] protected float _turretTimeToHeatUp;

        [SerializeField] protected float _turretCooldown;

        /// <summary>
        /// How fast can the AI shoot (if this value is faster than the turret
        /// fire rate, then it will be capped by it)
        /// </summary>
        public float TriggerSpeed => _triggerSpeed;

        /// <summary>
        /// How much time the turret will be used before having to cooldown
        /// </summary>
        public float TurretTimeToHeatUp => _turretTimeToHeatUp;

        /// <summary>
        /// How much time does it take to cooldown this turret
        /// </summary>
        public float TurretCooldown => _turretCooldown;
    }
}