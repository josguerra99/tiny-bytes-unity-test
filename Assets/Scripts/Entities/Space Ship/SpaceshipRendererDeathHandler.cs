using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Spaceship
{
    public class SpaceshipRendererDeathHandler : MonoBehaviour
    {
        [SerializeField] private Utilities.Audio.RandomAudioPlayer _audioPlayer;
        [SerializeField] private ParticleSystem _vfx;

        public void Play()
        {
            transform.SetParent(null);
            _vfx.Play();
            _audioPlayer.Play();
        }
    }
}