using UnityEngine;
using Turret;

namespace Spaceship
{
    public class SpaceshipBuilder
    {
        private InputType _inputType = InputType.AI_Horizontal;

        private int _spaceshipLevel = 0;
        private int _turretLevel = 0;
        private SpaceshipCreatorAsset _spaceshipCreatorAsset;
        private TurretCreatorAsset _turretCreatorAsset;

        public SpaceshipBuilder SetSpaceshipCreatorAsset(SpaceshipCreatorAsset creatorAsset, int level = 0)
        {
            _spaceshipLevel = level;
            _spaceshipCreatorAsset = creatorAsset;
            return this;
        }

        public SpaceshipBuilder SetTurretCreatorAsset(TurretCreatorAsset turretCreatorAsset, int level = 0)
        {
            _turretLevel = level;
            _turretCreatorAsset = turretCreatorAsset;
            return this;
        }

        /// <summary>
        /// Checks if the builder has enough data to create a spaceship
        /// </summary>
        protected bool HasEnoughData
        {
            get
            {
                if (_turretCreatorAsset == null || _spaceshipCreatorAsset == null)
                    return false;

                return true;
            }
        }

        public SpaceshipBuilder SetInput(InputType input)
        {
            _inputType = input;
            return this;
        }

        public SpaceShipController Build()
        {
            if (!HasEnoughData)
            {
                Debug.LogError("There is not enough data to create an spaceship");
                return null;
            }

            var (asset, controller, renderer) = _spaceshipCreatorAsset.CreateSpaceship(_spaceshipLevel);
            controller.name = asset.name;
            controller.Initialize(asset);
            controller.SetTurret(_turretCreatorAsset, _turretLevel);
            AddInputComponent(controller);
            renderer.gameObject.SetLayerRecursively(controller.gameObject.layer);
            return controller;
        }

        public static SpaceshipBuilder Start() => new SpaceshipBuilder();

        private void AddInputComponent(SpaceShipController controller)
        {
            switch (_inputType)
            {
                case InputType.None:
                    return;

                case InputType.Player:
                    controller.gameObject.AddComponent<SpaceshipInput>();
                    controller.gameObject.SetLayerRecursively(LayerMask.NameToLayer("Player"));
                    return;

                case InputType.AI_Horizontal:
                    controller.gameObject.AddComponent<AI.HorizontalMovementAI>();
                    controller.gameObject.SetLayerRecursively(LayerMask.NameToLayer("Enemy"));
                    return;
            }
        }

        public enum InputType
        {
            None,
            Player,
            AI_Horizontal
        };
    }
}