using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR

using UnityEditor;

#endif

namespace Spaceship

{
    /// <summary>
    /// Contains the required info to create a new spaceship instance (with multiple levels of upgrades),
    /// It also has a method for creating it
    /// </summary>
    [CreateAssetMenu(fileName = "Spaceship Creator", menuName = "Game/Spaceship/Creator")]
    public class SpaceshipCreatorAsset : ContainerScriptableObject<SpaceshipAsset>
    {
        [SerializeField] private string _name;
        [SerializeField] private SpaceShipController _controllerPrefab = new SpaceShipController();
        [SerializeField] private SpaceshipRenderer _rendererPrefab = new SpaceshipRenderer();

        public string Name => _name;

        /// <summary>
        /// Prefab of the renderer (skin) of the spaceship
        /// </summary>
        public SpaceshipRenderer RendererPrefab => _rendererPrefab;

        /// <summary>
        /// Prefab of the controller of the spaceship
        /// </summary>
        public SpaceShipController ControllerPrefab => _controllerPrefab;

        /// <summary>
        /// Creates an instance of the spaceship
        /// </summary>
        /// <param name="level">Level to instantiate</param>
        /// <returns>(SpaceshipAsset,SpaceshipController,SpaceshipRenderer)</returns>
        public (SpaceshipAsset, SpaceShipController, SpaceshipRenderer) CreateSpaceship(int level)
        {
            var asset = _items[level];
            var controller = Instantiate(_controllerPrefab);
            var renderer = Instantiate(_rendererPrefab, controller.transform);
            return (asset, controller, renderer);
        }
    }
}