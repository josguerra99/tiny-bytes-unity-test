using UnityEngine;

namespace Spaceship
{
    /// <summary>
    /// Contains the spaceship info, and it also creates it
    /// </summary>

    [CreateAssetMenu(fileName = "Spaceship", menuName = "Game/Spaceship/Default")]
    public class SpaceshipAsset : ScriptableObject
    {
        [SerializeField] private float _health;
        [SerializeField] private float _movementSpeed;

        public float Health => _health;
        public float MovementSpeed => _movementSpeed;
    }
}