using UnityEngine;
using Bullet;

namespace Turret
{
    /// <summary>
    /// Contains the required info to create a new turret instance with multiple levels of upgrades
    /// (Thats the reason for using ContainerScriptableObject as a base class),
    /// It also has a method for creating the TurretController (instantiate it)
    /// </summary>
    [CreateAssetMenu(fileName = "Turret Creator", menuName = "Game/Turret/Creator")]
    public class TurretCreatorAsset : ContainerScriptableObject<TurretAsset>
    {
        [SerializeField] private string _name;
        [SerializeField] private TurretControllerBase _controllerPrefab;
        [SerializeField] private BulletBase _bulletPrefab;
        [SerializeField] private GameObject _rendererPrefab;

        public string Name => _name;

        public GameObject RendererPrefab => _rendererPrefab;

        /// <summary>
        /// Creates a turret controller
        /// </summary>
        /// <returns>Turret instance</returns>
        public TurretControllerBase CreateTurret(int level)
        {
            var turret = Instantiate(_controllerPrefab);
            turret.Initialize(GetAt(level), _bulletPrefab);
            turret.gameObject.AddComponent<TurretAudioBase>();
            return turret;
        }
    }
}