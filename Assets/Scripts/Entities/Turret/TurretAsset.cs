using UnityEngine;
using Utilities.Audio;

namespace Turret
{
    /// <summary>
    /// Contains the turret data
    /// </summary>

    [CreateAssetMenu(fileName = "Turret", menuName = "Game/Turret/Data")]
    public class TurretAsset : ScriptableObject
    {
        [SerializeField] private float _fireRate;
        [SerializeField] private float _damage;
        [SerializeField] private float _bulletSpeed;
        [SerializeField] private ClipInfoAsset _shotAudio;

        public float FireRate => _fireRate;
        public float Damage => _damage;
        public float BulletSpeed => _bulletSpeed;

        public ClipInfoAsset ShotAudio => _shotAudio;
    }
}