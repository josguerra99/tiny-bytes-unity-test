using UnityEngine;
using Utilities;

namespace Turret
{
    [CreateAssetMenu(menuName = "Game/Turret/DB", fileName = "DB_Turret")]
    public class TurretsDB : ScriptableDBBase<TurretCreatorAsset>
    {
    }
}