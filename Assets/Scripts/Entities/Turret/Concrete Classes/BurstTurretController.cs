using Bullet;
using System.Collections;
using UnityEngine;

namespace Turret
{
    public class BurstTurretController : TurretControllerBase
    {
        public BurstTurretAsset BurstTurretAsset { get; protected set; }

        private WaitForSeconds _burstWaitForSeconds;

        private float _lastShootTime = -Mathf.Infinity;

        private float _timeBetweenShots = 0f;

        public override void Initialize(TurretAsset asset, BulletBase bullet)
        {
            BurstTurretAsset = asset as BurstTurretAsset;
            if (BurstTurretAsset == null)
            {
                Debug.LogError("A BurstTurretAsset is required");
                Destroy(this);
                return;
            }

            base.Initialize(asset, bullet);
            _burstWaitForSeconds = new WaitForSeconds(BurstTurretAsset.TimeBetweenBursts);
            _timeBetweenShots = BurstTurretAsset.TimeBetweenBursts * BurstTurretAsset.BurstAmount + Asset.FireRate;
        }

        public override void PressTrigger()
        {
            if (Time.time - _lastShootTime >= _timeBetweenShots)
            {
                StartCoroutine(BurstCoroutine());
            }
        }

        public override void ReleaseTheTrigger()
        {
        }

        private IEnumerator BurstCoroutine()
        {
            _lastShootTime = Time.time;

            for (int i = 0; i < BurstTurretAsset.BurstAmount; i++)
            {
                Shoot();
                yield return _burstWaitForSeconds;
            }
        }

        protected override void Shoot()
        {
            for (int i = 0; i < shootPoints.Length; i++)
            {
                var shootPoint = shootPoints[i];
                var bullet = BulletSpawner.Pool.Get();
                bullet.gameObject.SetLayerRecursively(gameObject.layer);
                bullet.Initialize(shootPoint.position, shootPoint.up, Asset.BulletSpeed, Asset.Damage);
                bullet.OnFinalize += () => { BulletSpawner.Pool.Release(bullet); };
                OnShoot?.Invoke(bullet, shootPoint.position, shootPoint.up);
            }
        }
    }
}