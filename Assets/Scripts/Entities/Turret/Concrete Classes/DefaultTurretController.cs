using UnityEngine;

namespace Turret
{
    public class DefaultTurretController : TurretControllerBase
    {
        protected float _lastShootTime = -Mathf.Infinity;

        public override void PressTrigger()
        {
            if (Time.time - _lastShootTime >= Asset.FireRate)
            {
                Shoot();
            }
        }

        public override void ReleaseTheTrigger()
        {
        }

        protected override void Shoot()
        {
            _lastShootTime = Time.time;

            for (int i = 0; i < shootPoints.Length; i++)
            {
                var shootPoint = shootPoints[i];
                var bullet = BulletSpawner.Pool.Get();
                bullet.gameObject.SetLayerRecursively(gameObject.layer);
                bullet.Initialize(shootPoint.position, shootPoint.up, Asset.BulletSpeed, Asset.Damage);
                bullet.OnFinalize += () => { BulletSpawner.Pool.Release(bullet); };
                OnShoot?.Invoke(bullet, shootPoint.position, shootPoint.up);
            }
        }
    }
}