using UnityEngine;

namespace Turret
{
    [CreateAssetMenu(fileName = "Turret Burst", menuName = "Game/Turret/Data Burst")]
    public class BurstTurretAsset : TurretAsset
    {
        [SerializeField] private int _burstAmount = 3;
        [SerializeField] private float _timeBetweenBursts = 0.75f;

        public float TimeBetweenBursts => _timeBetweenBursts;
        public int BurstAmount => _burstAmount;
    }
}