using UnityEngine;
using Bullet;

namespace Turret
{
    /// <summary>
    /// Base class for creating a turret
    /// </summary>
    public abstract class TurretControllerBase : MonoBehaviour
    {
        [SerializeField] protected Transform[] shootPoints;
        public TurretAsset Asset { get; protected set; }
        public BulletBase BulletPrefab { get; protected set; }
        public BulletSpawner BulletSpawner { get; protected set; }

        /// <summary>
        /// Event raised when this turret shoots a bullet
        /// </summary>
        public System.Action<BulletBase, Vector2, Vector2> OnShoot { get; set; }

        public virtual void Initialize(TurretAsset asset, BulletBase bullet)
        {
            this.Asset = asset;
            this.BulletPrefab = bullet;
            BulletSpawner = new BulletSpawner(bullet);
        }

        /// <summary>
        /// Presses turret trigger (it doesn't mean that it will shoot)
        /// This might check for things like fire rate or if this weapon is automatic
        /// or not, before actually shooting the bullet
        /// </summary>
        public abstract void PressTrigger();

        /// <summary>
        /// Is called when the trigger is not being pressed, its useful when creating
        /// a semi-auto turret or a charge weapon
        /// </summary>
        public abstract void ReleaseTheTrigger();

        /// <summary>
        /// Shoots the bullet
        /// </summary>
        protected abstract void Shoot();

        private void OnDestroy()
        {
            BulletSpawner.Dispose();
        }
    }
}