using UnityEditor;

namespace Turret
{
    [CustomEditor(typeof(TurretCreatorAsset))]
    public class TurretCreatorEditor : ContainerScriptableObjectEditor<TurretAsset>
    {
        private TurretCreatorAsset _script;
        protected override ContainerScriptableObject<TurretAsset> Script => _script;

        protected override void OnEnable()
        {
            base.OnEnable();
            _script = target as TurretCreatorAsset;
        }

        protected override void DrawAddButtonsSections()
        {
            DrawAddButton<TurretAsset>("Default Turret");
            DrawAddButton<BurstTurretAsset>("Burst Turret");
        }
    }
}