using Bullet;
using System;
using UnityEngine;

namespace Turret
{
    public class TurretAudioBase : MonoBehaviour
    {
        private Utilities.Audio.ClipInfoAsset clip;

        private void Awake()
        {
            var turret = GetComponent<TurretControllerBase>();
            turret.OnShoot += HandleShot;
            clip = turret.Asset.ShotAudio;
        }

        private void HandleShot(BulletBase arg1, Vector2 arg2, Vector2 arg3)
        {
            Utilities.Audio.AudioMngr.Player(Utilities.Audio.AudioMngr.Type.SFX).Play(clip);
        }
    }
}