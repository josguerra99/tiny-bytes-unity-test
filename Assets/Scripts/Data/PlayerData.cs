using System.Threading.Tasks;

namespace Data
{
    public class PlayerData
    {
        private static readonly PlayerData _instance = new PlayerData();

        public static PlayerData Instance => _instance;

        public static CurrencyManager SoftCurrency;
        public static CurrencyManager HardCurrency;
        public static InventoryManager Inventory;
        public static PlayerSelectionsManager Selections;
        public static GameScoresManager GameScores;

        public PlayerData()
        {
        }

        public static async Task Initialize()
        {
            await LoginManager.Autologin(new PlayFab.ClientModels.GetPlayerCombinedInfoRequestParams()
            {
            });

            var initializePlayer = await PlayfabAsync.ExecuteCloudFunction(
                PlayfabConsts.Function_InitializePlayerData, null);

            var playerData = await PlayfabAsync.GetPlayerCombinedInfo(new PlayFab.ClientModels.GetPlayerCombinedInfoRequestParams()
            {
                GetUserInventory = true,
                GetUserVirtualCurrency = true,
                GetUserReadOnlyData = true
            });

            var inventory = new PlayFab.ClientModels.GetUserInventoryResult()
            {
                Inventory = playerData.InfoResultPayload.UserInventory,
                VirtualCurrency = playerData.InfoResultPayload.UserVirtualCurrency
            };

            var userReadonlyData = playerData.InfoResultPayload.UserReadOnlyData;
            var catalog = await PlayfabAsync.GetCatalogItems(PlayfabConsts.CatalogId);

            SoftCurrency = new CurrencyManager(PlayfabConsts.SoftCoinsId, inventory.VirtualCurrency[PlayfabConsts.SoftCoinsId]);
            HardCurrency = new CurrencyManager(PlayfabConsts.HardCoinsId, inventory.VirtualCurrency[PlayfabConsts.HardCoinsId]);
            Inventory = new InventoryManager(catalog, inventory, SoftCurrency, HardCurrency);
            Selections = new PlayerSelectionsManager(userReadonlyData);
            GameScores = new GameScoresManager(SoftCurrency, LoginManager.CurrentProfile.PlayerId);
        }
    }
}