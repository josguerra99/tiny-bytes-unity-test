public class CurrencyManager
{
    public int Amount { get; private set; }

    /// <summary>
    /// Its called when there is any change to the currency amount
    /// </summary>
    public System.Action<int, int> OnAmountChange { get; set; }

    /// <summary>
    /// Current currency id
    /// </summary>
    public string CurrencyId { get; private set; }

    public CurrencyManager(string currencyId, int amount)
    {
        CurrencyId = currencyId;
        this.Amount = amount;
    }

    /// <summary>
    /// Modifies current local data
    /// (this should be called after a cloudscript function
    /// executes on playfab's server)
    /// </summary>
    /// <param name="newAmount"></param>
    public void ModifyDataInMemory(int newAmount)
    {
        var previous = Amount;
        Amount = newAmount;
        OnAmountChange?.Invoke(previous, Amount);
    }

    public void IncreaseAmountInMemory(int amount)
    {
        ModifyDataInMemory(Amount + amount);
    }

    public void DecreaseAmountInMemory(int amount)
    {
        ModifyDataInMemory(Amount - amount);
    }
}