using System.Threading.Tasks;

namespace Data
{
    public class GameScoresManager
    {
        private readonly CurrencyManager _softCurrency;

        private string _playFabId;

        public GameScoresManager(CurrencyManager softCurrency, string playFabId)
        {
            _playFabId = playFabId;
            _softCurrency = softCurrency;
        }

        public async Task<CompletionDataModel> FinishGame(int score, float time)
        {
            var result = await PlayfabAsync.ExecuteCloudFunction(PlayfabConsts.Function_FinishGame,
                PlayfabConsts.Parameters_FinishGame(score, time));

            var responseData = Helper.Deserialize<CompletionDataModel>(result.FunctionResult.ToString());
            _softCurrency.ModifyDataInMemory(responseData.softCoins + PlayerData.SoftCurrency.Amount);
            return responseData;
        }

        /// <summary>
        ///
        /// </summary>
        /// <returns></returns>
        public async Task<LeaderboardModel> GetScoreLeaderboard(int count)
        {
            var leaderboardTask = PlayfabAsync.GetLeaderboard(count);
            var leaderboardAroundPlayerTask = PlayfabAsync.GetLeaderboardAroundPlayer(count);
            var leaderboardResult = await leaderboardTask;
            var leaderboardAroundPlayerResult = await leaderboardAroundPlayerTask;

            var playerLeaderboardEntry = leaderboardResult.Leaderboard.Find(x => x.PlayFabId == _playFabId);

            if (playerLeaderboardEntry == null)
                playerLeaderboardEntry = leaderboardAroundPlayerResult.Leaderboard.Find(x => x.PlayFabId == _playFabId);

            return new LeaderboardModel()
            {
                mainLeaderboard = leaderboardResult.Leaderboard.ToArray(),
                playerEntry = playerLeaderboardEntry,
            };
        }

        public struct LeaderboardModel
        {
            public PlayFab.ClientModels.PlayerLeaderboardEntry[] mainLeaderboard;
            public PlayFab.ClientModels.PlayerLeaderboardEntry playerEntry;
        }

        public struct LeaderboardItem
        {
            public int place;
            public string playfabId;
            public string displayName;
            public int score;
        }

        public struct CompletionDataModel
        {
            public int softCoins;
            public int score;
            public float time;
        }
    }
}