using System.Collections.Generic;

namespace Data
{
    public class PlayfabConsts
    {
        public static readonly string CatalogId = "My Catalog";
        public static readonly string ScoreLeaderboardId = "High Score";

        public static readonly string SoftCoinsId = "SC";
        public static readonly string HardCoinsId = "HC";

        public static readonly string Function_InitializePlayerData = "InitializePlayerData";
        public static readonly string Function_FinishGame = "FinishGame";
        public static readonly string Function_PurchaseNextItemLevel = "PurchaseNextItemLevel";
        public static readonly string Function_SkipItemWait = "SkipItemWaitTime";
        public static readonly string Function_UnlockItemAfterWait = "UnlockItemAfterWait";
        public static readonly string Function_SelectItem = "SelectInventoryItem";

        public static readonly string TurretClass = "Turret";
        public static readonly string SpaceshipClass = "Spaceship";

        public static readonly string DefaultTurretId = "echo";
        public static readonly string DefaultSpaceshipId = "mirco_recon";

        public static string TurretSelectionKey => CurrentSelection(TurretClass);
        public static string SpaceshipSelectionKey => CurrentSelection(SpaceshipClass);

        public static string CurrentSelection(string itemClass) => $"Current.{itemClass}";

        public static Dictionary<string, object> Parameters_FinishGame(int score, float time)
        {
            return new Dictionary<string, object>()
        {
            {"score",score },
            {"time",time }
        };
        }

        public static Dictionary<string, object> Parameters_SkipItemWaitTime(string itemId, int level)
        {
            return new Dictionary<string, object>()
                {
                    {"itemId",itemId},
                    {"level",level}
                };
        }

        public static Dictionary<string, object> Parameters_PurchaseNextItemLevel(string itemId)
        {
            return new Dictionary<string, object>() { { "itemId", itemId } };
        }

        public static Dictionary<string, object> Parameters_UnlockItemAfterWait(string itemId, int level)
        {
            return Parameters_SkipItemWaitTime(itemId, level);
        }

        public static Dictionary<string, object> Parameters_SelectItem(string itemId, string itemClass)
        {
            return new Dictionary<string, object>()
        {
            {"itemId",itemId},
            {"itemClass",itemClass }
        };
        }
    }
}