using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using PlayFab.ClientModels;

namespace Data
{
    public class InventoryManager
    {
        /// <summary>
        /// Using this semaphore as a lock to avoid purchasing a skip if the
        /// item is already upgrading
        /// </summary>
        private static readonly SemaphoreSlim _semaphore = new SemaphoreSlim(1, 1);

        /// <summary>
        /// Raised when a item is purchased with soft coins (meaning that it has to wait to be unlocked)
        /// </summary>
        public System.Action<LevelableInventoryItem, int> OnCurrentLevelChange { get; set; }

        /// <summary>
        /// Raised when a hard coin purchase is made (skipping the wait time)
        /// or when the item is unlocked by waiting the defined time
        /// </summary>
        public System.Action<LevelableInventoryItem, int> OnCurrentUsableLevelChange { get; set; }

        /// <summary>
        /// Database for all of the items
        /// </summary>
        public readonly Dictionary<string, LevelableInventoryItem> Items = new Dictionary<string, LevelableInventoryItem>();

        private readonly CurrencyManager _softCurrency;
        private readonly CurrencyManager _hardCurrency;

        public InventoryManager(GetCatalogItemsResult catalog, GetUserInventoryResult inventory, CurrencyManager softCurrency, CurrencyManager hardCurrency)
        {
            _softCurrency = softCurrency;
            _hardCurrency = hardCurrency;
            InitializeItems(catalog, inventory);
        }

        /// <summary>
        /// Fills and initializes the Item's dictionary based on playfab's responses
        /// </summary>
        /// <param name="catalog">Catalog result</param>
        /// <param name="inventory">Inventory result</param>
        public void InitializeItems(GetCatalogItemsResult catalog, GetUserInventoryResult inventory)
        {
            Items.Clear();

            for (int i = 0; i < catalog.Catalog.Count; i++)
            {
                var item = new LevelableInventoryItem(catalog.Catalog[i], null, _softCurrency, _hardCurrency);
                Items.Add(item.ItemId, item);
            }

            for (int i = 0; i < inventory.Inventory.Count; i++)
            {
                var inventoryItem = inventory.Inventory[i];
                Items[inventoryItem.ItemId].SetInventoryItem(new InventoryItem(inventoryItem));
                var item = Items[inventoryItem.ItemId];

                if (item.HasUpgradePending)
                {
                    ScheduleTryToUnlock((float)item.RemainingTimeToUnlock, item.ItemId, item.CurrentItemLevel);
                }
            }
        }

        /// <summary>
        /// Purchase an item (or level of an item) using soft currency
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public async Task PurchaseItemNextLevel(LevelableInventoryItem item)
        {
            if (!item.CanPurchaseNextLevel)
                return;

            var cost = item.CostOfNextUpgrade;

            var result = await PlayfabAsync.ExecuteCloudFunction(
                    PlayfabConsts.Function_PurchaseNextItemLevel,
                    PlayfabConsts.Parameters_PurchaseNextItemLevel(item.ItemId));

            var newInventory = Helper.Deserialize<InventoryItem>(result.FunctionResult.ToString());
            item.SetInventoryItem(newInventory);

            _softCurrency.DecreaseAmountInMemory(cost);
            OnCurrentLevelChange?.Invoke(item, newInventory.customData.currentLevel);

            if (item.HasUpgradePending)
                ScheduleTryToUnlock((float)item.RemainingTimeToUnlock, item.ItemId, item.CurrentItemLevel);
        }

        /// <summary>
        /// Skips the wait time using hard currency
        /// </summary>
        /// <param name="item">Item to skip</param>
        /// <returns></returns>
        public async Task SkipItemWaitTime(LevelableInventoryItem item)
        {
            if (!item.CanSkipLevel)
                return;

            await _semaphore.WaitAsync();

            try
            {
                var cost = item.CostOfSkip;

                var result = await PlayfabAsync.ExecuteCloudFunction(
                        PlayfabConsts.Function_SkipItemWait,
                        PlayfabConsts.Parameters_SkipItemWaitTime(item.ItemId, item.CurrentItemLevel));

                var newInventory = Helper.Deserialize<InventoryItem>(result.FunctionResult.ToString());
                item.SetInventoryItem(newInventory);
                _hardCurrency.DecreaseAmountInMemory(cost);
                OnCurrentUsableLevelChange?.Invoke(item, newInventory.customData.currentUsableLevel);
            }
            finally
            {
                _semaphore.Release();
            }
        }

        /// <summary>
        /// Tries to unlock the item (should be called after the defined time has passed)
        /// </summary>
        /// <param name="item"></param>
        /// <param name="level">Level to check</param>
        /// <returns></returns>
        public async Task TryToUnlock(LevelableInventoryItem item, int level)
        {
            if (!item.HasUpgradePending || item.CurrentItemLevel != level)
                return;

            await _semaphore.WaitAsync();

            try
            {
                var result = await PlayfabAsync.ExecuteCloudFunction(
                    PlayfabConsts.Function_UnlockItemAfterWait,
                    PlayfabConsts.Parameters_UnlockItemAfterWait(item.ItemId, level));

                var newInventory = Helper.Deserialize<InventoryItem>(result.FunctionResult.ToString());
                item.SetInventoryItem(newInventory);
                OnCurrentUsableLevelChange?.Invoke(item, newInventory.customData.currentUsableLevel);
            }
            finally
            {
                _semaphore.Release();
            }
        }

        private void ScheduleTryToUnlock(float time, string id, int level)
        {
            CoroutineExecuter.Instance.StartCoroutine(ScheduleTryToUnlockTaskCoroutine(time, id, level));
        }

        private IEnumerator ScheduleTryToUnlockTaskCoroutine(float time, string id, int level)
        {
            if (time < 0)
                time = 0f;

            yield return new UnityEngine.WaitForSecondsRealtime(time + 0.1f);
            var task = TryToUnlock(Items[id], level);
            yield return new UnityEngine.WaitUntil(() => task.IsCompleted);
        }
    }
}