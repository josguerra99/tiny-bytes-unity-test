using System.Collections.Generic;
using System.Threading.Tasks;

namespace Data
{
    public class PlayerSelectionsManager
    {
        public System.Action<string> OnTurretChange { get; set; }
        public System.Action<string> OnSpaceshipChange { get; set; }

        public string CurrentTurretId { get; private set; } = PlayfabConsts.DefaultTurretId;
        public string CurrentSpaceshipId { get; private set; } = PlayfabConsts.DefaultSpaceshipId;

        /// <summary>
        /// Creates a player selections object
        /// </summary>
        /// <param name="userReadonlyData">Requires the user readonly data for the current player</param>
        public PlayerSelectionsManager(Dictionary<string, PlayFab.ClientModels.UserDataRecord> userReadonlyData)
        {
            CurrentTurretId = userReadonlyData[PlayfabConsts.TurretSelectionKey].Value;
            CurrentSpaceshipId = userReadonlyData[PlayfabConsts.SpaceshipSelectionKey].Value;
        }

        private async Task<string> SelectItem(string id, string itemClass)
        {
            var parameters = PlayfabConsts.Parameters_SelectItem(id, itemClass);
            try
            {
                var result = await PlayfabAsync.ExecuteCloudFunction(PlayfabConsts.Function_SelectItem, parameters);
                return result.FunctionResult.ToString();
            }
            catch (System.Exception ex)
            {
                UnityEngine.Debug.Log(ex);

                return "";
            }
        }

        public async Task<string> SelectTurret(string id)
        {
            await SelectItem(id, PlayfabConsts.TurretClass);
            CurrentTurretId = id;
            OnTurretChange?.Invoke(CurrentTurretId);
            return id;
        }

        public async Task<string> SetSpaceship(string id)
        {
            await SelectItem(id, PlayfabConsts.SpaceshipClass);
            CurrentSpaceshipId = id;
            OnSpaceshipChange?.Invoke(CurrentSpaceshipId);
            return id;
        }

        public void SetTurretInMemory(string id)
        {
            CurrentTurretId = id;
            OnTurretChange?.Invoke(CurrentTurretId);
        }

        public void SetSpaceshipInMemory(string id)
        {
            CurrentSpaceshipId = id;
            OnSpaceshipChange?.Invoke(CurrentSpaceshipId);
        }
    }
}