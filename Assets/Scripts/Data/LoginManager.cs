using PlayFab.ClientModels;
using UnityEngine;
using System.Threading.Tasks;
using Data;

public class LoginManager
{
    public static PlayerProfileModel CurrentProfile { get; protected set; } = null;

    public static async Task<LoginResult> Autologin(GetPlayerCombinedInfoRequestParams infoToRequest)
    {
#if UNITY_ANDROID
        infoToRequest.GetPlayerProfile = true;
        var result = await AndroidLogin(infoToRequest);
        CurrentProfile = result.InfoResultPayload.PlayerProfile;
        return result;
#else

#endif
    }

    public static async Task<LoginResult> AndroidLogin(GetPlayerCombinedInfoRequestParams infoToRequest)
    {
        var result = new LoginWithAndroidDeviceIDRequest
        {
            AndroidDeviceId = SystemInfo.deviceUniqueIdentifier.ToString(),
            InfoRequestParameters = infoToRequest,
            CreateAccount = true
        };

        return await PlayfabAsync.LoginWithAndroidDeviceID(result);
    }

    public static async Task<UpdateUserTitleDisplayNameResult> UpdateUserDisplayName(string displayName)
    {
        var result = await PlayfabAsync.UpdateUserTitleDisplayName(displayName);
        CurrentProfile.DisplayName = displayName;
        return result;
    }
}