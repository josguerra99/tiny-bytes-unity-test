using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadingScreenUI : MonoBehaviour, SceneChanger.ISwitchSceneAnimations
{
    [SerializeField] private Animator _anim;
    private int showHash = Animator.StringToHash("Show");
    private int hideHash = Animator.StringToHash("Hide");

    public void In(Action callback)
    {
        _anim.SetTrigger(showHash);
        this.WaitHalfSecond(() =>
        {
            callback?.Invoke();
        });
    }

    public void Out(Action callback)
    {
        _anim.SetTrigger(hideHash);

        this.WaitHalfSecond(() =>
        {
            callback?.Invoke();
        });
    }

    private void Start()
    {
        SceneChanger.Animation = this;
    }
}