using RadioactiveGames.Common.Math;
using UnityEngine;
using UnityEngine.UI;

public class UpgradeBarUI : MonoBehaviour
{
    [SerializeField] private Image currentFill;
    [SerializeField] private Image upgradeFill;

    private bool isInitialized = false;
    private FillManager current;
    private FillManager upgrade;
    private float currentBaseValue;


    private void Initialize()
    {

        if (!isInitialized)
        {
            current = new FillManager() { image = currentFill };
            upgrade = new FillManager() { image = upgradeFill };
            isInitialized = true;
        }
    }



    public void SetValues(float current, float upgrade, float max)
    {
        Initialize();
        currentBaseValue = current;
        this.current.SetMaxValue(max);
        this.current.SetValue(current);
        this.upgrade.SetMaxValue(max);
        this.upgrade.SetValue(upgrade);
    }
    public void MakeCurrentMatchUpgrade(float percentage)
    {
        Initialize();
        float value = Mathf.Lerp(currentBaseValue, upgrade.value, percentage);
        current.SetValue(value);
    }

    public void Update()
    {
        if (!isInitialized)
            return;

        current.UI_Update();
        upgrade.UI_Update();
    }

    [SerializeField]
    public struct FillManager
    {
        public Image image;
        public float value;
        public float max;

        public void SetMaxValue(float max)
        {
            this.max = max;
        }

        public void SetValue(float valueTarget)
        {
            this.value = valueTarget;
        }

        public void SkipAnimation()
        {
            image.fillAmount = value;
        }

        public void UI_Update()
        {
            float target = value / max;
            image.fillAmount = target;

          
        }
    }
}