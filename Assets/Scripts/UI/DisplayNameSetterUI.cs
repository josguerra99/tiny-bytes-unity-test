using System;
using UnityEngine;
using RadioactiveGames.UI.Animation;
using RadioactiveGames.UI;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

public class DisplayNameSetterUI : MonoBehaviour, Initializer.IDisplayNameSetter
{
    [SerializeField] private UIAnimator _anim;
    [SerializeField] private UIAnimator _titleScreenAnim;
    [SerializeField] private Initializer _initializer;
    [SerializeField] private TMPro.TMP_InputField _nameField;
    [SerializeField] private ButtonWithLoader _btn;

    private void Awake()
    {
        _initializer.DisplayNameSetter = this;
    }

    private Action OnNameSetTmp { get; set; }

    public void SetDisplayName(Action callback)
    {
        OnNameSetTmp = callback;
        _anim.Show(() =>
        {
            _nameField.ActivateInputField();
        });

        _titleScreenAnim.Hide();
        _btn.onClick.AddListener(Btn_HandleClick);
        _nameField.onSubmit.AddListener(val => _btn.onClick.Invoke());
    }

    public async void Btn_HandleClick()
    {
        if (_btn.IsLoading)
            return;

        if (!IsValidName(_nameField.text))
        {
            _nameField.text = "";
            _nameField.DeactivateInputField();
            _nameField.ActivateInputField();
            return;
        }

        _btn.SetTask(SetName());
        await _btn.Execute();
        _btn.SetTask(null);
    }

    private async Task SetName()
    {
        var name = _nameField.text;

        if (!IsValidName(name))
            return;
        await LoginManager.UpdateUserDisplayName(name);
        _anim.Hide();
        OnNameSetTmp?.Invoke();
    }

    private readonly Regex regex = new Regex(@"^[a-zA-Z][a-zA-Z0-9]{2,20}$");

    private bool IsValidName(string value)
    {
        return regex.IsMatch(value);
    }
}