using System;
using UnityEngine;

public class HUDManager : MonoBehaviour
{
    [SerializeField] private GameManager _gameManager;
    [SerializeField] private TMPro.TextMeshProUGUI _timerTxt;
    [SerializeField] private TMPro.TextMeshProUGUI _softCoinsTxt;

    private void Awake()
    {
        _gameManager.OnSetup += HandleSetup;
        _gameManager.OnStart += HandleStart;
    }

    private void Update()
    {
        if (!_gameManager.IsPlaying)
            return;

        if (Time.frameCount % 30 == 0)
            UI_UpdateTimer();
    }

    private void UI_UpdateTimer()
    {
        var timeSpan = TimeSpan.FromSeconds(_gameManager.ElapsedTime);

        _softCoinsTxt.SetText(_gameManager.Score.ToString("00"));
        _timerTxt.SetText(timeSpan.ToString(@"mm\:ss"));
    }

    private void HandleStart()
    {
        _gameManager.OnStart -= HandleStart;
    }

    private void HandleSetup()
    {
        _timerTxt.text = _softCoinsTxt.text = "";
        _gameManager.OnSetup -= HandleSetup;
    }
}