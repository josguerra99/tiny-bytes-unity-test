using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RadioactiveGames.UI.Animation;
using Data;

public class LeaderboardUI : MonoBehaviour
{
    [SerializeField] private Transform _container;
    [SerializeField] private UIAnimator _loadingAnimator;
    [SerializeField] private LeaderboardItemUI _leaderboardEntryUIPrefab;

    /// <summary>
    /// A prefab to be shown between the main leaderboard (top 5 scores) and the player
    /// when the player is not on the main leaderboard
    /// </summary>
    [SerializeField] private GameObject _ellipsisUIPrefab;

    public async void ShowLeaderboard()
    {
        _loadingAnimator.Show();

        var leaderboard = await PlayerData.GameScores.GetScoreLeaderboard(5);

        _container.DestroyAllChildren();

        for (int i = 0; i < leaderboard.mainLeaderboard.Length; i++)
        {
            var entry = leaderboard.mainLeaderboard[i];
            var ui = _container.CreatePrefab(_leaderboardEntryUIPrefab);
            ui.Setup(entry);
        }

        if (leaderboard.playerEntry.Position >= leaderboard.mainLeaderboard.Length)
        {
            var entry = leaderboard.playerEntry;

            if (leaderboard.playerEntry.Position >= leaderboard.mainLeaderboard.Length + 1)
                _container.CreateChild(_ellipsisUIPrefab);

            var ui = _container.CreatePrefab(_leaderboardEntryUIPrefab);
            ui.Setup(entry);
        }

        _loadingAnimator.Hide();
    }
}