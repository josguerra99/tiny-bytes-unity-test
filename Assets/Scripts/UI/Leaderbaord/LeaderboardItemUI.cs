using UnityEngine;
using UnityEngine.UI;

public class LeaderboardItemUI : MonoBehaviour
{
    [SerializeField] private TMPro.TextMeshProUGUI _placeTxt;
    [SerializeField] private TMPro.TextMeshProUGUI _nameTxt;
    [SerializeField] private TMPro.TextMeshProUGUI _scoreTxt;

    [SerializeField] private Graphic _placeBackgroundGraphic;
    [SerializeField] private Graphic _backgroundGraphic;

    [SerializeField] private Color _goldColor;
    [SerializeField] private Color _silverColor;
    [SerializeField] private Color _bronzeColor;
    [SerializeField] private Color _placeDefaultColor;
    [SerializeField] private Color _backgroundDefaultColor;
    [SerializeField] private Color _backgroundPlayerColor;

    public void Setup(PlayFab.ClientModels.PlayerLeaderboardEntry leaderboardEntry)
    {
        _placeTxt.SetText((leaderboardEntry.Position + 1).ToString());
        _nameTxt.SetText(leaderboardEntry.DisplayName ?? leaderboardEntry.PlayFabId.ToString());
        _scoreTxt.SetText(leaderboardEntry.StatValue.ToString());

        _backgroundGraphic.color = leaderboardEntry.PlayFabId == LoginManager.CurrentProfile.PlayerId ?
            _backgroundPlayerColor :
            _backgroundDefaultColor;

        var placeColor = _placeDefaultColor;

        if (leaderboardEntry.Position == 0)
            placeColor = _goldColor;
        else if (leaderboardEntry.Position == 1)
            placeColor = _silverColor;
        else if (leaderboardEntry.Position == 2)
            placeColor = _bronzeColor;

        _placeBackgroundGraphic.color = placeColor;
    }
}