using System.Collections.Generic;
using UnityEngine;
using TMPro;
using RadioactiveGames.UI.Animation;
using RadioactiveGames.UI;
using Data;
using System;
using RadioactiveGames.UI.Menu;

/// <summary>
/// An interface for inventory items (levelable items)
/// that allows selection of the item, purchase upgrades,
/// and also skip wait times
/// </summary>
public abstract class InventoryItemsUIBase : MonoApp
{
    [SerializeField] private MenuBase _menu;

    //Animators for the different states of the interface

    [SerializeField] private UIAnimator _timerAnim;
    [SerializeField] private UIAnimator _lockedAnim;
    [SerializeField] private UIAnimator _unlockedAnim;
    [SerializeField] private UIAnimator _upgradeAnim;
    [SerializeField] private UIAnimator _skipAnim;
    [SerializeField] private UIAnimator _maxLevelAnim;
    [SerializeField] private UIAnimator _selectedVisualizerAnim;
    [SerializeField] private UIAnimator _selectedAnim;

    [SerializeField] private TextMeshProUGUI _titleTxt;
    [SerializeField] private TextMeshProUGUI _softCoinsTxt;
    [SerializeField] private TextMeshProUGUI _hardCoinsTxt;
    [SerializeField] private TextMeshProUGUI _softCoinsUpgradeTxt;
    [SerializeField] private TextMeshProUGUI _timerTxt;

    [SerializeField] private ButtonWithLoader _purchaseBtn;
    [SerializeField] private ButtonWithLoader _upgradeBtn;
    [SerializeField] private ButtonWithLoader _skipBtn;
    [SerializeField] private ButtonWithAnimator _selectBtn;

    [SerializeField] private Item3DRendererBase _renderer;

    protected UI_State CurrentUIState { get; set; }

    /// <summary>
    /// The item that is currently being viewed
    /// (this is different from the selected item)
    /// </summary>
    protected LevelableInventoryItem CurrentItem { get; set; }

    /// <summary>
    /// Index of the current item (for IdsToShow list)
    /// </summary>
    protected int _currentShownIndex = -1;

    /// <summary>
    /// Id of the first item that it will be shown on the interface
    /// </summary>
    protected abstract string InitialItemId { get; }

    /// <summary>
    /// Let the script know how an item is named,
    /// (for example it can use the name on PlayFab
    /// or use a localized name from somewhere else)
    /// </summary>
    /// <param name="id">Item id</param>
    /// <returns>Name of the item</returns>
    protected abstract string GetTitleOfItem(string id);

    /// <summary>
    /// Checks if the player has this item selected as
    /// their current active item for this category
    /// </summary>
    /// <param name="itemId"></param>
    /// <returns></returns>
    protected abstract bool IsSelected(string itemId);

    /// <summary>
    /// Gets the 3D model of an item, this 3D model will be
    /// passed to the Item3DRendererBase "_renderer"
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    protected abstract GameObject GetPrefabOfItem(string id);

    /// <summary>
    /// Items that can be viewed by this ui
    /// </summary>
    protected abstract List<string> IdsToShow { get; }

    public override void Initialize()
    {
        base.Initialize();

        UI_SetStateNone();

        //Add events listeners
        PlayerData.Inventory.OnCurrentLevelChange += HandleCurrentLevelChange;
        PlayerData.Inventory.OnCurrentUsableLevelChange += HandleCurrentUsableLevelChange;
        PlayerData.SoftCurrency.OnAmountChange += HandleMoneyChange;
        PlayerData.HardCurrency.OnAmountChange += HandleMoneyChange;

        //Add events to the buttons
        _purchaseBtn.onClick.AddListener(HandlePurchaseBtnClick);
        _upgradeBtn.onClick.AddListener(HandleUpgradeBtnClick);
        _skipBtn.onClick.AddListener(HandleSkipBtnClick);
        _selectBtn.onClick.AddListener(HandleSelectClick);

        _menu.OnShowStart += UI_ShowRender;

        ShowItem(InitialItemId);

        SwipeManager.Instance.OnSwipeRight += HandleSwipeRight;
        SwipeManager.Instance.OnSwipeLeft += HandleSwipeLeft;
    }

    public override void OnDestroyIfInitialized()
    {
        base.OnDestroyIfInitialized();
        PlayerData.Inventory.OnCurrentLevelChange -= HandleCurrentLevelChange;
        PlayerData.Inventory.OnCurrentUsableLevelChange -= HandleCurrentUsableLevelChange;
        PlayerData.SoftCurrency.OnAmountChange -= HandleMoneyChange;
        PlayerData.HardCurrency.OnAmountChange -= HandleMoneyChange;

        SwipeManager.Instance.OnSwipeRight -= HandleSwipeRight;
        SwipeManager.Instance.OnSwipeLeft -= HandleSwipeLeft;
    }

    /// <summary>
    /// Sets the current item and updates the interface
    /// so the item can be visualized
    /// </summary>
    /// <param name="id">Item to show</param>
    protected virtual void ShowItem(string id)
    {
        CurrentItem = PlayerData.Inventory.Items[id];

        UI_InitializeForCurrentItem();
        UI_UpdateSelected();

        UI_ShowRender();
        _currentShownIndex = IdsToShow.FindIndex(x => x == id);
    }

    private string _lastRenderId;

    /// <summary>
    /// Shows the CurrentItem on the _renderer
    /// </summary>
    protected virtual void UI_ShowRender()
    {
        if (!_menu.IsShowing)
            return;

        //raised when the gameobject is shown
        void callback(string id)
        {
            if (id == _lastRenderId)
            {
                if (IsSelected(CurrentItem.ItemId))
                {
                    _selectedVisualizerAnim.StartAnimation(true);
                }
            }
        }

        if (!IsSelected(CurrentItem.ItemId))
            _selectedVisualizerAnim.StartAnimation(false);

        _renderer.ShowItem(GetPrefabOfItem(CurrentItem.ItemId), callback, out _lastRenderId);
    }

    public virtual void Update()
    {
        if (!IsInitialized || !_menu.IsShowing)
            return;

        if (Input.GetKeyDown(KeyCode.A))
            Previous();

        if (Input.GetKeyDown(KeyCode.D))
            Next();

        if (Time.frameCount % 30 == 0 && CurrentUIState == UI_State.Purchased_WaitingForUpgrade)
            UI_UpdateTimerText();
    }

    /// <summary>
    /// Syncs the interface state with the current item state.
    /// For example, it will show the timer and the skip time button if the current item is
    /// currently waiting for an upgrade.
    /// </summary>
    public virtual void UI_InitializeForCurrentItem()
    {
        //Update text values
        _softCoinsTxt.SetText(CurrentItem.CostOfNextUpgrade.ToString());
        _softCoinsUpgradeTxt.SetText(CurrentItem.CostOfNextUpgrade.ToString());
        _hardCoinsTxt.SetText(CurrentItem.CostOfSkip.ToString());
        _titleTxt.SetText(GetTitleOfItem(CurrentItem.ItemId));

        UI_UpdateButtonInteractableStatus();

        if (CurrentItem.CurrentItemLevel == -1)
        {
            UI_SetStateNotPurchased();
            return;
        }
        if (CurrentItem.HasUpgradePending)
        {
            UI_SetStateWaiting();
            return;
        }

        if (CurrentItem.HasReachedMaxLevel)
        {
            UI_SetStateMaxLevel();
            return;
        }

        UI_SetStateUnlock();
    }

    /// <summary>
    /// If the current item is selected, it will change the interface
    /// to reflect that
    /// </summary>
    /// <param name="alsoChangeVisualizer">Also updates the visualizer (an icon on top of the 3D model)</param>
    protected virtual void UI_UpdateSelected(bool alsoChangeVisualizer = false)
    {
        var value = IsSelected(CurrentItem.ItemId);
        _selectedAnim.StartAnimation(value);

        if (alsoChangeVisualizer && value)
            _selectedVisualizerAnim.StartAnimation(value);
    }

    /// <summary>
    /// Updates the interactable status of the buttons
    /// (for example the purchase button wont be interactable
    /// if the player has not enough money to purchase the upgrade)
    /// </summary>
    protected virtual void UI_UpdateButtonInteractableStatus()
    {
        _purchaseBtn.interactable = CurrentItem.CanPurchaseNextLevel;
        _upgradeBtn.interactable = CurrentItem.CanPurchaseNextLevel;
        _skipBtn.interactable = CurrentItem.CanSkipLevel;
    }

    /// <summary>
    /// Updates the upgrade timer with the remaining time
    /// </summary>
    protected virtual void UI_UpdateTimerText()
    {
        var secondsRemaining = CurrentItem.RemainingTimeToUnlock;
        if (secondsRemaining < 0)
            secondsRemaining = 0;

        var timeSpan = TimeSpan.FromSeconds(secondsRemaining);
        _timerTxt.SetText(timeSpan.ToString(@"mm\:ss"));
    }

    #region UI_STATES

    protected virtual void UI_SetStateNone()
    {
        _timerAnim.Hide();
        _lockedAnim.Hide();
        _unlockedAnim.Hide();
        _upgradeAnim.Hide();
        _skipAnim.Hide();
        _maxLevelAnim.Hide();
        CurrentUIState = UI_State.None;
    }

    protected virtual void UI_SetStateNotPurchased()
    {
        _timerAnim.Hide();
        _lockedAnim.Show();
        _unlockedAnim.Hide();
        CurrentUIState = UI_State.Not_Purchased;
    }

    protected virtual void UI_SetStateMaxLevel()
    {
        _timerAnim.Hide();
        _lockedAnim.Hide();
        _unlockedAnim.Show();
        _upgradeAnim.Hide();
        _skipAnim.Hide();
        _maxLevelAnim.Show();
        CurrentUIState = UI_State.Purchased_MaxLevel;
    }

    protected virtual void UI_SetStateWaiting()
    {
        _timerAnim.Show();
        _lockedAnim.Hide();
        _unlockedAnim.Show();
        _upgradeAnim.Hide();
        _skipAnim.Show();
        _maxLevelAnim.Hide();
        UI_UpdateTimerText();
        CurrentUIState = UI_State.Purchased_WaitingForUpgrade;
    }

    protected virtual void UI_SetStateUnlock()
    {
        _timerAnim.Hide();
        _lockedAnim.Hide();
        _unlockedAnim.Show();
        _upgradeAnim.Show();
        _skipAnim.Hide();
        _maxLevelAnim.Hide();
        CurrentUIState = UI_State.Purchased_UnlockNextLevel;
    }

    #endregion UI_STATES

    /// <summary>
    /// Show the next item on the list
    /// </summary>
    public virtual void Next()
    {
        var index = RadioactiveGames.Common.ListExtensions.FullCircular(_currentShownIndex + 1, IdsToShow.Count);
        ShowItem(IdsToShow[index]);
    }

    /// <summary>
    /// Show the previous item on the list
    /// </summary>
    public virtual void Previous()
    {
        var index = RadioactiveGames.Common.ListExtensions.FullCircular(_currentShownIndex - 1, IdsToShow.Count);
        ShowItem(IdsToShow[index]);
    }

    #region BUTTON EVENTS

    /// <summary>
    /// Handles the selection button click
    /// (this must set the player's selection
    /// as the current viewing item)
    /// </summary>
    protected abstract void HandleSelectClick();

    private async void HandleUpgradeBtnClick()
    {
        if (_upgradeBtn.IsLoading || CurrentItem == null || !CurrentItem.CanPurchaseNextLevel)
            return;

        _upgradeBtn.SetTask(PlayerData.Inventory.PurchaseItemNextLevel(CurrentItem));
        await _upgradeBtn.Execute();
        _upgradeBtn.SetTask(null);
    }

    private async void HandlePurchaseBtnClick()
    {
        if (_purchaseBtn.IsLoading || CurrentItem == null || !CurrentItem.CanPurchaseNextLevel)
            return;

        _purchaseBtn.SetTask(PlayerData.Inventory.PurchaseItemNextLevel(CurrentItem));
        await _purchaseBtn.Execute();
        _purchaseBtn.SetTask(null);
    }

    private async void HandleSkipBtnClick()
    {
        if (_skipBtn.IsLoading || CurrentItem == null || !CurrentItem.CanSkipLevel)
            return;

        _skipBtn.SetTask(PlayerData.Inventory.SkipItemWaitTime(CurrentItem));
        await _skipBtn.Execute();
        _skipBtn.SetTask(null);
    }

    #endregion BUTTON EVENTS

    #region ITEM CHANGE EVENTS

    private void HandleCurrentUsableLevelChange(LevelableInventoryItem item, int level)
    {
        if (item != CurrentItem)
            return;

        UI_InitializeForCurrentItem();
    }

    private void HandleCurrentLevelChange(LevelableInventoryItem item, int level)
    {
        if (item != CurrentItem)
            return;

        UI_InitializeForCurrentItem();
    }

    private void HandleMoneyChange(int arg1, int arg2)
    {
        UI_UpdateButtonInteractableStatus();
    }

    #endregion ITEM CHANGE EVENTS

    #region INPUT CALLBACKS

    private void HandleSwipeLeft(Vector2 arg1, Vector2 arg2)
    {
        if (_menu.IsShowing)
            Previous();
    }

    private void HandleSwipeRight(Vector2 arg1, Vector2 arg2)
    {
        if (_menu.IsShowing)

            Next();
    }

    #endregion INPUT CALLBACKS

    /// <summary>
    /// Represents the current state of the interface
    /// </summary>
    public enum UI_State
    {
        None,
        Not_Purchased,
        Purchased_MaxLevel,
        Purchased_UnlockNextLevel,
        Purchased_WaitingForUpgrade
    };
}