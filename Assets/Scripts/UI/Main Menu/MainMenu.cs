using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using RadioactiveGames.UI;
using RadioactiveGames.UI.Menu;
using RadioactiveGames.UI.Animation;
using System;
using Data;

public class MainMenu : MonoApp
{
    [SerializeField] private MenuBase _menu;
    [SerializeField] private Item3DRendererBase _spaceshipRenderer;
    [SerializeField] private Item3DRendererBase _turretRenderer;
    [SerializeField] private Button _playBtn;
    [SerializeField] private UIAnimator _spaceshipAnim;
    [SerializeField] private UIAnimator _turretAnim;
    [SerializeField] private TMPro.TextMeshProUGUI _currentViewingTitleTxt;

    private CurrentViewingItem _viewingItem = CurrentViewingItem.Spacehip;

    private enum CurrentViewingItem
    {
        Spacehip,
        Turret
    }

    public override void Awake()
    {
        _playBtn.onClick.AddListener(HandlePlayBtn);
        _menu.OnShowStart += HandleMenuShow;

        base.Awake();
    }

    private void Start()
    {
        SwipeManager.Instance.OnSwipeRight += HandleRightSwipe;
        SwipeManager.Instance.OnSwipeLeft += HandleLeftSwipe;

        _currentViewingTitleTxt.SetText("Spaceship");
        _spaceshipAnim.Show();
        _turretAnim.Hide();
        ShowCurrentViewingItem();
    }

    public override void OnDestroy()
    {
        SwipeManager.Instance.OnSwipeRight -= HandleRightSwipe;
        SwipeManager.Instance.OnSwipeLeft -= HandleLeftSwipe;
    }

    private void HandleRightSwipe(Vector2 arg1, Vector2 arg2)
    {
        if (!_menu.IsShowing)
            return;
        SwitchShowingItem();
    }

    private void HandleLeftSwipe(Vector2 arg1, Vector2 arg2)
    {
        if (!_menu.IsShowing)
            return;

        SwitchShowingItem();
    }

    public void SwitchShowingItem()
    {
        if (_viewingItem == CurrentViewingItem.Spacehip)
        {
            _currentViewingTitleTxt.SetText("Turret");

            _viewingItem = CurrentViewingItem.Turret;
            _spaceshipAnim.Hide();
            _turretAnim.Show();
        }
        else
        {
            _currentViewingTitleTxt.SetText("Spaceship");

            _viewingItem = CurrentViewingItem.Spacehip;
            _spaceshipAnim.Show();
            _turretAnim.Hide();
        }

        ShowCurrentViewingItem();
    }

    private void HandlePlayBtn()
    {
        SceneChanger.LoadSceneWithLoadingScreen("Game");
    }

    public override void Initialize()
    {
        base.Initialize();
        if (_menu.IsShowing)
        {
            HandleMenuShow();
        }
    }

    private void HandleMenuShow()
    {
        if (PlayerData.Selections == null)
            return;

        ShowCurrentViewingItem();

        this.ExecuteNextFrame(async () =>
        {
            var turretSelection = PlayerData.Selections.SelectTurret(PlayerData.Selections.CurrentTurretId);
            var spaceshipSelection = PlayerData.Selections.SetSpaceship(PlayerData.Selections.CurrentSpaceshipId);
            await turretSelection;
            await spaceshipSelection;
        });
    }

    private void ShowCurrentViewingItem()
    {
        if (PlayerData.Selections == null)
            return;

        switch (_viewingItem)
        {
            case CurrentViewingItem.Spacehip:

                var spaceship = Spaceship.SpaceshipDB.Instance.GetItem(PlayerData.Selections.CurrentSpaceshipId);
                _turretRenderer.Hide(null, out var turretRenderId);
                _spaceshipRenderer.ShowItem(spaceship.RendererPrefab.gameObject, null, out var renderId);

                return;

            case CurrentViewingItem.Turret:

                var turret = Turret.TurretsDB.Instance.GetItem(PlayerData.Selections.CurrentTurretId);
                _spaceshipRenderer.Hide(null, out var turretRenderId1);
                _turretRenderer.ShowItem(turret.RendererPrefab, null, out var renderId1);
                return;

            default:
                break;
        }
    }
}