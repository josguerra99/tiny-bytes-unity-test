using System.Collections.Generic;
using UnityEngine;
using Data;
using Turret;

public class CurrentTurretUI : InventoryItemsUIBase
{
    [SerializeField] private UpgradeBarUI _levelBarUI;
    [SerializeField] private UpgradeBarUI _damageBarUI;
    [SerializeField] private UpgradeBarUI _fireRateUI;

    protected override string InitialItemId => PlayerData.Selections.CurrentTurretId;

    private readonly List<string> _idsToShow = new List<string>();
    protected override List<string> IdsToShow => _idsToShow;

    public override void Awake()
    {
        base.Awake();
        for (int i = 0; i < TurretsDB.Instance.List.Count; i++)
        {
            _idsToShow.Add(TurretsDB.Instance.List[i].Id);
        }
    }

    public override void Initialize()
    {
        base.Initialize();
        PlayerData.Selections.OnTurretChange += HandleTurretChange;
    }

    public override void OnDestroyIfInitialized()
    {
        base.OnDestroyIfInitialized();
        PlayerData.Selections.OnTurretChange -= HandleTurretChange;
    }

    private void HandleTurretChange(string id)
    {
        if (id != CurrentItem.ItemId)
            ShowItem(id);
        else
            UI_UpdateSelected(true);
    }

    #region UI FUNCTIONS

    public override void UI_InitializeForCurrentItem()
    {
        base.UI_InitializeForCurrentItem();
        UI_InitializeStats();
    }

    protected override void UI_UpdateTimerText()
    {
        base.UI_UpdateTimerText();
        UI_UpdateUpgardeStatsValues((float)CurrentItem.TimeSinceLastUnlock / CurrentItem.TimeToUnlock);
    }

    private void UI_InitializeStats()
    {
        var currentLevel = CurrentItem.CurrentUsableItemLevel;
        var upgradeLevel = CurrentItem.HasUpgradePending ?
            CurrentItem.CurrentItemLevel : CurrentItem.LevelToPurchase;

        var turret = TurretsDB.Instance.GetItem(CurrentItem.ItemId);

        var dataCurrent =
            currentLevel == -1 ? null :
            turret.GetAt(currentLevel);

        var dataUpgrade =
            upgradeLevel == -1 ? null :
            turret.GetAt(upgradeLevel);

        _levelBarUI.SetValues(
          currentLevel + 1,
          upgradeLevel + 1,
          CurrentItem.MaxLevel + 1
          );

        _damageBarUI.SetValues(
            dataCurrent ? dataCurrent.Damage : 0,
            dataUpgrade ? dataUpgrade.Damage : 0,
            200f
            );

        _fireRateUI.SetValues(
            dataCurrent ? dataCurrent.FireRate : 0,
            dataUpgrade ? dataUpgrade.FireRate : 0,
            500f
            );
    }

    private void UI_UpdateUpgardeStatsValues(float t)
    {
        _levelBarUI.MakeCurrentMatchUpgrade(t);
        _damageBarUI.MakeCurrentMatchUpgrade(t);
        _fireRateUI.MakeCurrentMatchUpgrade(t);
    }

    protected override void HandleSelectClick()
    {
        if (CurrentItem.CurrentUsableItemLevel != -1)
            PlayerData.Selections.SetTurretInMemory(CurrentItem.ItemId);
    }

    protected override string GetTitleOfItem(string id) => TurretsDB.Instance.GetItem(id).Name;

    protected override bool IsSelected(string id) => PlayerData.Selections.CurrentTurretId == id;

    protected override GameObject GetPrefabOfItem(string id) => TurretsDB.Instance.GetItem(id).RendererPrefab;

    protected override void ShowItem(string id)
    {
        base.ShowItem(id);
    }

    #endregion UI FUNCTIONS
}