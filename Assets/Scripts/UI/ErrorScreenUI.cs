using UnityEngine;
using UnityEngine.UI;
using RadioactiveGames.UI.Animation;

public class ErrorScreenUI : MonoBehaviour, Initializer.IHandleError
{
    [SerializeField] private Initializer initializer;
    [SerializeField] private TMPro.TextMeshProUGUI _errorTxt;
    [SerializeField] private UIAnimator _anim;
    [SerializeField] private Button _retryBtn;

    private void Awake()
    {
        initializer.ErrorHandler = this;
        _retryBtn.onClick.AddListener(Retry);
    }

    public void Retry()
    {
        initializer.Awake();
        App.Initialize();
        _anim.Hide();
    }

    public void SetError(string message)
    {
        _errorTxt.text = message;
        _anim.Show();
    }
}