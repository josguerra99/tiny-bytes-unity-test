using UnityEngine;
using Data;

public class ShowCoinsUI : MonoApp
{
    [SerializeField] private TMPro.TextMeshProUGUI _text;
    [SerializeField] private string _id = "SC";
    private CurrencyManager _currencyManager;

    private void Reset()
    {
        _text = GetComponent<TMPro.TextMeshProUGUI>();
    }

    public override void Initialize()
    {
        base.Initialize();

        _currencyManager = GetCurrencyManager(_id);

        if (_currencyManager == null)
            _currencyManager = PlayerData.SoftCurrency;

        _currencyManager.OnAmountChange += HandleAmountChange;
        HandleAmountChange(0, _currencyManager.Amount);
    }

    private CurrencyManager GetCurrencyManager(string id)
    {
        if (PlayerData.SoftCurrency.CurrencyId == id)
            return PlayerData.SoftCurrency;

        if (PlayerData.HardCurrency.CurrencyId == id)
            return PlayerData.HardCurrency;

        return null;
    }

    public override void OnDestroyIfInitialized()
    {
        base.OnDestroyIfInitialized();
        _currencyManager.OnAmountChange -= HandleAmountChange;
    }

    private void HandleAmountChange(int arg1, int arg2)
    {
        _text.text = arg2.ToString("00");
    }
}