using System.Collections.Generic;
using UnityEngine;
using Data;
using Spaceship;

public class CurrentSpaceshipUI : InventoryItemsUIBase
{
    [SerializeField] private UpgradeBarUI _levelBarUI;
    [SerializeField] private UpgradeBarUI _hpBarUI;
    [SerializeField] private UpgradeBarUI _speedBarUI;

    protected override string InitialItemId => PlayerData.Selections.CurrentSpaceshipId;

    private readonly List<string> _idsToShow = new List<string>();
    protected override List<string> IdsToShow => _idsToShow;

    public override void Awake()
    {
        base.Awake();
        for (int i = 0; i < SpaceshipDB.Instance.List.Count; i++)
        {
            _idsToShow.Add(SpaceshipDB.Instance.List[i].Id);
        }
    }

    public override void Initialize()
    {
        base.Initialize();
        PlayerData.Selections.OnSpaceshipChange += HandleSpaceshipChange;
    }

    public override void OnDestroyIfInitialized()
    {
        base.OnDestroyIfInitialized();
        PlayerData.Selections.OnSpaceshipChange -= HandleSpaceshipChange;
    }

    private void HandleSpaceshipChange(string id)
    {
        if (id != CurrentItem.ItemId)
            ShowItem(id);
        else
            UI_UpdateSelected(true);
    }

    #region UI FUNCTIONS

    public override void UI_InitializeForCurrentItem()
    {
        base.UI_InitializeForCurrentItem();
        UI_InitializeStats();
    }

    protected override void UI_UpdateTimerText()
    {
        base.UI_UpdateTimerText();
        UI_UpdateUpgardeStatsValues((float)CurrentItem.TimeSinceLastUnlock / CurrentItem.TimeToUnlock);
    }

    private void UI_InitializeStats()
    {
        var currentLevel = CurrentItem.CurrentUsableItemLevel;
        var upgradeLevel = CurrentItem.HasUpgradePending ?
            CurrentItem.CurrentItemLevel : CurrentItem.LevelToPurchase;

        var spaceship = SpaceshipDB.Instance.GetItem(CurrentItem.ItemId);

        var dataCurrent =
            currentLevel == -1 ? null :
            spaceship.GetAt(currentLevel);

        var dataUpgrade =
            upgradeLevel == -1 ? null :
            spaceship.GetAt(upgradeLevel);

        _levelBarUI.SetValues(
          currentLevel + 1,
          upgradeLevel + 1,
          CurrentItem.MaxLevel + 1
          );

        _speedBarUI.SetValues(
            dataCurrent ? dataCurrent.MovementSpeed : 0,
            dataUpgrade ? dataUpgrade.MovementSpeed : 0,
            25f
            );

        _hpBarUI.SetValues(
            dataCurrent ? dataCurrent.Health : 0,
            dataUpgrade ? dataUpgrade.Health : 0,
            500f
            );
    }

    private void UI_UpdateUpgardeStatsValues(float t)
    {
        _levelBarUI.MakeCurrentMatchUpgrade(t);
        _speedBarUI.MakeCurrentMatchUpgrade(t);
        _hpBarUI.MakeCurrentMatchUpgrade(t);
    }

    protected override void HandleSelectClick()
    {
        if (CurrentItem.CurrentUsableItemLevel != -1)
            PlayerData.Selections.SetSpaceshipInMemory(CurrentItem.ItemId);
    }

    protected override string GetTitleOfItem(string id) => SpaceshipDB.Instance.GetItem(id).Name;

    protected override bool IsSelected(string id) => PlayerData.Selections.CurrentSpaceshipId == id;

    protected override GameObject GetPrefabOfItem(string id) => SpaceshipDB.Instance.GetItem(id).RendererPrefab.gameObject;

    #endregion UI FUNCTIONS
}