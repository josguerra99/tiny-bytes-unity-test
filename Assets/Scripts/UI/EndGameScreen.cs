using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Data.GameScoresManager;

public class EndGameScreen : MonoBehaviour
{
    [SerializeField] private GameManager _gameManager;
    [SerializeField] private Animator _anim;
    [SerializeField] private TMPro.TextMeshProUGUI _scoreTxt;
    [SerializeField] private TMPro.TextMeshProUGUI _softCurrencyTxt;
    [SerializeField] private UnityEngine.UI.Button _continueBtn;

    private int _showGameoverHash = Animator.StringToHash("Show Game Over");
    private int _showEndHash = Animator.StringToHash("Show End");

    private void Awake()
    {
        _gameManager.OnFinish += HandleFinish;
        _gameManager.OnProcessEnd += HandleProcessEnd;
    }

    private void Start()
    {
        gameObject.SetActive(false);
    }

    private void HandleFinish(CompletionDataModel obj)
    {
        gameObject.SetActive(true);

        _anim.SetTrigger(_showGameoverHash);
    }

    private void HandleProcessEnd(CompletionDataModel obj)
    {
        _scoreTxt.text = obj.score.ToString();
        _softCurrencyTxt.text = $"(+{obj.softCoins} SC)";
        _anim.SetTrigger(_showEndHash);
        _continueBtn.onClick.AddListener(() =>
        {
            SceneChanger.LoadSceneWithLoadingScreen("Menu");
        });
    }
}