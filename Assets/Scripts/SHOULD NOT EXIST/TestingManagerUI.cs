using UnityEngine;
using RadioactiveGames.UI;

public class TestingManagerUI : MonoBehaviour
{
    [SerializeField] private ButtonWithLoader addSoftCurrencyBtn;
    [SerializeField] private ButtonWithLoader addHardCurrencyBtn;
    [SerializeField] private ButtonWithLoader resetInventoryBtn;
    [SerializeField] private TMPro.TMP_InputField softCurrencyField;
    [SerializeField] private TMPro.TMP_InputField hardCurrencyField;

    private void Awake()
    {
        addSoftCurrencyBtn.onClick.AddListener(HandleSoftCurrencyClick);
        addHardCurrencyBtn.onClick.AddListener(HandleHardCurrencyClick);
        resetInventoryBtn.onClick.AddListener(HandleResetInventoryClick);
    }

    private async void HandleResetInventoryClick()
    {
        if (resetInventoryBtn.IsLoading)
            return;

        resetInventoryBtn.SetTask(Data.TestingManager.ResetInventory());
        await resetInventoryBtn.Execute();
        resetInventoryBtn.SetTask(null);
    }

    private async void HandleHardCurrencyClick()
    {
        int.TryParse(hardCurrencyField.text, out int amount);

        if (addHardCurrencyBtn.IsLoading)
            return;

        addHardCurrencyBtn.SetTask(Data.TestingManager.AddHardCurrency(amount));
        await addHardCurrencyBtn.Execute();
        addHardCurrencyBtn.SetTask(null);
    }

    private async void HandleSoftCurrencyClick()
    {
        int.TryParse(softCurrencyField.text, out int amount);

        if (addSoftCurrencyBtn.IsLoading)
            return;

        addSoftCurrencyBtn.SetTask(Data.TestingManager.AddSoftCurrency(amount));
        await addSoftCurrencyBtn.Execute();
        addSoftCurrencyBtn.SetTask(null);
    }
}