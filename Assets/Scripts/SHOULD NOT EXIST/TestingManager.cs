using System.Collections.Generic;
using System.Threading.Tasks;

namespace Data
{
    public class TestingManager
    {
        public static readonly string Function_ResetInventory = "ResetInventory";
        public static readonly string Function_AddCurrency = "AddCurrency";

        public static Dictionary<string, object> Parameters_AddCurrency(string currencyId, int amount)
        {
            return new Dictionary<string, object>()
                {
                    {"currencyId",currencyId},
                    {"amount",amount}
                };
        }

        public static Dictionary<string, object> Parameters_ResetInventory()
        {
            return new Dictionary<string, object>()
            {
            };
        }

        public static async Task AddSoftCurrency(int amount)
        {
            await PlayfabAsync.ExecuteCloudFunction(
                Function_AddCurrency,
                Parameters_AddCurrency(PlayfabConsts.SoftCoinsId, amount)
                );
            PlayerData.SoftCurrency.IncreaseAmountInMemory(amount);
        }

        public static async Task AddHardCurrency(int amount)
        {
            await PlayfabAsync.ExecuteCloudFunction(
                Function_AddCurrency,
                Parameters_AddCurrency(PlayfabConsts.HardCoinsId, amount)
                );
            PlayerData.HardCurrency.IncreaseAmountInMemory(amount);
        }

        public static async Task ResetInventory()
        {
            var result = await PlayfabAsync.ExecuteCloudFunction(
                 Function_ResetInventory,
                 Parameters_ResetInventory()
                 );
            UnityEngine.Debug.Log(result.FunctionResult);

            await PlayerData.Initialize();
            SceneChanger.LoadSceneWithLoadingScreen("Initializer");
        }
    }
}