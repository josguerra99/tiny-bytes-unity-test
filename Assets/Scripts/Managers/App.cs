using UnityEngine;
using Data;

public class App
{
    private static System.Action OnInitialize { get; set; }
    public static System.Action<System.Exception> OnError { get; set; }

    private static bool _isInitialized = false;

    /// <summary>
    /// Prefab that contains the managers that this game requires
    /// </summary>
    private static GameObject _appPrefabInstance;

    [RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.AfterSceneLoad)]
    public static async void Initialize()
    {
        try
        {
            _isInitialized = false;

            QualitySettings.vSyncCount = 0;
            Application.targetFrameRate = 60;

            if (_appPrefabInstance == null)
            {
                _appPrefabInstance = Object.Instantiate(Resources.Load("App")) as GameObject;
                Object.DontDestroyOnLoad(_appPrefabInstance);
            }

            await PlayerData.Initialize();

            _isInitialized = true;
            OnInitialize?.Invoke();
        }
        catch (System.Exception exception)
        {
            OnError?.Invoke(exception);
            throw exception;
        }
        finally
        {
            OnError = null;
            OnInitialize = null;
        }
    }

    public static void SubscribeToInitialization(System.Action callback)
    {
        if (_isInitialized)
        {
            callback?.Invoke();
            return;
        }

        void action()
        {
            callback?.Invoke();
            OnInitialize -= action;
        }

        OnInitialize += action;
    }

    public static void UnsubscribeFromInitialization(System.Action callback)
    {
        OnInitialize -= callback;
    }

    public static void SubscribeToError(System.Action<System.Exception> callback)
    {
        void action(System.Exception exception)
        {
            callback?.Invoke(exception);
            OnError -= action;
        }

        OnError += action;
    }

    public static void UnsubscribeFromError(System.Action<System.Exception> callback)
    {
        OnError -= callback;
    }
}