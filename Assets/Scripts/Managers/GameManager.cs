using System.Collections.Generic;
using UnityEngine;
using Spaceship;
using System.Threading.Tasks;
using Data;
using Turret;
using Meteorite;
using static Data.GameScoresManager;

public class GameManager : MonoApp
{
    /// <summary>
    /// Event raised when the game is setup, but hasn't started yet
    /// </summary>
    public System.Action OnSetup { get; set; }

    /// <summary>
    /// Event raised when the game starts
    /// </summary>
    public System.Action OnStart { get; set; }

    /// <summary>
    /// Event raised when the games finishes, but before process
    /// the data (execute the playfab's cloud function to update
    /// the score and grant the player money)
    /// </summary>
    public System.Action<CompletionDataModel> OnFinish { get; set; }

    /// <summary>
    /// Its called after the game process the data (execute thje playfab's cloud
    /// function to update the score and grant the player money)
    /// </summary>
    public System.Action<CompletionDataModel> OnProcessEnd { get; set; }

    [Header("Required Components")]
    [SerializeField] private SpaceshipSpawner _playerSpawner;

    [SerializeField] private List<MeteoriteController> _meteoritePrefabs = new List<MeteoriteController>();
    [SerializeField] private MeteoriteSpawner _meteoriteSpawner;

    /// <summary>
    /// The player's spaceship
    /// </summary>
    public SpaceShipController Player { get; protected set; }

    public bool IsPlaying { get; private set; }
    public float ElapsedTime { get; private set; } = 0;
    public float StartTime { get; private set; } = Mathf.Infinity;
    public ushort Score { get; private set; } = 0;

    /// <summary>
    /// When was the last time a meteor was spawned
    /// is used for the meteor spawn rate (how often
    /// are meteors spawned)
    /// </summary>
    private float _lastMetioriteTime = -Mathf.Infinity;

    /// <summary>
    /// How often does the meteorite spawn
    /// </summary>
    private float _meteoriteSpawnCooldown = 5f;

    /// <summary>
    /// A multiplier for the speed of the meteorites
    /// </summary>
    private float _meteoriteSpeedMultiplier = 0.75f;

    /// <summary>
    /// How many coins will it get per shot
    /// </summary>
    private ushort _softCoinsMultiplier = 1;

    private int lastMetioriteSpawnerIndex = -1;

    [SerializeField] private TestSpaceshipBuilder testSpaceship;

    public override void Awake()
    {
        base.Awake();
        if (testSpaceship != null)
        {
            HandleInitialization();
        }
    }

    public override void Initialize()
    {
        base.Initialize();
        if (testSpaceship == null)
            HandleInitialization();
    }

    private async void HandleInitialization()
    {
        await SetupGame();
        await StartGame();
    }

    /// <summary>
    /// Setups and instantiates the required scripts and components
    /// (i.e. creating the player)
    /// </summary>
    /// <returns></returns>
    public async Task SetupGame()
    {
        IsPlaying = false;
        _meteoriteSpawner.Setup(_meteoritePrefabs);
        var player = await CreatePlayer();
        OnSetup?.Invoke();
    }

    public async Task StartGame()
    {
        StartTime = Time.time;
        OnStart?.Invoke();
        IsPlaying = true;
    }

    public void FinishGame()
    {
        IsPlaying = false;
        var data = new CompletionDataModel()
        {
            score = Score,
            time = ElapsedTime,
            softCoins = Score * 10
        };
        OnFinish?.Invoke(data);
        ProcessEndGame(data);
    }

    /// <summary>
    /// Process all the data for this game (adds score to the leaderboard, increases currency, etc.)
    /// </summary>
    public async void ProcessEndGame(CompletionDataModel completionData)
    {
        var result = await PlayerData.GameScores.FinishGame(completionData.score, completionData.time);
        OnProcessEnd?.Invoke(result);
    }

    #region SETUP METHODS

    /// <summary>
    /// Creates an spaceship builder based on the Player's Data
    /// (This can only be called after initialization)
    /// </summary>
    /// <returns></returns>
    private SpaceshipBuilder GetSpaceshipFromPlayersData()
    {
        var spaceshipId = PlayerData.Selections.CurrentSpaceshipId;
        var turretId = PlayerData.Selections.CurrentTurretId;
        var turretLevel = PlayerData.Inventory.Items[turretId].InventoryItem.customData.currentUsableLevel;
        var spaceshipLevel = PlayerData.Inventory.Items[spaceshipId].InventoryItem.customData.currentUsableLevel;

        return SpaceshipBuilder.Start()
                               .SetSpaceshipCreatorAsset(SpaceshipDB.Instance.GetItem(spaceshipId), spaceshipLevel)
                               .SetTurretCreatorAsset(TurretsDB.Instance.GetItem(turretId), turretLevel)
                               .SetInput(SpaceshipBuilder.InputType.Player);
    }

    private async Task<SpaceShipController> CreatePlayer()
    {
        var playerSpaceshipBulder =
            testSpaceship == null ? GetSpaceshipFromPlayersData() : testSpaceship.CreateBuilder();

        Player = _playerSpawner.Spawn(playerSpaceshipBulder);

        Player.OnBulletHit += hitInfo =>
        {
            if (hitInfo.targetRemainingHealth <= 0)
                Score += _softCoinsMultiplier;
        };

        Player.OnDeath += FinishGame;

        return Player;
    }

    #endregion SETUP METHODS

    #region GAME LOOP METHODS

    private void Update()
    {
        if (!IsPlaying)
            return;

        ElapsedTime = Time.time - StartTime;

        if (Time.frameCount % 5 == 0)
            UpdateDifficulty();

        ManageMeteorites();
    }

    /// <summary>
    /// Updates the game's difficulty depending on the elapsed time
    /// </summary>
    private void UpdateDifficulty()
    {
        _meteoriteSpawnCooldown = 5f - DifficultyEquation(ElapsedTime, 0.005f);

        //Cap the cooldown
        if (_meteoriteSpawnCooldown <= 0.15f)
            _meteoriteSpawnCooldown = 0.15f;

        _meteoriteSpeedMultiplier = 0.75f + DifficultyEquation(ElapsedTime, 0.01f) - 0.75f;

        if (_meteoriteSpeedMultiplier < 0.75f)
            _meteoriteSpeedMultiplier = 0.75f;

        _softCoinsMultiplier = (ushort)(1 + DifficultyEquation(ElapsedTime, 0.005f));
    }

    /// <summary>
    /// A f(x) linear function [f(x)=x*n], it can be used as a multiplier to increase difficulty
    /// for example increasing enemies health or speed
    /// </summary>
    /// <param name="x">X value that must increase during the game (for example the elapsed time)</param>
    /// <param name="multiplier">Increase (or decrease if less than 1) the value returned by the linear equation</param>
    /// <returns>x*multiplier</returns>
    private float DifficultyEquation(float x, float multiplier)
    {
        return x * multiplier;
    }

    /// <summary>
    /// Spawns a random meteorite on a random spawner
    /// </summary>
    private void ManageMeteorites()
    {
        if (Time.time - _lastMetioriteTime >= _meteoriteSpawnCooldown)
        {
            _meteoriteSpawner.Spawn(_meteoriteSpeedMultiplier);
            _lastMetioriteTime = Time.time;
        }
    }

    #endregion GAME LOOP METHODS
}