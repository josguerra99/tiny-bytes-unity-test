using UnityEngine;

public abstract class TargetSpawner<T> : MonoBehaviour where T : TargetBase
{
    public abstract T Spawn();

    protected abstract void InitializeTarget(T target);

    protected void SetPosition(Transform targetTransform)
    {
        targetTransform.SetPositionAndRotation(transform.position, transform.rotation);
    }
}