using UnityEngine;
using System.Collections.Generic;

namespace Meteorite
{
    public class MeteoriteSpawner : MonoBehaviour
    {
        [SerializeField] private List<Transform> _spawnPositions;
        private float _speedMultiplier = 1f;

        private readonly List<GameObjectPoolBase<MeteoriteController>> spawners = new List<GameObjectPoolBase<MeteoriteController>>();

        /// <summary>
        /// Initializes this spawner
        /// </summary>
        /// <param name="prefabs"></param>
        public void Setup(List<MeteoriteController> prefabs)
        {
            for (int i = 0; i < prefabs.Count; i++)
            {
                spawners.Add(new GameObjectPoolBase<MeteoriteController>(prefabs[i]));
            }
        }

        /// <summary>
        /// Clears all the spanwer pools
        /// </summary>
        private void DisposeSpawners()
        {
            for (int i = 0; i < spawners.Count; i++)
            {
                var spawner = spawners[i];
                if (spawner != null)
                    spawner.Dispose();
            }
            spawners.Clear();
        }

        /// <summary>
        ///
        /// </summary>
        /// <param name="speedMultiplier">Speed Multiplier to apply to the meteorites</param>
        /// <returns></returns>
        public MeteoriteController Spawn(float speedMultiplier)
        {
            _speedMultiplier = speedMultiplier;
            return Spawn();
        }

        /// <summary>
        /// Spawns a metiorite and initializes it
        /// </summary>
        /// <returns></returns>
        public MeteoriteController Spawn()
        {
            var randomPosition = GetRandomSpawnerPosition();
            var randomMeteoriteSpawner = spawners[Random.Range(0, spawners.Count)];//Gets random spawner

            var meteorite = randomMeteoriteSpawner.Pool.Get();
            meteorite.transform.SetPositionAndRotation(randomPosition.position, randomPosition.rotation);
            meteorite.SpeedMultiplier = _speedMultiplier;
            meteorite.Initialize();

            void handleDeath()
            {
                if (randomMeteoriteSpawner != null)
                    randomMeteoriteSpawner.Pool.Release(meteorite);

                meteorite.OnDeath -= handleDeath;
            }

            meteorite.OnDeath += handleDeath;

            return meteorite;
        }

        private int _lastMetoeritePositionIndex = -1;

        /// <summary>
        /// Gets a random spawn position without repeating itself
        /// </summary>
        /// <returns></returns>
        private Transform GetRandomSpawnerPosition()
        {
            var counter = 0;
            while (true)
            {
                var index = Random.Range(0, _spawnPositions.Count);

                if (index != _lastMetoeritePositionIndex || counter > 10)
                {
                    _lastMetoeritePositionIndex = index;
                    return _spawnPositions[index];
                }

                counter++;
            }
        }

        private void OnDestroy()
        {
            DisposeSpawners();
        }
    }
}