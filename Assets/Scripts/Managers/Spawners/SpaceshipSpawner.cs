using Spaceship;

public class SpaceshipSpawner : TargetSpawner<SpaceShipController>
{
    private SpaceshipBuilder builder;

    public SpaceShipController Spawn(SpaceshipBuilder builder)
    {
        this.builder = builder;
        return Spawn();
    }

    public override SpaceShipController Spawn()
    {
        var spaceship = builder.Build();
        SetPosition(spaceship.transform);
        InitializeTarget(spaceship);
        return spaceship;
    }

    protected override void InitializeTarget(SpaceShipController target)
    {
    }
}