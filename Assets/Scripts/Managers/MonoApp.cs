using System;
using UnityEngine;

public class MonoApp : MonoBehaviour
{
    /// <summary>
    /// Should this MonoApp listen for initialization errors
    /// </summary>
    protected virtual bool IsListeningForErrors => false;

    /// <summary>
    /// Is this MonoApp already initialized
    /// </summary>
    protected bool IsInitialized { get; set; }

    public virtual void Awake()
    {
        App.SubscribeToInitialization(Initialize);
        if (IsListeningForErrors)
            App.SubscribeToError(OnError);
    }

    /// <summary>
    /// Called when the app initialized
    /// </summary>
    public virtual void Initialize()
    {
        IsInitialized = true;
    }

    /// <summary>
    /// Called when an error occurs during initialization
    /// </summary>
    /// <param name="exception">Exception to show</param>
    protected virtual void OnError(Exception exception)
    {
    }

    /// <summary>
    /// Called inside OnDestroy only if this MonoApp
    /// was initialized
    /// </summary>
    public virtual void OnDestroyIfInitialized()
    {
    }

    public virtual void OnDestroy()
    {
        if (!IsInitialized)
        {
            App.UnsubscribeFromInitialization(Initialize);

            if (IsListeningForErrors)
                App.UnsubscribeFromError(OnError);
        }
        else
        {
            OnDestroyIfInitialized();
        }
    }
}