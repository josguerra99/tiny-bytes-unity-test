using System;

/// <summary>
/// Waits for the App to be initialized,
/// when is done, it will open the next scene
/// </summary>
public class Initializer : MonoApp
{
    public IHandleError ErrorHandler { get; set; }
    public IDisplayNameSetter DisplayNameSetter { get; set; }

    protected override bool IsListeningForErrors => true;

    public override void Initialize()
    {
        base.Initialize();

        //Checks if the user has already set his display name
        if (!string.IsNullOrEmpty(LoginManager.CurrentProfile.DisplayName))
            OpenNextScene();
        else
            DisplayNameSetter?.SetDisplayName(OpenNextScene);
    }

    protected override void OnError(Exception obj)
    {
        base.OnError(obj);
        if (ErrorHandler != null)
            ErrorHandler.SetError(obj.Message);
    }

    public void OpenNextScene()
    {
        SceneChanger.LoadSceneWithLoadingScreen("Menu");
    }

    public interface IDisplayNameSetter
    {
        /// <summary>
        /// Called when the player wants to set their name
        /// </summary>
        /// <param name="callback">Raised after the new display name is set</param>
        void SetDisplayName(Action callback);
    }

    public interface IHandleError
    {
        void SetError(string message);

        void Retry();
    }
}