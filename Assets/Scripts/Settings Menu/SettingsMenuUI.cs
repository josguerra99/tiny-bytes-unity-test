using UnityEngine;
using UnityEngine.UI;

public class SettingsMenuUI : MonoBehaviour
{
    [SerializeField] private Button _lowQualityBtn;
    [SerializeField] private Button _highQualityBtn;
    [SerializeField] private Button _lowFPSBtn;
    [SerializeField] private Button _highFPSBtn;

    public void GetValues()
    {
        var qualityFromPrefs = PlayerPrefs.GetInt("Quality", 3);
        var FPSFromPrefs = PlayerPrefs.GetInt("FPS", 60);

        QualitySettings.SetQualityLevel(qualityFromPrefs);
        Application.targetFrameRate = FPSFromPrefs;
    }

    private void Awake()
    {
        GetValues();
    }

    private void Start()
    {
        _highQualityBtn.onClick.AddListener(SetHighQuality);
        _lowQualityBtn.onClick.AddListener(SetLowQuality);
        _highFPSBtn.onClick.AddListener(SetHighFPS);
        _lowFPSBtn.onClick.AddListener(SetLowFPS);
        UI_UpdateInteractable();
    }

    public void SetQuality(int level)
    {
        QualitySettings.SetQualityLevel(level);
        PlayerPrefs.SetInt("Quality", level);
        UI_UpdateInteractable();
    }

    public void SetFPS(int amount)
    {
        Application.targetFrameRate = amount;
        PlayerPrefs.SetInt("FPS", amount);
        UI_UpdateInteractable();
    }

    public void SetLowQuality()
    {
        SetQuality(1);
    }

    public void SetHighQuality()
    {
        SetQuality(3);
    }

    public void SetLowFPS()
    {
        SetFPS(30);
    }

    public void SetHighFPS()
    {
        SetFPS(60);
    }

    public void UI_UpdateInteractable()
    {
        var quality = QualitySettings.GetQualityLevel();
        var fps = Application.targetFrameRate;

        _lowQualityBtn.interactable = quality == 3;
        _highQualityBtn.interactable = quality == 1;
        _lowFPSBtn.interactable = fps == 60;
        _highFPSBtn.interactable = fps == 30;
    }
}