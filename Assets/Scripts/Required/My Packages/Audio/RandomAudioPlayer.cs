using RadioactiveGames.Common;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Utilities.Audio
{
    public class RandomAudioPlayer : MonoBehaviour
    {
        public AudioMngr.Type type;
        public ClipInfo[] clips;
        public Vector2 interval;

        public bool randomizeStereo = false;
        private IEnumerator scheduledAudio;

        private int oldIndex = -1;

        private void OnDestroy()
        {
            isDestroyed = true;
            Cancel();
        }

        private void OnDisable()
        {
            isDestroyed = true;
            Cancel();
        }

        private void Cancel()
        {
            if (source != null)
            {
                AudioMngr.Player(type).FreeSource(source, playId);
                source = null;
            }

            if (scheduledAudio != null)
            {
                StopCoroutine(scheduledAudio);
            }
        }

#if UNITY_EDITOR
        public int testIndex;
        public bool test = false;

        private void Update()
        {
            if (test)
            {
                var clip = clips[testIndex];
                Play(clip);

                test = false;
            }
        }

#endif

        private AudioSourceInfo source;
        private string playId;
        private bool isDestroyed = false;

        public void Play()
        {
            var clip = clips.GetRandomItem(ref oldIndex);
            Play(clip);
        }

        private void Play(ClipInfo clip)
        {
            source = AudioMngr.Player(type).Play(clip, (player) =>
            {
                player.source.panStereo = 0f;
                source = null;
            });
            playId = source.playId;
            if (randomizeStereo) source.source.panStereo = Random.Range(-1f, 1f);
        }
    }
}