using UnityEngine;

namespace Utilities.Audio
{
    [CreateAssetMenu(fileName = "AUDIO_", menuName = "Game/Audio/Audio Clip")]
    public class ClipInfoAsset : ScriptableObject
    {
        public ClipInfo clip;
    }
}