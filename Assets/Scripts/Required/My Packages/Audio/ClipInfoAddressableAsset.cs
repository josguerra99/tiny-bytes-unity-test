using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//using UnityEngine.AddressableAssets;
using Utilities.Audio;

[CreateAssetMenu(fileName = "AUDIO_Addresable", menuName = "Game/Audio/Audio Clip Addressable")]
public class ClipInfoAddressableAsset : ClipInfoAsset
{
    //  public AssetReferenceAudioClip addressable;
}