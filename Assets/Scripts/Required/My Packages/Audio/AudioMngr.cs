﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using Utilities.Audio;
using UnityEngine.Events;
using RadioactiveGames.Common;

namespace Utilities.Audio
{
    public class AudioMngr : MonoBehaviour
    {
        public AudioMixer mixer;
        public static AudioMngr Instance { get; private set; }
        [SerializeField] private List<Audio> sources = new List<Audio>();
        public Dictionary<Type, Audio> Dict { get; } = new Dictionary<Type, Audio>();

        public static Audio Player(Type type)
        {
            return Instance.Dict[type];
        }

        public void Awake()
        {
            if (Instance == null)
            {
                Instance = this;

                for (int i = 0; i < sources.Count; i++)
                {
                    var source = sources[i];
                    source.CreateAudioSource(transform);
                    source.Init(this);
                    Dict.Add(source.type, source);
                }

                if (gameObject.transform.parent == null) DontDestroyOnLoad(gameObject);
            }
            else
            {
                Destroy(gameObject);
            }
        }

        private void OnDestroy()
        {
            StopAllCoroutines();
        }

        [System.Serializable]
        public class Audio
        {
            [Header("Info")]
            public string name;

            public Type type;
            public int poolSize = 10;
            public bool bypassReverbZone = false;

            [Header("Required")]
            public AudioMixerGroup group;

            private readonly Dictionary<AudioSource, AudioSourceInfo> AllSources = new Dictionary<AudioSource, AudioSourceInfo>();
            private readonly List<AudioSource> inUse = new List<AudioSource>();
            private readonly Queue<AudioSourceInfo> available = new Queue<AudioSourceInfo>();

            [SerializeField, HideInInspector] private List<AudioSource> sources = new List<AudioSource>();
            private AudioMngr mngr;

            public Dictionary<AudioSource, FadeInfo> fades = new Dictionary<AudioSource, FadeInfo>();//to , fadeinfo

            public struct FadeInfo
            {
                public string id;
                public AudioSource from;
                public AudioSource to;
                public IEnumerator coroutine;
            }

            public void Init(AudioMngr mngr)
            {
                this.mngr = mngr;

                for (int i = 0; i < sources.Count; i++)
                {
                    var source = sources[i];
                    available.Enqueue(new AudioSourceInfo()
                    {
                        id = System.Guid.NewGuid().ToString(),
                        playId = null,
                        source = source
                    });

                    AllSources.Add(source, available.Peek());
                }
            }

            public void CreateAudioSource(Transform parent)
            {
                var tmp = new List<AudioSource>();
                tmp.AddRange(sources);

                sources.Clear();

                for (int i = 0; i < tmp.Count; i++)
                {
                    Destroy(tmp[i].gameObject);
                }

                for (int i = 0; i < poolSize; i++)
                {
                    var go = parent.CreateChild(null, "_source_" + name + "_" + i.ToString("00"));
                    var source = go.AddComponent<AudioSource>();
                    source.outputAudioMixerGroup = group;
                    source.bypassReverbZones = bypassReverbZone;
                    sources.Add(source);
                }
            }

            public List<AudioSourceInfo> GetFreeSources(int quantity)
            {
                var ret = new List<AudioSourceInfo>();
                for (int i = 0; i < quantity; i++)
                {
                    if (available.Count == 0) break;
                    ret.Add(available.Dequeue());
                }

                return ret;
            }

            public void PlayOneShot(AudioClip clip, float volumeScale)
            {
                sources[0].PlayOneShot(clip, volumeScale);
            }

            public AudioSourceInfo Play(ClipInfo info, System.Action<AudioSourceInfo> onStop = null)
            {
                var result = GetFreeSources(1);
                //Si no hay liberar las que esten en uso para utilizarla
                if (result.Count == 0)
                {
                    //Liberar necesarias aunque esten en uso (sonidos viejos)
                    for (int i = 0; i < 1; i++)
                    {
                        if (i >= inUse.Count) break;
                        var s = AllSources[inUse[i]];
                        FreeSource(s, s.playId);
                    }
                    result = GetFreeSources(1);
                }
                //Segunda comprobacion
                if (result.Count == 0) return null;

                var sourceInfo = result[0];
                var playId = System.Guid.NewGuid().ToString();
                sourceInfo.playId = playId;
                var source = sourceInfo.source;
                source.volume = info.volume;
                source.pitch = info.pitch;
                source.clip = info.Clip;
                source.loop = info.loop;
                sourceInfo.currentClip = info;

                if (!info.loop)
                {
                    AddStopEvent(sourceInfo, info, () =>
                     {
                         if (onStop != null) onStop.Invoke(sourceInfo);
                         FreeSource(sourceInfo, playId);
                     });
                    inUse.Add(source);
                }

                if (info.delay == 0) source.Play(); else source.PlayDelayed(info.delay);
                return sourceInfo;
            }

            public AudioSourceInfo GetSource(AudioSource source)
            {
                if (!AllSources.ContainsKey(source)) return null;
                return AllSources[source];
            }

            public AudioSource Blend(AudioSource from, ClipInfo to, float duration = 1f, bool keepOldTime = false)
            {
                AudioSource destination = Play(to)?.source;
                if (keepOldTime) destination.time = ListExtensions.FullCircular(from.time, to.Clip.length);
                destination.volume = 0;

                var coroutine = CrossFadeTo(from, to, destination, duration);
                var blendId = System.Guid.NewGuid().ToString();
                fades.Add(destination, new FadeInfo()
                {
                    id = blendId,
                    from = from,
                    to = destination,
                    coroutine = coroutine
                });

                if (fades.ContainsKey(from))
                {
                    var blend = fades[from];
                    Instance.StopCoroutine(blend.coroutine);
                    Instance.StartCoroutine(FadeOut(blend.from, duration));
                    fades.Remove(from);
                }
                Instance.StartCoroutine(coroutine);
                return destination;
            }

            public void FreeWithFade(AudioSourceInfo sourceInfo, string playId, float duration = 1f)
            {
                Instance.StartCoroutine(_FreeWithFade(sourceInfo, playId, duration));
            }

            public IEnumerator _FreeWithFade(AudioSourceInfo sourceInfo, string playId, float duration = 1f)
            {
                if (sourceInfo.playId != playId)
                {
                    yield break;
                }

                var t = 0f;
                var startVolume = sourceInfo.source.volume;
                var targetVolume = 0f;
                while (t < 1)
                {
                    t += Time.deltaTime / duration;
                    if (t > 1) t = 1f;
                    sourceInfo.source.volume = Mathf.Lerp(startVolume, targetVolume, t);
                    yield return null;
                }
                sourceInfo.source.volume = 0f;
                FreeSource(sourceInfo, playId);
            }

            public void FreeSource(AudioSourceInfo sourceInfo, string playId)
            {
                if (sourceInfo.playId != playId) { return; }
                var source = sourceInfo.source;

                sourceInfo.playId = null;
                sourceInfo.currentClip = null;

                if (source == null)
                    return;

                source.Stop();
                source.clip = null;
                available.Enqueue(sourceInfo);
                inUse.RemoveAll(el => el == source);
            }

            internal AudioSourceInfo Play(ClipInfoAsset asset, System.Action<AudioSourceInfo> onStop = null)
            {
                return Play(asset.clip, onStop);
            }

            public void AddStopEvent(AudioSourceInfo source, ClipInfo clip, System.Action action)
            {
                mngr.StartCoroutine(source.StopEvent(clip, action));
            }

            private IEnumerator CrossFadeTo(AudioSource from, ClipInfo to, AudioSource destination, float duration)
            {
                var fromVolume = from.volume;

                return Fade((percentage) =>
                {
                    destination.volume = Mathf.Sqrt(percentage) * to.volume;
                    from.volume = Mathf.Sqrt(1 - percentage) * fromVolume;
                },
                () =>
                {
                    destination.volume = to.volume;
                    from.volume = 0;
                    var source = AllSources[from];
                    FreeSource(source, source.playId);
                    fades.Remove(destination);
                }
                , duration);
            }

            private IEnumerator FadeOut(AudioSource from, float duration)
            {
                return Fade((percentage) =>
                {
                    from.volume = Mathf.Sqrt(1 - percentage);
                },
                () =>
                {
                    var source = AllSources[from];
                    FreeSource(source, source.playId);
                }
                , duration);
            }

            private IEnumerator Fade(System.Action<float> animate, System.Action onDone, float duration)
            {
                var startTime = Time.time;
                var finalTime = Time.time + duration;

                while (Time.time < finalTime)
                {
                    var percentage = (Time.time - startTime) / duration;
                    animate?.Invoke(percentage);
                    yield return null;
                }
                onDone?.Invoke();
            }
        }

        public enum Type
        { Music, SFX, UI, Ambience }

        public static float FloatToDB(float val)
        {
            return (Mathf.Log10(val) + 1) * 20f;
            //return Mathf.Pow(-((val / -80f) - 1f), 2f);
        }
    }

    [System.Serializable]
    public class ClipInfo
    {
        public virtual AudioClip Clip => clip;
        [SerializeField] private AudioClip clip;
        public float volume = 1f;
        public float pitch = 1f;
        public bool loop = false;
        public float delay = 0f;
        [System.NonSerialized] public WaitForSeconds _yield;
    }

    [System.Serializable]
    public class AudioSourceInfo
    {
        public string id;
        public bool IsPlaying => playId != null;
        public AudioSource source;
        public string playId;
        public ClipInfo currentClip = null;

        public IEnumerator StopEvent(ClipInfo clip, System.Action action)
        {
            if (clip._yield == null) { clip._yield = new WaitForSeconds(clip.delay + 0.1f + clip.Clip.length); }
            yield return clip._yield;
            action?.Invoke();
        }
    }
}