using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Utilities.Audio
{
    public class PlaySingletonAudio : AudioPlayer
    {
        public static PlaySingletonAudio Instance { get; private set; }

        protected override void Awake()
        {
            if (!Instance)
            {
                Instance = this;
                DontDestroyOnLoad(gameObject);
                base.Awake();
            }
            else
            {
                Destroy(this);
            }
        }
    }
}