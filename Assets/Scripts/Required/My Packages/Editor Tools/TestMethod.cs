using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestMethod : MonoBehaviour
{
#if UNITY_EDITOR
    public bool inEditMode = false;
    public UnityEngine.Events.UnityEvent onTest;
#endif
}