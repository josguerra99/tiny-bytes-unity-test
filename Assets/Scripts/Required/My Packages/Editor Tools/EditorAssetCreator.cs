using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

#if UNITY_EDITOR

namespace RadioactiveGames.EditorTools
{
    public static class EditorAssetCreator
    {
        public static T LoadAsset<T>(string path) where T : Object
        {
            var asset = (T)AssetDatabase.LoadAssetAtPath(path, typeof(T));
            return asset;
        }

        public static T CreateAsset<T>(string path, System.Action<T> modifyBeforeSave = null, bool focusAfterCreation = true) where T : ScriptableObject
        {
            T asset = ScriptableObject.CreateInstance<T>();
            modifyBeforeSave?.Invoke(asset);

            AssetDatabase.CreateAsset(asset, path);

            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();

            if (focusAfterCreation)
            {
                EditorUtility.FocusProjectWindow();
                Selection.activeObject = asset;
            }

            return asset;
        }

        public static T CreateAsset<T>(T original, string path, System.Action<T> modifyBeforeSave = null, bool focusAfterCreation = true) where T : ScriptableObject
        {
            T asset = Object.Instantiate(original);
            modifyBeforeSave?.Invoke(asset);

            AssetDatabase.CreateAsset(asset, path);
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();

            if (focusAfterCreation)
            {
                EditorUtility.FocusProjectWindow();
                Selection.activeObject = asset;
            }

            return asset;
        }
    }
}

#endif