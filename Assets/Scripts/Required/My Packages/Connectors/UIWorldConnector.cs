using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RadioactiveGames.UI
{
    public class UIWorldConnector : UIConnector
    {
        public Vector3 WorldPosition { get; set; }

        public override Vector2 AnchoredPosition
        {
            get
            {
                if (Cam == null) return Vector2.zero;

                GetScripts();
                Vector2 temp = Cam.WorldToViewportPoint(WorldPosition);
                temp.x *= canvasRT.sizeDelta.x;
                temp.y *= canvasRT.sizeDelta.y;
                temp.x -= canvasRT.sizeDelta.x * canvasRT.pivot.x;
                temp.y -= canvasRT.sizeDelta.y * canvasRT.pivot.y;
                return temp;
            }
        }
    }
}