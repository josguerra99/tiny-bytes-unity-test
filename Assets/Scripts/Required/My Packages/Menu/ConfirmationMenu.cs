using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RadioactiveGames.UI.Menu
{
    public class ConfirmationMenu : MenuBase
    {
        [SerializeField] private UnityEngine.UI.Button m_acceptButton;
        [SerializeField] private UnityEngine.UI.Button m_cancelButton;
        [SerializeField] private UnityEngine.Events.UnityEvent m_onAccept;
        [SerializeField] private UnityEngine.Events.UnityEvent m_onCancel;

        protected override void Awake()
        {
            base.Awake();
            m_acceptButton.onClick.AddListener(m_onAccept.Invoke);
            m_cancelButton.onClick.AddListener(m_onCancel.Invoke);
        }

        public void SetConfirmationEvent(UnityEngine.Events.UnityAction action, bool clearPrevious = true)
        {
            if (clearPrevious) m_onAccept.RemoveAllListeners();
            m_onAccept.AddListener(action);
        }
    }
}