﻿using System.Collections.Generic;
using UnityEngine;
using System;
using RadioactiveGames.Common;

namespace RadioactiveGames.UI.Menu
{
    public class MenuNavigation : MonoBehaviour
    {
        /// <summary>
        /// Graph of menus (this is the information used to navigate between the different menus)
        /// </summary>
        public MenuNavigationAsset GraphData;

        /// <summary>
        /// List of all the menus (monobehaviours)
        /// </summary>
        [SerializeField] private List<MenuBase> allMenus = new List<MenuBase>();

        private Dictionary<int, MenuBase> _db = null;

        /// <summary>
        /// Dictionary for all the menus inside this navigation group
        /// </summary>
        public Dictionary<int, MenuBase> MenusDB
        {
            get
            {
                if (_db == null)
                {
                    _db = new Dictionary<int, MenuBase>();

                    for (int i = 0; i < allMenus.Count; i++)
                    {
                        var menu = allMenus[i];
                        _db.Add(menu.Id, menu);
                    }
                }
                return _db;
            }
        }

        /// <summary>
        /// Stack of menus for back functionality
        /// </summary>
        public readonly Stack<int> MenusStack = new Stack<int>();

        /// <summary>
        /// Gets the parent of the current menu
        /// </summary>
        public MenuBase CurrentParent
        {
            get
            {
                //if (!string.IsNullOrEmpty(currentNode.parentId))
                //    return MenusDB[currentNode.parentId];
                return null;
            }
        }

        /// <summary>
        /// Gets the current open menu
        /// </summary>
        public MenuBase CurrentMenu => _currentMenu;

        //public MenuNavigationInfo.Node CurrentNode => _currentNode;

        /// <summary>
        /// Current open menu
        /// </summary>
        protected MenuBase _currentMenu;

        protected MenuNavigationAsset.Node _currentNode;

        /// <summary>
        /// Event that is called when the menu is changed
        /// (PreviousMenu, CurrentMenu)
        /// </summary>
        public Action<MenuBase, MenuBase> OnChangeMenu { get; set; }

        /// <summary>
        /// Event that is triggered when going back to a previous menu
        /// </summary>
        public Action OnBack { get; set; }

        [SerializeField] protected MenuBase _initialMenu;

        private void Start()
        {
            GoTo(_initialMenu);
        }

        /// <summary>
        /// Opens a menu
        /// </summary>
        /// <param name="menuId">Menu Id to open</param>
        public void GoTo(int menuId)
        {
            GoTo(MenusDB[menuId]);
        }

        /// <summary>
        /// Opens a menu
        /// </summary>
        /// <param name="menuId">Menu Id to open</param>
        public void GoTo(string menuId)
        {
            int hash = menuId.Hash();
            GoTo(hash);
        }

        /// <summary>
        /// Opens a menu
        /// </summary>
        /// <param name="menu">Menu to open</param>
        public void GoTo(MenuBase menu)
        {
            if (_currentMenu == null)
            {
                _currentMenu = menu;
                _currentMenu.Show();
            }

            var path = GraphData.GetPath(_currentMenu, menu);
            if (path == null) return;
            var old = _currentMenu;
            _currentMenu = menu;
            _currentNode = GraphData.GetNode(menu);
            foreach (var menuNode in path)
            {
                if (string.IsNullOrEmpty(menuNode.menuNameId)) continue;
                if (menuNode.menuNameId == menu.MenuNameId)
                {
                    MenusDB[menuNode.menuNameId.Hash()].Show();

                    //Abrir menus necesarios para la visualización
                    var currentNode = menuNode;
                    while (true)
                    {
                        var parentId = currentNode.parentNameId;
                        if (!currentNode.requiresPreviousOpen || string.IsNullOrEmpty(parentId))
                            break;

                        MenusDB[parentId.Hash()].Show();
                        currentNode = GraphData.GetNode(parentId);
                    }
                }
                else { MenusDB[menuNode.menuNameId.Hash()].Hide(); }
            }

            if (old != null)
                MenusStack.Push(old.Id);
            OnChangeMenu?.Invoke(old, _currentMenu);
        }

        /// <summary>
        /// Goes to the previous visited menu
        /// </summary>
        public virtual void GoBack()
        {
            if (MenusStack.Count == 0)
                return;

            GoTo(MenusStack.Pop());
            MenusStack.Pop();

            OnBack?.Invoke();
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
                GoBack();
        }
    }
}