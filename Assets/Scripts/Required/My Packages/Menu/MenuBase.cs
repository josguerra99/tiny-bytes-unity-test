﻿using UnityEngine;
using RadioactiveGames.Common;
using RadioactiveGames.UI.Animation;

namespace RadioactiveGames.UI.Menu
{
    /// <summary>
    /// Manages a simple menu object
    /// </summary>
    public class MenuBase : MonoBehaviour
    {
        [SerializeField] public UnityEngine.UI.Selectable defaultUIElement;
        [SerializeField] protected UIAnimator animator;
        [SerializeField] protected string menuId;

        public string MenuNameId => menuId;

        public System.Action OnShowStart { get; set; }
        public System.Action OnHideStart { get; set; }
        public System.Action OnShowFinish { get; set; }
        public System.Action OnHideFinish { get; set; }

        private int _id = -1;

        public int Id
        {
            get
            {
                if (_id == -1)
                    _id = menuId.Hash();

                return _id;
            }
        }

        private void Reset()
        {
            animator = gameObject.GetComponent<UIAnimator>();
        }

        protected virtual void Awake()
        {
            //Adds on start animation events
            animator.OnAnimationStart += (val) =>
            {
                if (val)
                    OnShowStart?.Invoke();
                else
                    OnShowFinish?.Invoke();
            };

            //Adds of finish animation events
            animator.OnAnimationFinish += (val) =>
            {
                if (val)
                    OnShowFinish?.Invoke();
                else
                    OnHideFinish?.Invoke();

                if (val)
                    defaultUIElement?.Select();
            };
        }

        /// <summary>
        /// Opens this menu
        /// </summary>
        public virtual void Show()
        {
            animator.Show();
        }

        /// <summary>
        /// Closes this menu
        /// </summary>
        public virtual void Hide()
        {
            animator.Hide();
        }

        /// <summary>
        /// Toggles open and close
        /// </summary>
        public virtual void Toggle()
        {
            if (IsShowing) Hide();
            else Show();
        }

        /// <summary>
        /// Checks if the current menu is open or not
        /// </summary>
        public bool IsShowing => animator.IsShowing;
    }
}