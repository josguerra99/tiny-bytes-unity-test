using UnityEngine;
using UnityEngine.UI;
using RadioactiveGames.UI.Animation;
using RadioactiveGames.UI.Menu;
using RadioactiveGames.Common;

public class MenuTabUI : MonoBehaviour
{
    [SerializeField] private Button m_button;
    [SerializeField] private string m_menuId;
    [SerializeField] private UIAnimator m_selectedAnim;
    [SerializeField] protected MenuNavigation navigation;

    public virtual MenuNavigation Navigation => navigation;

    private int menuIdHash;

    private void Awake()
    {
        menuIdHash = m_menuId.Hash();
    }

    private void Start()
    {
        m_button.onClick.AddListener(() => { Navigation.GoTo(menuIdHash); });

        if (Navigation.CurrentMenu.Id == menuIdHash)
            m_selectedAnim.Show();

        Navigation.OnChangeMenu += HandleChangeMenu;
    }

    /// <summary>
    /// Is triggered when a menu changes on the navigation,
    /// When this method is triggered then it will update the animator
    /// </summary>
    /// <param name="previous">Previous menu</param>
    /// <param name="current">New menu</param>
    private void HandleChangeMenu(MenuBase previous, MenuBase current)
    {
        if (previous.Id == menuIdHash)
        {
            m_selectedAnim.Hide();
        }

        if (current.Id == menuIdHash)
        {
            m_selectedAnim.Show();
        }
    }

    private void OnDestroy()
    {
        if (Navigation)
            Navigation.OnChangeMenu -= HandleChangeMenu;
    }
}