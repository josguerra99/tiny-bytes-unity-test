using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace RadioactiveGames.UI.Menu
{
    public class SubscribeToMenuEvents : MonoBehaviour
    {
        public MenuBase menu;
        public UnityEvent onStartShow;
        public UnityEvent onStartHide;

        public UnityEvent onFinishShow;
        public UnityEvent onFinishHide;

        //public bool dontTriggerOnSameState = false;

        private void Reset()
        {
            menu = GetComponent<MenuBase>();
        }

        private void Awake()
        {
            menu.OnShowStart += HandleShowStart;
            menu.OnShowFinish += HandleShowFinish;
            menu.OnHideStart += HandleHideStart;
            menu.OnHideFinish += HandleHideFinish;
        }

        private void HandleHideFinish()
        {
            HandleOnFinish(false);
        }

        private void HandleHideStart()
        {
            HandleOnStart(false);
        }

        private void HandleShowFinish()
        {
            HandleOnFinish(true);
        }

        private void HandleShowStart()
        {
            HandleOnStart(true);
        }

        private void HandleOnStart(bool val)
        {
            if (val) { onStartShow?.Invoke(); }
            else { onStartHide?.Invoke(); }
        }

        private void HandleOnFinish(bool val)
        {
            if (val) { onFinishShow?.Invoke(); }
            else onFinishHide?.Invoke();
        }

        private void OnDestroy()
        {
            if (menu == null) return;

            menu.OnShowStart -= HandleShowStart;
            menu.OnShowFinish -= HandleShowFinish;
            menu.OnHideStart -= HandleHideStart;
            menu.OnHideFinish -= HandleHideFinish;
        }
    }
}