﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace RadioactiveGames.UI.Menu
{
    [Serializable]
    public class MenuNavigationAsset : ScriptableObject
    {
        public List<NodeLinkData> links = new List<NodeLinkData>();
        public List<MenuNodeData> nodes = new List<MenuNodeData>();

        [SerializeField] public List<Node> Graph;

        /// <summary>
        /// Current menu info on a graph (a node from the graph)
        /// </summary>
        [Serializable]
        public struct Node
        {
            /// <summary>
            /// This node's index  (on this list)
            /// </summary>
            public int index;

            /// <summary>
            /// This node's id (unique id)
            /// </summary>
            public int id;

            /// <summary>
            /// Node's name
            /// </summary>
            public string menuNameId;

            /// <summary>
            /// Node's parent's name
            /// </summary>
            public string parentNameId;

            /// <summary>
            /// Check if the preivous menu has to be open for this menu to show up
            /// </summary>
            public bool requiresPreviousOpen;

            /// <summary>
            /// Checks neighbors by its indexes
            /// </summary>
            public List<int> neighbors;

            /// <summary>
            /// Menu's hashed id
            /// </summary>
            public int MenuId;

            /// <summary>
            /// Menu's parent's hashed id
            /// </summary>
            public int ParentId;
        };

        public Node GetNode(string id)
        {
            return Graph.Find((n) => n.menuNameId == id);
        }

        public Node GetNode(MenuBase menu)
        {
            return Graph.Find((n) => n.menuNameId == menu.MenuNameId);
        }

        public List<Node> GetPath(MenuBase start, MenuBase end)
        {
            var startIndex = GetNode(start).index;
            var endIndex = GetNode(end).index;
            return GetPath(startIndex, endIndex);
        }

        public List<Node> GetPath(int startIndex, int endIndex)
        {
            var prev = Solve(startIndex);
            return ReconstructPath(startIndex, endIndex, prev);
        }

        private List<Node> ReconstructPath(int startIndex, int endIndex, int[] prev)
        {
            var path = new List<Node>();
            for (int i = endIndex; i >= 0; i = prev[i])
            {
                path.Add(Graph[i]);
            }
            path.Reverse();

            if (path[0].index == startIndex)
                return path;
            return null;
        }

        public int[] Solve(int startIndex)
        {
            var q = new Queue<int>();
            q.Enqueue(startIndex);
            var visited = new bool[Graph.Count];
            visited[startIndex] = true;
            var prev = new int[Graph.Count];
            for (int i = 0; i < prev.Length; i++)
            {
                prev[i] = -1;
            }

            while (q.Count != 0)
            {
                var node = q.Dequeue();
                var neighbours = Graph[node].neighbors;

                foreach (var next in neighbours)
                {
                    if (!visited[next])
                    {
                        q.Enqueue(next);
                        visited[next] = true;
                        prev[next] = node;
                    }
                }
            }
            return prev;
        }
    }
}