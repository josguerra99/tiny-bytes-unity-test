﻿using UnityEngine;

namespace RadioactiveGames.UI.Menu
{
    [System.Serializable]
    public class MenuNodeData
    {
        public int id;
        public Vector2 position;

        public string menuId;
        public bool requiresPreviousToBeOpen = false;
    }
}