﻿namespace RadioactiveGames.UI.Menu
{
    [System.Serializable]
    public struct NodeLinkData
    {
        /// <summary>
        /// Start node for this link (edge)
        /// </summary>
        public int baseNodeId;

        /// <summary>
        /// End node for this link (edge)
        /// </summary>
        public int targetNodeGuid;

        public string portName;
    }
}