using System.Text;

namespace RadioactiveGames.Common
{
    public static class StringExtensions
    {
        /// <summary>
        /// Gets a deterministic int value from a string
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static int Hash(this string input)
        {
            if (string.IsNullOrEmpty(input))
                return 0;

            unchecked
            {
                int hash1 = (5381 << 16) + 5381;
                int hash2 = hash1;

                for (int i = 0; i < input.Length; i += 2)
                {
                    hash1 = ((hash1 << 5) + hash1) ^ input[i];
                    if (i == input.Length - 1)
                        break;
                    hash2 = ((hash2 << 5) + hash2) ^ input[i + 1];
                }

                return hash1 + (hash2 * 1566083941);
            }
        }

        /// <summary>
        /// Removes non ascii characters
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string RemoveAccents(this string input)
        {
            string normalized = input.Normalize(NormalizationForm.FormKD);
            Encoding removal = Encoding.GetEncoding(Encoding.ASCII.CodePage,
                                                    new EncoderReplacementFallback(""),
                                                    new DecoderReplacementFallback(""));
            byte[] bytes = removal.GetBytes(normalized);
            return Encoding.ASCII.GetString(bytes);
        }

        /// <summary>
        /// Compares to texts, removing accents and being case insensitive
        /// </summary>
        /// <param name="original">Original full text</param>
        /// <param name="compareTo">Current text</param>
        /// <returns></returns>
        public static bool SearchCompare(this string original, string compareTo)
        {
            if (string.IsNullOrWhiteSpace(compareTo)) return true;
            original = original.ToLowerInvariant().RemoveAccents();
            compareTo = compareTo.ToLowerInvariant().Trim().RemoveAccents();
            return original.StartsWith(compareTo) || original.Contains(compareTo);
        }

        /// <summary>
        /// Big numbers suffixes
        /// </summary>
        private static readonly string[] suffixes = new string[] {string.Empty,
        "k", "M", "B", "T", "qd", "Qn", "sx", "Sp", "O", "N",
            "de", "Ud", "DD", "tdD", "qdD", "QnD", "sxD", "SpD", "OcD", "NvD",
            "Vgn", "UVg", "DVg", "TVg", "qtV", "QnV", "SeV", "SPG", "OVG", "NVG",
            "TGN", "UTG", "DTG", "tsTG", "qtTG", "QnTG", "ssTG", "SpTG", "OcTG", "NoTG",
            "QdDR", "uQDR", "dQDR", "tQDR", "qdQDR", "QnQDR", "sxQDR", "SpQDR", "OQDDr",
            "NQDDr", "qQGNT", "uQGNT", "dQGNT", "tQGNT", "qdQGNT", "QnQGNT", "sxQGNT",
            "SpQGNT", "OQQGNT", "NQQGNT", "SXGNTL"
        };

        /// <summary>
        /// Create a BigIntenger from a string
        /// </summary>
        /// <param name="str">Number as text</param>
        /// <returns>BigInteger</returns>
        public static System.Numerics.BigInteger StringToBig(this string str)
        {
            //Check if it ends with a number
            var lastDigit = str[str.Length - 1];

            if (char.IsDigit(lastDigit))
            {
                var parse = System.Numerics.BigInteger.TryParse(str, out var result);
                if (!parse) return 0;
                return result;
            }

            str = str.ToLower();
            //Check if it ends with a prefix
            for (int i = 1; i < suffixes.Length; i++)
            {
                if (str.EndsWith(suffixes[i].ToLower()))
                {
                    var prefix = suffixes[i];
                    string onlyNumbers = str.Substring(0, str.Length - prefix.Length);
                    var canParse = System.Numerics.BigInteger.TryParse(onlyNumbers, out var number);
                    if (!canParse) return 0;

                    var pow = System.Numerics.BigInteger.Pow(10, i * 3);
                    return System.Numerics.BigInteger.Multiply(number, pow);
                }
            }

            return 0;
        }

        /// <summary>
        /// Returns a short string value of a number with suffixes i.e. (k, M, B)
        /// </summary>
        /// <param name="moneyValue">Big Integer</param>
        /// <returns>Number as a humanized text</returns>
        public static string BigToString(this System.Numerics.BigInteger moneyValue)
        {
            if (moneyValue < 10000)
                return "" + moneyValue;

            try
            {
                string moneyAsString = moneyValue.ToString();
                string prefix = suffixes[(moneyAsString.Length - 1) / 3];

                System.Numerics.BigInteger chopAmmount = (moneyAsString.Length - 1) % 3 + 1;

                int insertPoint = (int)chopAmmount;

                chopAmmount += 2;

                moneyAsString = moneyAsString.Remove(System.Math.Min(moneyAsString.Length - 1, (int)chopAmmount));
                moneyAsString = moneyAsString.Insert(insertPoint, ".");

                if (moneyAsString.EndsWith(".00"))
                    moneyAsString = moneyAsString.Substring(0, moneyAsString.Length - 3);

                return moneyAsString + prefix;
            }
            catch
            {
                return "Infinity";
            }
        }
    }
}