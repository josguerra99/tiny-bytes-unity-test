using System.Collections;
using UnityEngine;

public static class CoroutineExtensions
{
    public readonly static WaitForSeconds delayQuarterSecond = new WaitForSeconds(0.25f);
    public readonly static WaitForSeconds delayHalfSecond = new WaitForSeconds(0.5f);
    public readonly static WaitForSeconds delay1Second = new WaitForSeconds(1f);
    public readonly static WaitForSeconds delay5Seconds = new WaitForSeconds(5f);

    /// <summary>
    /// Executes callback the next frame
    /// </summary>
    /// <param name="mono">Which monobehaviour will execute the coroutine</param>
    /// <param name="callback">Callback which will be executed after m frames</param>
    /// <param name="multiplier">How many frames you should wait</param>
    public static void ExecuteNextFrame(this MonoBehaviour mono, System.Action callback, int multiplier = 1)
    {
        mono.StartCoroutine(ExecuteNextFrame(callback, multiplier));
    }

    /// <summary>
    /// Executes callback after a quarter of a second
    /// </summary>
    /// <param name="mono">Which monobehaviour will execute the coroutien</param>
    /// <param name="callback">Callback which will be executed after m frames</param>
    /// <param name="multiplier">How many quarter of seconds should wait</param>
    public static void WaitAQuarterSecond(this MonoBehaviour mono, System.Action callback, int multiplier = 1)
    {
        mono.StartCoroutine(WaitAQuarterSecond(callback, multiplier));
    }

    public static void WaitHalfSecond(this MonoBehaviour mono, System.Action callback, int multiplier = 1)
    {
        mono.StartCoroutine(WaitHalfSecond(callback, multiplier));
    }

    public static void WaitASecond(this MonoBehaviour mono, System.Action callback, int multiplier = 1)
    {
        mono.StartCoroutine(WaitASecond(callback, multiplier));
    }

    public static void Wait5Seconds(this MonoBehaviour mono, System.Action callback, int multiplier = 1)
    {
        mono.StartCoroutine(Wait5Seconds(callback, multiplier));
    }

    #region COROUTINES

    public static IEnumerator ExecuteNextFrame(System.Action callback, int multiplier = 1)
    {
        yield return Wait(null, multiplier);
        callback.Invoke();
    }

    public static IEnumerator WaitAQuarterSecond(System.Action callback, int multiplier = 1)
    {
        yield return Wait(delayQuarterSecond, multiplier);
        callback.Invoke();
    }

    public static IEnumerator WaitHalfSecond(System.Action callback, int multiplier = 1)
    {
        yield return Wait(delayHalfSecond, multiplier);
        callback.Invoke();
    }

    public static IEnumerator WaitASecond(System.Action callback, int multiplier = 1)
    {
        yield return Wait(delay1Second, multiplier);
        callback.Invoke();
    }

    public static IEnumerator Wait5Seconds(System.Action callback, int multiplier = 1)
    {
        yield return Wait(delay1Second, multiplier);
        callback.Invoke();
    }

    private static IEnumerator Wait(YieldInstruction instruction, int multiplier = 1)
    {
        if (multiplier == 1)
        {
            yield return instruction;
        }
        else
        {
            for (int i = 0; i < multiplier; i++)
            {
                yield return instruction;
            }
        }
    }

    #endregion COROUTINES
}