using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RadioactiveGames.Common
{
    public static class ListExtensions
    {
        /// <summary>
        /// Circular index (for example, if a list has a length of 4, the index 4 will return 0)
        /// </summary>
        /// <param name="currentId"></param>
        /// <param name="length"></param>
        /// <returns>Fixed Index</returns>
        public static int FullCircular(int currentId, int length)
        {
            if (length == 0) return -1;
            return (currentId + length) % length;
        }

        public static float FullCircular(float currentId, float length)
        {
            if (length == 0) return -1f;
            return (currentId + length) % length;
        }

        /// <summary>
        /// Get a random number without repeating the previous value
        /// </summary>
        /// <param name="old"></param>
        /// <param name="min"></param>
        /// <param name="max"></param>
        /// <returns></returns>
        public static int GetRandomNoReapiting(ref int old, int min, int max)
        {
            if ((max - min) <= 1)
                return Mathf.Abs((max - min));

            int current = old;
            while (current == old)
            {
                current = Random.Range(min, max);
            }
            old = current;
            return old;
        }

        public static T GetRandomItem<T>(this List<T> list)
        {
            var i = -1;
            return GetRandomItem(list, ref i);
        }

        public static T GetRandomItem<T>(this T[] array)
        {
            var i = -1;
            return GetRandomItem(array, ref i);
        }

        public static T GetRandomItem<T>(this List<T> list, ref int old)
        {
            return list[GetRandomNoReapiting(ref old, 0, list.Count - 1)];
        }

        public static T GetRandomItem<T>(this T[] array, ref int old)
        {
            return array[GetRandomNoReapiting(ref old, 0, array.Length - 1)];
        }

        public static float GetRandomNoReapiting(ref float old, float min, float max)
        {
            if ((max - min) <= 1)
                return Mathf.Abs((max - min));

            float current = old;
            while (current == old)
            {
                current = Random.Range(min, max);
            }
            old = current;

            return 0f;
        }
    }
}