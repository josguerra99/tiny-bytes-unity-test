﻿using UnityEngine;
using System.Linq;

public static class MonoExtensions
{
    /// <summary>
    /// Destroys a gameobject,
    /// if the application is playing it uses Destroy
    /// otherwise it uses DestroyImmediate
    /// </summary>
    /// <param name="g"></param>
    public static void Destroy(GameObject g)
    {
        if (Application.isPlaying)
        {
            Object.Destroy(g);
        }
        else
        {
            Object.DestroyImmediate(g);
        }
    }

    /// <summary>
    /// Destroys all the childrens inside a transform
    /// </summary>
    /// <param name="transform">Parent of the children</param>

    public static void DestroyAllChildren(this Transform transform)
    {
        if (transform == null) return;

        if (Application.isPlaying)
        {
            foreach (Transform child in transform)
            {
                if (child != transform)
                    Object.Destroy(child.gameObject);
            }
        }
        else
        {
            var tempList = transform.Cast<Transform>().ToList();
            foreach (var child in tempList)
            {
                if (child != transform)
                    Object.DestroyImmediate(child.gameObject);
            }
        }
    }

    /// <summary>
    /// Creates a prefab inside a gameobject
    /// </summary>
    /// <typeparam name="T">Monobehaviour</typeparam>
    /// <param name="parent">Parent of the prefab</param>
    /// <param name="prefab">Prefab to instantiate</param>
    /// <param name="name">Name of the instance</param>
    /// <returns>Monobehaviour (Prefab type)</returns>
    public static T CreatePrefab<T>(this Transform parent, T prefab, string name = "_child") where T : MonoBehaviour
    {
        if (prefab == null)
            return null;

        var instance = Object.Instantiate(prefab);
        if (parent != null)
        {
            instance.transform.SetParent(parent);
            instance.transform.localScale = Vector3.one;
            instance.transform.localPosition = Vector3.zero;
            instance.transform.localRotation = Quaternion.identity;
        }
        instance.gameObject.name = name;
        return instance;
    }

    /// <summary>
    /// Creates a gameobject as children of another gameobject
    /// </summary>
    /// <param name="parent">Parent of the gameobject</param>
    /// <param name="prefab">Prefab to instantiate (if this param is null then a new gameobject will be created)</param>
    /// <param name="name">Name of the instance</param>
    /// <returns>GameObject</returns>

    public static GameObject CreateChild(this Transform parent, GameObject prefab = null, string name = "_child")
    {
        GameObject child;
        if (prefab == null)
            child = new GameObject(name);
        else
            child = Object.Instantiate(prefab, parent);

        if (parent != null)
        {
            child.transform.SetParent(parent);
            child.transform.localScale = Vector3.one;
            child.transform.localPosition = Vector3.zero;
            child.transform.localRotation = Quaternion.identity;
        }

        child.name = name;
        return child;
    }

    /// <summary>
    /// Gets a component from gameObject, if the component doesnt exist,
    /// it will add it
    /// </summary>
    /// <typeparam name="T">Component</typeparam>
    /// <param name="item"></param>
    /// <returns>Component</returns>

    public static T GetAddComponent<T>(this MonoBehaviour item) where T : Component
    {
        if (item.GetComponent<T>() == null)
            item.gameObject.AddComponent<T>();
        return item.GetComponent<T>();
    }

    public static T GetAddComponent<T>(this GameObject item) where T : Component
    {
        if (item.GetComponent<T>() == null)
            item.AddComponent<T>();
        return item.GetComponent<T>();
    }

    public static T GetAddComponent<T>(this T item) where T : Component
    {
        if (item.GetComponent<T>() == null)
            item.gameObject.AddComponent<T>();
        return item.GetComponent<T>();
    }
}