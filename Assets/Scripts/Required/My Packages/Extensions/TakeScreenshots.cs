using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class TakeScreenshots : MonoBehaviour
{
    private string _path;

    private void Awake()
    {
        _path = Path.Combine(Application.persistentDataPath, "Screenshots");
        if (!Directory.Exists(_path))
            Directory.CreateDirectory(_path);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.F2))
        {
            var date = System.DateTime.Now;
            var path = Path.Combine(_path, $"Screenshot-{date.Year}{date.Month}{date.Day}{date.Hour}{date.Minute}{date.Second}.png");
            ScreenCapture.CaptureScreenshot(path, 2);
        }
    }
}