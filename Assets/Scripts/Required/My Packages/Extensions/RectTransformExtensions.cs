﻿using UnityEngine;

namespace RadioactiveGames.Common
{
    public static class RectTransformExtensions
    {
        public static Rect WorldRect(this RectTransform rt)
        {
            var worldCorners = new Vector3[4];
            rt.GetWorldCorners(worldCorners);
            var result = new Rect(
                          worldCorners[0].x,
                          worldCorners[0].y,
                          worldCorners[2].x - worldCorners[0].x,
                          worldCorners[2].y - worldCorners[0].y);
            return result;
        }

        /// <summary>
        /// Checks if a RectTransform overlaps with another
        /// </summary>
        /// <param name="a"></param>
        /// <param name="b"></param>
        /// <returns></returns>
        public static bool Overlaps(this RectTransform a, RectTransform b)
        {
            return a.WorldRect().Overlaps(b.WorldRect());
        }

        public static void SetLeft(this RectTransform rt, float left)
        {
            rt.offsetMin = new Vector2(left, rt.offsetMin.y);
        }

        public static void SetRight(this RectTransform rt, float right)
        {
            rt.offsetMax = new Vector2(-right, rt.offsetMax.y);
        }

        public static void SetTop(this RectTransform rt, float top)
        {
            rt.offsetMax = new Vector2(rt.offsetMax.x, -top);
        }

        public static void SetBottom(this RectTransform rt, float bottom)
        {
            rt.offsetMin = new Vector2(rt.offsetMin.x, bottom);
        }

        /// <summary>
        /// Converts from a world position to a canvas position (pivot and anchored position must be on the center)
        /// </summary>
        /// <param name="canvasRT">Rect Transform of canvas</param>
        /// <param name="position">World Position</param>
        /// <param name="offset">Offset</param>
        /// <returns>Canvas position (pivot and anchored position must be centered)</returns>
        public static Vector2 WorldToCanvas(this RectTransform canvasRT, Vector3 position, Vector2 offset = default)
        {
            //For example (0,0) is lower left, middle is (0.5,0.5)
            Vector2 temp = Camera.main.WorldToViewportPoint(position);

            //Calculate position considering our percentage, using our canvas size
            //So if canvas size is (1100,500), and percentage is (0.5,0.5), current value will be (550,250)
            temp.x *= canvasRT.sizeDelta.x;
            temp.y *= canvasRT.sizeDelta.y;

            //The result is ready, but, this result is correct if canvas recttransform pivot is 0,0 - left lower corner.
            //But in reality its middle (0.5,0.5) by default, so we remove the amount considering cavnas rectransform pivot.
            //We could multiply with constant 0.5, but we will actually read the value, so if custom rect transform is passed(with custom pivot) ,
            //returned value will still be correct.

            temp.x -= canvasRT.sizeDelta.x * canvasRT.pivot.x;
            temp.y -= canvasRT.sizeDelta.y * canvasRT.pivot.y;

            return temp + offset;
        }

        public static Vector3 ScreenToWorld(Camera camera, Vector3 position)
        {
            position.z = camera.nearClipPlane;
            return camera.ScreenToWorldPoint(position);
        }

        public static Vector2 ScreenToCanvas(this RectTransform canvasRT, Vector2 position)
        {
            Vector2 temp = Camera.main.ScreenToViewportPoint(position);

            temp.x *= canvasRT.sizeDelta.x;
            temp.y *= canvasRT.sizeDelta.y;
            temp.x -= canvasRT.sizeDelta.x * canvasRT.pivot.x;
            temp.y -= canvasRT.sizeDelta.y * canvasRT.pivot.y;

            return temp;
        }

        public static Rect RectTransformToScreenSpace(this RectTransform transform)
        {
            Vector2 size = Vector2.Scale(transform.rect.size, transform.lossyScale);
            return new Rect((Vector2)transform.position - (size * 0.5f), size);
        }

        public static bool PointInsideRT(this RectTransform rt, Vector2 position)
        {
            Vector2 localMousePosition = rt.InverseTransformPoint(position);
            return rt.rect.Contains(localMousePosition);
        }

        public enum Anchor { Left, Right, Up, Down };

        /// <summary>
        /// Sets the anchor and pivot from code
        /// </summary>
        /// <param name="rt"></param>
        /// <param name="anchor"></param>
        /// <param name="extended"></param>
        public static void SetAnchorsAndPivot(ref RectTransform rt, Anchor anchor, bool extended = false)
        {
            Vector2 anchorMin = Vector2.zero;
            Vector2 anchorMax = Vector2.zero;
            Vector2 pivot = Vector2.zero;
            switch (anchor)
            {
                case Anchor.Left:
                    if (extended) { anchorMin = new Vector2(0, 0); anchorMax = new Vector2(0, 1); }
                    else { anchorMin = anchorMax = new Vector2(0, 0.5f); }
                    pivot = new Vector2(0, 0.5f);
                    break;

                case Anchor.Right:
                    if (extended) { anchorMin = new Vector2(1, 0); anchorMax = new Vector2(1, 1); }
                    else { anchorMin = anchorMax = new Vector2(1, 0.5f); }
                    pivot = new Vector2(1, 0.5f);
                    break;

                case Anchor.Up:
                    if (extended) { anchorMin = new Vector2(0, 1); anchorMax = new Vector2(1, 1); }
                    else { anchorMin = anchorMax = new Vector2(0.5f, 1); }
                    pivot = new Vector2(0.5f, 1);
                    break;

                case Anchor.Down:
                    if (extended) { anchorMin = Vector2.zero; anchorMax = new Vector2(1f, 0); }
                    else { anchorMin = anchorMax = new Vector2(0.5f, 0); }
                    pivot = new Vector2(0.5f, 0);
                    break;

                default:
                    break;
            }

            rt.anchorMin = anchorMin;
            rt.anchorMax = anchorMax;
            rt.pivot = pivot;
        }
    }
}