using System.Collections;
using UnityEngine;

public class CoroutineExecuter : MonoBehaviour
{
    private static CoroutineExecuter _instance = null;

    public static CoroutineExecuter Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = (new GameObject("coroutine_executer")).AddComponent<CoroutineExecuter>();
                DontDestroyOnLoad(_instance);
            }
            return _instance;
        }
    }

    public static void Stop(IEnumerator coroutine)
    {
        Instance.StopCoroutine(coroutine);
    }

    public static void Execute(IEnumerator coroutine)
    {
        Instance.StartCoroutine(coroutine);
    }
}