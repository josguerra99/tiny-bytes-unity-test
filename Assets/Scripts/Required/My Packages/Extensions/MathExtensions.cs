﻿using System.Collections.Generic;
using UnityEngine;

namespace RadioactiveGames.Common.Math
{
    public static class MathExtensions
    {
        /// <summary>
        /// Checks if two numbers are approximately the same
        /// </summary>
        /// <param name="a">First number</param>
        /// <param name="b">Second number</param>
        /// <param name="threshold">How close does the numbers need to be</param>
        /// <returns>Boolean representing if the numbers are close enough</returns>
        public static bool Approximately(float a, float b, float threshold = 0.05f)
        {
            return ((a - b) < 0 ? ((a - b) * -1) : (a - b)) <= threshold;
        }

        public static (int, int) GetAspecRatio(float width, float height)
        {
            float aspect = width / height;

            if (Approximately(aspect, 16f / 9f))
                return (16, 9);
            if (Approximately(aspect, 21f / 9f))
                return (21, 9);
            if (Approximately(aspect, 32f / 9f))
                return (32, 9);
            if (Approximately(aspect, 5f / 3f))
                return (5, 3);
            if (Approximately(aspect, 16f / 10f))
                return (16, 10);
            if (Approximately(aspect, 3f / 2f))
                return (3, 2);
            if (Approximately(aspect, 4f / 3f))
                return (4, 3);
            if (Approximately(aspect, 5f / 4f))
                return (5, 4);

            return (0, 0);
        }

        public static Vector3 Perpendicular(this Vector3 v1, Vector3 v2)
        {
            return Vector3.Cross(v1, v2);
        }

        public static Vector3 SwitchUpAxis(Vector3 v)
        {
            return new Vector3(v.x, v.z, v.y);
        }

        public static Vector3 ConvertCoordinates(Vector3 point, Vector3 origin, Vector3 up, Vector3 right)
        {
            Vector3 forward = Perpendicular(up, right);
            Vector3 x = right * point.x;
            Vector3 z = up * point.z;
            Vector3 y = forward * point.y;
            Vector3 final = x + y + z + origin;
            return final;
        }

        public static Vector3 Centroid(List<Vector3> points)
        {
            float x = 0, y = 0, z = 0;
            for (int i = 0; i < points.Count; i++)
            {
                var point = points[i];

                x += point.x;
                y += point.y;
                z += point.z;
            }

            float n = points.Count;
            return new Vector3(x / n, y / n, z / n);
        }
    }
}