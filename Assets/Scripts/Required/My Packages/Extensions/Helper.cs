﻿using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace RadioactiveGames.Common
{
    public static class Helper
    {
        /// <summary>
        /// Check layer belongs in the layer mask
        /// </summary>
        /// <param name="layer">Int of the layer</param>
        /// <param name="mask">Layermask</param>
        /// <returns>Boolean</returns>
        public static bool LayerIsInMask(int layer, LayerMask mask)
        {
            return mask == (mask | (1 << layer));
        }

        /// <summary>
        /// Sets the layer of the gameObject and all of its children
        /// </summary>
        /// <param name="go">GameObject</param>
        /// <param name="mask">LayerMask</param>
        public static void SetLayerRecursively(this GameObject go, LayerMask mask)
        {
            int layer = (int)Mathf.Log(mask.value, 2);
            go.SetLayerRecursively(layer);
        }

        /// <summary>
        /// Sets the layer of the gameObject and all of its children
        /// </summary>
        /// <param name="go">GameObject</param>
        /// <param name="layerNumber">Index of the layer</param>
        public static void SetLayerRecursively(this GameObject go, int layerNumber)
        {
            foreach (Transform trans in go.GetComponentsInChildren<Transform>(true))
            {
                trans.gameObject.layer = layerNumber;
            }
        }
    }
}