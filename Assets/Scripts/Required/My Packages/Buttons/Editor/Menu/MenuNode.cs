﻿using UnityEditor.Experimental.GraphView;

namespace RadioactiveGames.UI.Menu
{
    [System.Serializable]
    public class MenuNode : Node
    {
        /// <summary>
        /// Id for this node
        /// </summary>
        public int id;

        /// <summary>
        /// Name Id this nodes represents
        /// </summary>
        public string menuId;

        /// <summary>
        /// Checks if this menu's parent has to be open for this one to show up
        /// </summary>
        public bool requiresPreviousOpen = false;

        /// <summary>
        /// Checks if this is the root node
        /// </summary>
        public bool EntryPoint = false;
    }
}