﻿using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEditor.Experimental.GraphView;
using UnityEngine;
using UnityEngine.UIElements;
using RadioactiveGames.Common;

namespace RadioactiveGames.UI.Menu
{
    public class GraphSaveUtility
    {
        private MenuGraphView _targetView;
        private MenuNavigationAsset _assetCache;
        private List<Edge> Edges => _targetView.edges.ToList();
        private List<MenuNode> Nodes => _targetView.nodes.ToList().Cast<MenuNode>().ToList();

        public static GraphSaveUtility GetInstance(MenuGraphView view)
        {
            return new GraphSaveUtility { _targetView = view };
        }

        public void SaveGraph(string filename)
        {
            if (!Edges.Any()) return;

            var asset = ScriptableObject.CreateInstance<MenuNavigationAsset>();

            var connectedPorts = Edges.Where(x => x.input.node != null).ToArray();

            for (int i = 0; i < connectedPorts.Length; i++)
            {
                var outputNode = connectedPorts[i].output.node as MenuNode;
                var inputNode = connectedPorts[i].input.node as MenuNode;

                asset.links.Add(new NodeLinkData()
                {
                    baseNodeId = outputNode.id,
                    targetNodeGuid = inputNode.id,
                    portName = connectedPorts[i].output.portName
                });
            }
            foreach (var node in Nodes.Where(node => !node.EntryPoint))
            {
                asset.nodes.Add(new MenuNodeData
                {
                    id = node.id,
                    menuId = node.menuId,
                    requiresPreviousToBeOpen = node.requiresPreviousOpen,
                    position = node.GetPosition().position
                });
            }

            asset.Graph = CreateGraph(asset, true);

            if (!AssetDatabase.IsValidFolder("Assets/Game")) { AssetDatabase.CreateFolder("Assets", "Game"); }
            if (!AssetDatabase.IsValidFolder("Assets/Game/Menus")) { AssetDatabase.CreateFolder("Assets/Game", "Menus"); }
            AssetDatabase.CreateAsset(asset, $"Assets/Game/Menus/{filename}.asset");
            AssetDatabase.SaveAssets();
        }

        private List<MenuNavigationAsset.Node> CreateGraph(MenuNavigationAsset asset, bool bidirectional)
        {
            var graph = new List<MenuNavigationAsset.Node>();
            Dictionary<int, MenuNavigationAsset.Node> nodes = new Dictionary<int, MenuNavigationAsset.Node>();

            //Entry node
            MenuNavigationAsset.Node root = new MenuNavigationAsset.Node()
            {
                index = 0,
                id = asset.links[0].baseNodeId,
                neighbors = new List<int>()
            };

            graph.Add(root);
            nodes.Add(root.id, root);

            //Create nodes
            for (int i = 0; i < asset.nodes.Count; i++)
            {
                var node = asset.nodes[i];
                var n = new MenuNavigationAsset.Node()
                {
                    index = i + 1,
                    id = node.id,
                    menuNameId = node.menuId,
                    requiresPreviousOpen = node.requiresPreviousToBeOpen,
                    neighbors = new List<int>(),
                    MenuId = node.menuId.Hash(),
                };

                nodes.Add(n.id, n);
                //graph.Add(n);
            }

            //Add parent nodes
            asset.links.ForEach(edge =>
            {
                nodes[edge.baseNodeId].neighbors.Add(nodes[edge.targetNodeGuid].index);//Bi-directional
                if (bidirectional) nodes[edge.targetNodeGuid].neighbors.Add(nodes[edge.baseNodeId].index);//Bi-directional

                var node = nodes[edge.targetNodeGuid];
                node.parentNameId = nodes[edge.baseNodeId].menuNameId;
                node.ParentId = node.parentNameId.Hash();
                nodes[edge.targetNodeGuid] = node;
            });

            foreach (var n in nodes.Values)
            {
                if (n.index != 0) graph.Add(n);
            }

            nodes.Clear();

            return graph;
        }

        public void LoadGraph(string filename)
        {
            _assetCache = AssetDatabase.LoadAssetAtPath<MenuNavigationAsset>($"Assets/Game/Menus/{filename}.asset");
            if (_assetCache == null)
            {
                EditorUtility.DisplayDialog("File not found", "Target file does not exists", "ok");
                return;
            }

            ClearGraph();
            GenerateNodes();
            ConnectNodes();
        }

        private void ConnectNodes()
        {
            foreach (var n in Nodes)
            {
                var connections = _assetCache.links.Where(x => x.baseNodeId == n.id).ToList();
                connections.ForEach(c =>
                {
                    var targetNodeGuid = c.targetNodeGuid;
                    var targetNode = Nodes.Find(x => x.id == targetNodeGuid);
                    LinkNodes(n.outputContainer[0].Q<Port>(), targetNode.inputContainer[0].Q<Port>());
                    targetNode.SetPosition(new Rect(
                        _assetCache.nodes.Find(x => x.id == targetNodeGuid).position,
                        _targetView.defaultNodeSize
                        ));
                });
            }
        }

        private void LinkNodes(Port output, Port input)
        {
            var tempEdge = new Edge { output = output, input = input };
            tempEdge?.input.Connect(tempEdge);
            tempEdge?.output.Connect(tempEdge);
            _targetView.Add(tempEdge);
        }

        private void GenerateNodes()
        {
            foreach (var nodeData in _assetCache.nodes)
            {
                var tempNode = _targetView.CreateMenuNode(nodeData.menuId, nodeData.requiresPreviousToBeOpen);
                tempNode.id = nodeData.id;
                _targetView.AddElement(tempNode);
                //  _targetView.AddOutputPort(tempNode);
                /*
            var nodePorts = _assetCache.links.Where(x => x.baseNodeId == nodeData.id).ToList();
            nodePorts.ForEach(x => _targetView.AddOutputPort(tempNode));
*/
            }
        }

        private void ClearGraph()
        {
            Nodes.Find(x => x.EntryPoint).id = _assetCache.links[0].baseNodeId;

            foreach (var node in Nodes)
            {
                if (node.EntryPoint) continue;
                Edges.Where(x => x.input.node == node).ToList()
                    .ForEach(edge => { _targetView.RemoveElement(edge); });

                _targetView.RemoveElement(node);
            }
        }
    }
}