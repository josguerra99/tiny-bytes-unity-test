﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using RadioactiveGames.EditorTools;

namespace RadioactiveGames.UI
{
    [CustomEditor(typeof(ButtonEvents))]
    public class ButtonEventsEditor : UnityEditor.UI.ButtonEditor
    {
        private bool events = false;

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            this.serializedObject.Update();

            SerializedProperty onSelect = this.serializedObject.FindProperty("onSelect");
            SerializedProperty onDeselect = this.serializedObject.FindProperty("onDeselect");
            SerializedProperty onPointerDown = this.serializedObject.FindProperty("onPointerDown");
            SerializedProperty onPointerUp = this.serializedObject.FindProperty("onPointerUp");
            SerializedProperty selectOnHover = this.serializedObject.FindProperty("selectOnHover");

            EditorGUILayout.PropertyField(selectOnHover, new GUIContent("Select on hover"));
            EditorHelper.DrawSection("Events", () =>
            {
                EditorGUILayout.PropertyField(onSelect, new GUIContent("Select"));
                EditorGUILayout.PropertyField(onDeselect, new GUIContent("Deselect"));
                EditorGUILayout.PropertyField(onPointerDown, new GUIContent("Pointer Down"));
                EditorGUILayout.PropertyField(onPointerUp, new GUIContent("Pointer Up"));
            }, ref events, false);

            serializedObject.ApplyModifiedProperties();
        }
    }

    [CustomEditor(typeof(ButtonWithAnimator))]
    public class ButtonWithAnimatorEditor : ButtonEventsEditor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            this.serializedObject.Update();

            SerializedProperty clickAnim = this.serializedObject.FindProperty("clickAnim");
            SerializedProperty interactableAnim = this.serializedObject.FindProperty("interactableAnim");
            SerializedProperty clickAnimOnDisabled = this.serializedObject.FindProperty("disabedClickAnim");

            EditorGUILayout.PropertyField(clickAnim, new GUIContent("Click Animator"));
            EditorGUILayout.PropertyField(interactableAnim, new GUIContent("Interactable Animator"));
            EditorGUILayout.PropertyField(clickAnimOnDisabled, new GUIContent("Disabled Click Animator"));

            serializedObject.ApplyModifiedProperties();
        }
    }

    [CustomEditor(typeof(ButtonWithLoader))]
    public class ButtonWithLoaderEditor : ButtonWithAnimatorEditor
    {
        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            this.serializedObject.Update();

            SerializedProperty loaderAnim = this.serializedObject.FindProperty("_loadingAnimator");
            EditorGUILayout.PropertyField(loaderAnim, new GUIContent("Loader Animator"));

            serializedObject.ApplyModifiedProperties();
        }
    }
}