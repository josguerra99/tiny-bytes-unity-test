using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;

namespace RadioactiveGames.UI
{
    public class ButtonWithLoader : ButtonWithAnimator
    {
        [SerializeField] private Animation.UIAnimator _loadingAnimator;
        public bool IsLoading { get; protected set; }

        private Task _task;

        public void SetTask(Task task)
        {
            _task = task;
        }

        public async Task Execute()
        {
            if (IsLoading || _task == null)
                return;

            _loadingAnimator.Show();
            IsLoading = true;
            try
            {
                await _task;
            }
            catch (System.Exception)
            {
            }
            finally
            {
                _loadingAnimator.Hide();
                IsLoading = false;
            }
        }
    }
}