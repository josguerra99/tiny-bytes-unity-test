﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Events;

namespace RadioactiveGames.UI
{
    public class HoldButtonUI : Selectable, IBeginDragHandler, IDragHandler, IEndDragHandler
    {
        public Image fillImage;
        public float holdTime = 1f;
        private bool holding = false;
        public float holdPercentage = 0;
        public UnityEvent onHoldStart;
        public UnityEvent onHoldFinish;
        public UnityEvent onHoldCancel;

        [SerializeField] private AnimationCurve curve = new AnimationCurve(new Keyframe(0, 0), new Keyframe(1, 1));

        private bool oldHolding = false;

        protected override void Awake()
        {
            fillImage.fillAmount = curve.Evaluate(holdPercentage);
        }

        public void Update()
        {
            if (holding && interactable)
            {
                if (oldHolding != holding) onHoldStart?.Invoke();
                holdPercentage += Time.deltaTime / holdTime;
                fillImage.fillAmount = curve.Evaluate(holdPercentage);
            }

            if (oldHolding != holding) { oldHolding = holding; }
        }

        public override void OnPointerExit(PointerEventData eventData)
        {
#if UNITY_STANDALONE || UNITY_EDITOR
            base.OnPointerExit(eventData);
            if (!holding) return;
            holding = false;
            holdPercentage = 0;
            onHoldCancel?.Invoke();
            fillImage.fillAmount = holdPercentage;
#endif
        }

        public override void OnPointerDown(PointerEventData eventData)
        {
            holding = true;
            base.OnPointerDown(eventData);
        }

        public override void OnPointerUp(PointerEventData eventData)
        {
            if (!holding) return;
            if (holdPercentage >= 1)
            {
                onHoldFinish?.Invoke();
            }
            else
            {
                onHoldCancel?.Invoke();
            }

            holdPercentage = 0;
            fillImage.fillAmount = holdPercentage;
            holding = false;

            base.OnPointerUp(eventData);
        }

        public void OnBeginDrag(PointerEventData eventData)
        {
#if !UNITY_EDITOR && (  UNITY_IOS || UNITY_ANDROID)

            if (!holding) return;
            holding = false;
            holdPercentage = 0;
            onHoldCancel?.Invoke();
            fillImage.fillAmount = holdPercentage;
#endif
        }

        public void OnEndDrag(PointerEventData eventData)
        {
        }

        public void OnDrag(PointerEventData eventData)
        {
        }
    }
}