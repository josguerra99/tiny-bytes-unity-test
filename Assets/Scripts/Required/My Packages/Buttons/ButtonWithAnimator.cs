using UnityEngine;
using RadioactiveGames.UI.Animation;
using UnityEngine.EventSystems;

namespace RadioactiveGames.UI
{
    public class ButtonWithAnimator : ButtonEvents
    {
        public UIAnimator clickAnim;
        public UIAnimator disabedClickAnim;
        public UIAnimator interactableAnim;

        protected override void Start()
        {
            base.Start();

            if (Application.isPlaying)
            {
                onPointerDown.AddListener(clickAnim.Show);
                onPointerUp.AddListener(clickAnim.Hide);
            }
        }

        public override void OnPointerDown(PointerEventData eventData)
        {
            base.OnPointerDown(eventData);
            if (!interactable && disabedClickAnim != null)
                disabedClickAnim.Show();
        }

        public override void OnPointerUp(PointerEventData eventData)
        {
            base.OnPointerUp(eventData);

            if (!interactable && disabedClickAnim != null)
                disabedClickAnim.Hide();
        }

        protected override void DoStateTransition(SelectionState state, bool instant)
        {
            base.DoStateTransition(state, instant);

            if (interactableAnim == null)
                return;

            if (instant && Application.isPlaying)
            {
                if (state == SelectionState.Disabled)
                    interactableAnim.Show();
                else
                    interactableAnim.Hide();
            }
            else
            {
                if (state == SelectionState.Disabled)
                    interactableAnim.SkipAnimation(true);
                else
                    interactableAnim.SkipAnimation(false);
            }
        }
    }
}