using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class CanvasSafeZone : MonoBehaviour
{
    private static readonly List<CanvasSafeZone> helpers = new List<CanvasSafeZone>();

    public static System.Action OnResolutionOrOrientationChanged { get; set; }

    private static bool screenChangeVarsInitialized = false;
    private static ScreenOrientation lastOrientation = ScreenOrientation.Landscape;
    private static Vector2 lastResolution = Vector2.zero;
    private static Rect lastSafeArea = Rect.zero;

    [SerializeField] private RectTransform m_safeAreaTransform;
    [SerializeField] private Canvas m_canvas;
    [SerializeField] private RectTransform m_rectTransform;

    private void Reset()
    {
        if (!m_safeAreaTransform) m_safeAreaTransform = GetComponent<RectTransform>();
        if (!m_canvas) m_canvas = GetComponentInParent<Canvas>();
        if (!m_rectTransform) m_rectTransform = GetComponent<RectTransform>();
    }

    private void Awake()
    {
        if (!helpers.Contains(this)) helpers.Add(this);

        Reset();

        if (!screenChangeVarsInitialized)
        {
            lastOrientation = Screen.orientation;
            lastResolution.x = Screen.width;
            lastResolution.y = Screen.height;
            lastSafeArea = Screen.safeArea;
            screenChangeVarsInitialized = true;
        }

        ApplySafeArea();
    }

    private void Update()
    {
        if (helpers.Count > 0 && helpers[0] != this) return;//Actualizar valores estaticos unicamente con el index 0 de la lista

        if (Application.isMobilePlatform && Screen.orientation != lastOrientation)
            OrientationChanged();

        if (Screen.safeArea != lastSafeArea)
            SafeAreaChanged();

        if (Screen.width != lastResolution.x || Screen.height != lastResolution.y)
            ResolutionChanged();
    }

    private void ApplySafeArea()
    {
        if (m_safeAreaTransform == null || m_canvas == null || m_rectTransform == null)
            return;

        var safeArea = Screen.safeArea;
        var anchorMin = safeArea.position;
        var anchorMax = safeArea.position + safeArea.size;
        anchorMin.x /= m_canvas.pixelRect.width;
        anchorMin.y /= m_canvas.pixelRect.height;
        anchorMax.x /= m_canvas.pixelRect.width;
        anchorMax.y /= m_canvas.pixelRect.height;

        m_safeAreaTransform.anchorMin = anchorMin;
        m_safeAreaTransform.anchorMax = anchorMax;
    }

    private void OnDestroy()
    {
        if (helpers != null && helpers.Contains(this))
            helpers.Remove(this);
    }

    private static void OrientationChanged()
    {
        //Debug.Log("Orientation changed from " + lastOrientation + " to " + Screen.orientation + " at " + Time.time);

        lastOrientation = Screen.orientation;
        lastResolution.x = Screen.width;
        lastResolution.y = Screen.height;

        OnResolutionOrOrientationChanged?.Invoke();
    }

    private static void ResolutionChanged()
    {
        //Debug.Log("Resolution changed from " + lastResolution + " to (" + Screen.width + ", " + Screen.height + ") at " + Time.time);

        lastResolution.x = Screen.width;
        lastResolution.y = Screen.height;

        OnResolutionOrOrientationChanged?.Invoke();
    }

    private static void SafeAreaChanged()
    {
        // Debug.Log("Safe Area changed from " + lastSafeArea + " to " + Screen.safeArea.size + " at " + Time.time);

        lastSafeArea = Screen.safeArea;

        for (int i = 0; i < helpers.Count; i++)
            helpers[i].ApplySafeArea();
    }
}