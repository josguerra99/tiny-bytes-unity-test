using UnityEngine;
using UnityEngine.Playables;

namespace RadioactiveGames.UI.Animation.Timeline
{
    public class UIAnimatorClip : PlayableAsset
    {
        public bool show;

        public override Playable CreatePlayable(PlayableGraph graph, GameObject owner)
        {
            var playable = ScriptPlayable<UIAnimatorBehaviour>.Create(graph);
            var behaviour = playable.GetBehaviour();
            behaviour.show = show;
            return playable;
        }
    }
}