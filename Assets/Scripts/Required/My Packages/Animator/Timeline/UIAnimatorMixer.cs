using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Playables;
using RadioactiveGames.UI;

namespace RadioactiveGames.UI.Animation.Timeline
{
    public class UIAnimatorMixer : PlayableBehaviour
    {
        // Start is called before the first frame update
        public override void ProcessFrame(Playable playable, FrameData info, object playerData)
        {
            var anim = playerData as UIAnimator;
            bool show = false;

            if (anim == null) return;

            int inputCount = playable.GetInputCount();
            for (int i = 0; i < inputCount; i++)
            {
                float inputWeight = playable.GetInputWeight(i);
                if (inputWeight > 0f)//Get current clip
                {
                    var inputPlayable = (ScriptPlayable<UIAnimatorBehaviour>)playable.GetInput(i);
                    var inputBehaviour = inputPlayable.GetBehaviour();
                    show = true;
                }
            }

            if (Application.isPlaying)
            {
                if (show) anim.Show();
                else anim.Hide();
            }
            else
            {
                anim.SkipAnimation(show);
            }
        }
    }
}