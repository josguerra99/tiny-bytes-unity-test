using UnityEngine;
using UnityEngine.Playables;
using UnityEngine.Timeline;
using RadioactiveGames.UI;

namespace RadioactiveGames.UI.Animation.Timeline
{
    [TrackBindingType(typeof(UIAnimator))]
    [TrackClipType(typeof(UIAnimatorClip))]
    public class UIAnimatorTrack : TrackAsset
    {
        public override Playable CreateTrackMixer(PlayableGraph graph, GameObject go, int inputCount)
        {
            return ScriptPlayable<UIAnimatorMixer>.Create(graph, inputCount);
        }
    }
}