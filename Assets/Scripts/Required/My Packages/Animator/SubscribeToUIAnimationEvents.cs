﻿using UnityEngine;
using UnityEngine.Events;

namespace RadioactiveGames.UI.Animation
{
    public class SubscribeToUIAnimationEvents : MonoBehaviour
    {
        public UIAnimator anim;
        public UnityEvent onStartShow;
        public UnityEvent onStartHide;

        public UnityEvent onFinishShow;
        public UnityEvent onFinishHide;

        //public bool dontTriggerOnSameState = false;

        private void Reset()
        {
            anim = GetComponent<UIAnimator>();
        }

        private void Awake()
        {
            anim.OnAnimationStart += HandleOnStart;
            anim.OnAnimationFinish += HandleOnFinish;
        }

        private void HandleOnStart(bool val)
        {
            if (val) { onStartShow?.Invoke(); }
            else { onStartHide?.Invoke(); }
        }

        private void HandleOnFinish(bool val)
        {
            if (val) { onFinishShow?.Invoke(); }
            else onFinishHide?.Invoke();
        }

        private void OnDestroy()
        {
            if (anim == null) return;
            anim.OnAnimationStart -= HandleOnStart;
            anim.OnAnimationFinish -= HandleOnFinish;
        }
    }
}