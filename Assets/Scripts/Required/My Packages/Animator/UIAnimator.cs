﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace RadioactiveGames.UI.Animation
{
    [DefaultExecutionOrder(-10)]
    public class UIAnimator : MonoBehaviour
    {
        [SerializeField] private List<UIAnimation> animations = new List<UIAnimation>();
        [SerializeField] private bool initialValue = false;
        [SerializeField] private GameObject objectToDisable = null;
        [SerializeField] private UpdateType currentUpdateType = UpdateType.LateUpdate;
        [SerializeField] private bool useUnscaledDeltaTime = false;

        public Action<bool> OnAnimationStart { get; set; }
        public Action<bool> OnAnimationFinish { get; set; }

        private Action OnFinishTmp { get; set; }
        public bool IsShowing { get; private set; } = false;
        public bool Animating { get; private set; }
        private bool HasEvents { get; set; } = false;
        public UIAnimation LongestAnimation { get; private set; } = null;

        private float t;
        private float targetTime;

        public enum UpdateType
        {
            Update,
            LateUpdate,
            Coroutine
        }

        private void Reset()
        {
            animations.AddRange(GetComponentsInChildren<UIAnimation>());
        }

        private void OnDestroy()
        {
            StopUpdateCoroutine();
        }

        private void Awake()
        {
            Init();
        }

        /// <summary>
        /// Initialize the animations and also finds the longest animation
        /// </summary>
        /// <param name="anim"></param>
        private void InitAnimation(UIAnimation anim)
        {
            anim.Init(initialValue);

            if (LongestAnimation == null || anim.TotalTime > LongestAnimation.TotalTime)
                LongestAnimation = anim;
        }

        /// <summary>
        /// Initializes the animator and its animations, also adds events
        /// </summary>
        private void Init()
        {
            IsShowing = initialValue;

            for (int i = 0; i < animations.Count; i++)
            {
                var animation = animations[i];
                InitAnimation(animation);
            }

            if (objectToDisable)
                objectToDisable.SetActive(IsShowing);

            t = targetTime = GetTargetTime(IsShowing);

            if (!HasEvents)
            {
                OnAnimationFinish += HandleAnimationFinish;
                OnAnimationStart += HandleAnimationStart;
            }

            if (currentUpdateType == UpdateType.Coroutine)
            {
                StartUpdateCoroutine();
            }

            HasEvents = true;
        }

        /// <summary>
        /// Toggles between show and hide states
        /// </summary>
        public void Toggle()
        {
            if (!IsShowing) { Show(); } else { Hide(); }
        }

        /// <summary>
        /// Starts animating this animator
        /// </summary>
        /// <param name="showVal">Value for showing or hidding</param>
        public void StartAnimation(bool showVal)
        {
            if (animations.Count == 0)
                return;

            //If this object is disabled in the hierarchy then is safe to skip this animation
            if (!gameObject.activeInHierarchy)
            {
                SkipAnimation(showVal);
                return;
            }

            //If its already at the same state then we can ignore the animation
            if (IsShowing == showVal)
                return;

            IsShowing = showVal;
            targetTime = GetTargetTime(IsShowing);
            OnAnimationStart?.Invoke(showVal);
        }

        /// <summary>
        /// Animates this animator
        /// </summary>
        public void Animate()
        {
            //Finish this animation
            if (Common.Math.MathExtensions.Approximately(t, targetTime, 0.1f))
            {
                t = targetTime;
                UpdateAnimations(t);
                OnAnimationFinish?.Invoke(IsShowing);
                return;
            }

            UpdateAnimations(t);

            if (t <= targetTime)
                t += useUnscaledDeltaTime ? Time.unscaledDeltaTime : Time.deltaTime;
            else
                t -= useUnscaledDeltaTime ? Time.unscaledDeltaTime : Time.deltaTime;
        }

        /// <summary>
        /// Updates the animation states of every animation
        /// </summary>
        private void UpdateAnimations(float t)
        {
            for (int i = 0; i < animations.Count; i++)
            {
                animations[i].AnimateWithTime(t);
            }
        }

        /// <summary>
        /// Handles default event calls for OnStartAnimation
        /// </summary>
        /// <param name="val"></param>
        private void HandleAnimationStart(bool val)
        {
            Animating = true;

            if (val && objectToDisable)
                objectToDisable.SetActive(true);
        }

        /// <summary>
        /// Handles default event calls for OnAnimationFinish
        /// </summary>
        /// <param name="val"></param>
        private void HandleAnimationFinish(bool showing)
        {
            OnFinishTmp?.Invoke();

            Animating = false;
            t = targetTime;

            if (!showing && objectToDisable)
                objectToDisable.SetActive(false);
        }

        /// <summary>
        /// Shows the current animator
        /// </summary>
        /// <param name="callback"></param>
        public void Show(System.Action callback)
        {
            OnFinishTmp = null;

            void handleCallback()
            {
                callback?.Invoke();
                OnFinishTmp -= handleCallback;
            }

            OnFinishTmp += handleCallback;

            Show();
        }

        /// <summary>
        /// Hides the current animator
        /// </summary>
        /// <param name="callback"></param>
        public void Hide(Action callback)
        {
            OnFinishTmp = null;

            void handleCallback()
            {
                callback?.Invoke();
                OnFinishTmp -= handleCallback;
            }

            OnFinishTmp += handleCallback;

            Hide();
        }

        /// <summary>
        /// Shows the current animator
        /// </summary>
        public void Show()
        {
            StartAnimation(true);
        }

        /// <summary>
        /// Hides the current animator
        /// </summary>
        public void Hide()
        {
            StartAnimation(false);
        }

        /// <summary>
        /// Skips the animation and just shows the final result
        /// </summary>
        /// <param name="show">Show or hide</param>
        public void SkipAnimation(bool show)
        {
            for (int i = 0; i < animations.Count; i++)
            {
                var animation = animations[i];
                animation.SkipAnimation(show);
            }

            IsShowing = show;

            OnAnimationStart?.Invoke(show);
            OnAnimationFinish?.Invoke(show);

            if (!Application.isPlaying && objectToDisable)
                objectToDisable.gameObject.SetActive(show);
        }

        /// <summary>
        /// Gets the time that the current animation has to go
        /// </summary>
        /// <param name="showing"></param>
        /// <returns></returns>
        private float GetTargetTime(bool showing) => IsShowing ? LongestAnimation.TotalTime : 0f;

        public void AddAnimation(UIAnimation anim)
        {
            animations.Add(anim);
        }

        public void RemoveAnimation(UIAnimation anim)
        {
            animations.Remove(anim);
        }

        #region Update Methods

#if UNITY_EDITOR
        public bool toggle = false;
#endif

        private void Update()
        {
#if UNITY_EDITOR
            if (toggle)
            {
                Toggle();
                toggle = false;
            }
#endif

            if (Animating && currentUpdateType == UpdateType.Update)
                Animate();
        }

        private void LateUpdate()
        {
            if (Animating && currentUpdateType == UpdateType.LateUpdate)
                Animate();
        }

        private IEnumerator updateCoroutine = null;

        private void StartUpdateCoroutine()
        {
            StopUpdateCoroutine();
            updateCoroutine = UpdateCoroutine();
            CoroutineExecuter.Execute(updateCoroutine);
        }

        private void StopUpdateCoroutine()
        {
            if (updateCoroutine != null)
                CoroutineExecuter.Stop(updateCoroutine);
        }

        private IEnumerator UpdateCoroutine()
        {
            if (currentUpdateType != UpdateType.Coroutine)
                yield break;

            while (true)
            {
                if (Animating)
                    Animate();
                yield return null;
            }
        }

        #endregion Update Methods
    }
}