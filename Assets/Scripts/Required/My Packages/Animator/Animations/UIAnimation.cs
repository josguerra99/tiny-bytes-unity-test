﻿using UnityEngine;

namespace RadioactiveGames.UI.Animation
{
    /// <summary>
    /// Base class for an animation
    /// </summary>
    public abstract class UIAnimation : MonoBehaviour
    {
        [SerializeField] protected float m_duration = 0.25f;
        [SerializeField] protected float m_delay = 0f;
        [SerializeField] protected CustomAnimationCurve customCurve;

        /// <summary>
        /// Inverts the curve value when the animation is hiding (for example, its useful with a bouce ease out)
        /// </summary>
        [SerializeField] protected bool invertCurveOnHide = false;

        // [SerializeField] protected AnimationCurve customCurve = new AnimationCurve(new Keyframe(0, 0), new Keyframe(1, 1));

        /// <summary>
        /// Duration of the animation without the delay
        /// </summary>
        public float Duration => m_duration;

        /// <summary>
        /// Time the animation takes to start
        /// </summary>
        public float Delay => m_delay;

        /// <summary>
        /// Total time the animation takes to complete (duration + delay)
        /// </summary>
        public float TotalTime => m_totalTime;

        private float m_totalTime = 0f;

        /// <summary>
        /// Checks if the animation is showing or hiding
        /// </summary>
        public bool IsShowing { get; protected set; } = false;

        protected virtual void Reset()
        {
            var anim = GetComponent<UIAnimator>();
            if (anim) anim.AddAnimation(this);
        }

        private void OnDestroy()
        {
            var anim = GetComponent<UIAnimator>();
            if (anim) anim.RemoveAnimation(this);
        }

        /// <summary>
        /// Initializes the current animation
        /// </summary>
        /// <param name="initialValue"></param>
        public virtual void Init(bool initialValue)
        {
            m_totalTime = m_duration + m_delay;
            SkipAnimation(initialValue);
        }

        /// <summary>
        /// Animates based on an animator time
        /// </summary>
        /// <param name="t"></param>
        public void AnimateWithTime(float t)
        {
            AnimateWithPercentage(ConverToPercentage(t));
        }

        /// <summary>
        /// Animates based on a percentage value between 0 and 1
        /// </summary>
        /// <param name="percentage"></param>
        public virtual void AnimateWithPercentage(float percentage)
        {
            if (percentage == 0f && IsShowing)
            {
                UpdateShowingValue(false);
            }

            if (percentage == 1f && !IsShowing)
            {
                UpdateShowingValue(true);
            }
        }

        /// <summary>
        /// Updates the showing value for this animation, it can be overrided to add events when the animation
        /// showing value changes
        /// </summary>
        /// <param name="value"></param>
        protected virtual void UpdateShowingValue(bool value)
        {
            IsShowing = value;

            if (invertCurveOnHide)
                isCurveInverted = IsShowing;
        }

        /// <summary>
        /// Skips the animation part and shows the final result
        /// </summary>
        /// <param name="show"></param>
        public virtual void SkipAnimation(bool show)
        {
            UpdateShowingValue(show);
            AnimateWithPercentage(IsShowing ? 1f : 0f);
        }

        /// <summary>
        /// Converts the animator time to a percentage
        /// </summary>
        /// <param name="t"></param>
        protected float ConverToPercentage(float t)
        {
            float time = Mathf.Clamp(t - Delay, 0f, Duration);
            return Mathf.Clamp01(time / Duration);
        }

        /// <summary>
        /// Converts the percentage to a curve value, this way the animation can be modified just by changing that curve value
        /// </summary>
        /// <param name="percentage"></param>
        /// <returns></returns>
        protected float GetCurveValue(float percentage)
        {
            if (isCurveInverted)
            {
                return 1f - customCurve.Evaluate(1f - percentage);
            }

            return customCurve.Evaluate(percentage);
        }

        private bool isCurveInverted = false;
    }
}