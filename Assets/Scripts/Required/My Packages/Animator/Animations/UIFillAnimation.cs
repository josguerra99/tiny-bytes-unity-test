﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace RadioactiveGames.UI.Animation
{
    public class UIFillAnimation : UIAnimation
    {
        [SerializeField] private float m_from = 0;
        [SerializeField] private float m_to = 1;
        [SerializeField] private Image m_image;

        public override void AnimateWithPercentage(float percentage)
        {
            base.AnimateWithPercentage(percentage);
            m_image.fillAmount = Mathf.Lerp(m_from, m_to, GetCurveValue(percentage));
        }
    }
}