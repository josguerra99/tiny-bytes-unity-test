using System.Collections.Generic;
using UnityEngine;

namespace RadioactiveGames.UI.Animation
{
    public class UISizeAnimation : UIAnimation
    {
        [SerializeField] private Vector2 m_from;
        [SerializeField] private Vector2 m_to;
        [SerializeField] private RectTransform m_rt;
        [SerializeField] private List<RectTransform> m_layoutsToRebuild;

        public override void AnimateWithPercentage(float percentage)
        {
            base.AnimateWithPercentage(percentage);
            m_rt.sizeDelta = Vector2.Lerp(m_from, m_to, GetCurveValue(percentage));
            Rebuild();
        }

        protected override void UpdateShowingValue(bool value)
        {
            base.UpdateShowingValue(value);
            Rebuild();
        }

        public override void SkipAnimation(bool show)
        {
            base.SkipAnimation(show);
            Rebuild();
        }

        private void Rebuild()
        {
            for (int i = 0; i < m_layoutsToRebuild.Count; i++)
            {
                UnityEngine.UI.LayoutRebuilder.ForceRebuildLayoutImmediate(m_layoutsToRebuild[i]);
            }
        }
    }
}