using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RadioactiveGames.UI.Animation
{
    public class UITextColorAnimation : UIAnimation
    {
        [SerializeField] private TMPro.TextMeshProUGUI m_graphic;
        [SerializeField] private Color m_from;
        [SerializeField] private Color m_to;

        public override void AnimateWithPercentage(float percentage)
        {
            base.AnimateWithPercentage(percentage);
            m_graphic.color = Color.Lerp(m_from, m_to, GetCurveValue(percentage));
        }
    }
}