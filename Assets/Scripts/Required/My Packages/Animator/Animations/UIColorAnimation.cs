﻿using UnityEngine;
using UnityEngine.UI;

namespace RadioactiveGames.UI.Animation
{
    public class UIColorAnimation : UIAnimation
    {
        [SerializeField] private Color m_from;
        [SerializeField] private Color m_to;
        [SerializeField] private Graphic m_graphic;

        public override void AnimateWithPercentage(float percentage)
        {
            base.AnimateWithPercentage(percentage);
            m_graphic.color = Color.Lerp(m_from, m_to, GetCurveValue(percentage));
        }
    }
}