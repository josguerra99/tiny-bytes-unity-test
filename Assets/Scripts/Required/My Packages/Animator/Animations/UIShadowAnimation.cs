using UnityEngine;

namespace RadioactiveGames.UI.Animation
{
    public class UIShadowAnimation : UIAnimation
    {
        [SerializeField] private UnityEngine.UI.Shadow m_graphic;
        [SerializeField] private Color m_colorFrom;
        [SerializeField] private Color m_colorTo;
        [SerializeField] private Vector2 m_positionFrom;
        [SerializeField] private Vector2 m_positionTo;

        public override void AnimateWithPercentage(float percentage)
        {
            base.AnimateWithPercentage(percentage);
            float val = GetCurveValue(percentage);
            m_graphic.effectColor = Color.Lerp(m_colorFrom, m_colorTo, val);
            m_graphic.effectDistance = Vector2.Lerp(m_positionFrom, m_positionTo, val);
        }
    }
}