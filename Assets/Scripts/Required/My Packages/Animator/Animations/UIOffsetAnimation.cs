using UnityEngine;

namespace RadioactiveGames.UI.Animation
{
    public class UIOffsetAnimation : UIAnimation
    {
        [Tooltip("Left , Bottom"), SerializeField, Header("From")]
        private Vector2 m_offsetMinFrom;

        [Tooltip("Right, Up"), SerializeField]
        private Vector2 m_offsetMaxFrom;

        [Tooltip("Left , Bottom"), SerializeField, Header("To")]
        private Vector2 m_offsetMinTo;

        [Tooltip("Right , Up"), SerializeField]
        private Vector2 m_offsetMaxTo;

        [SerializeField] private RectTransform rt;

        public override void AnimateWithPercentage(float percentage)
        {
            base.AnimateWithPercentage(percentage);
            float val = GetCurveValue(percentage);
            rt.offsetMin = Vector2.Lerp(m_offsetMinFrom, m_offsetMinTo, val);
            rt.offsetMax = Vector2.Lerp(m_offsetMaxFrom, m_offsetMaxTo, val);
        }
    }
}