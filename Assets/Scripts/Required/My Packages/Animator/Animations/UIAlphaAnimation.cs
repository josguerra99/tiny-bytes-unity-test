﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RadioactiveGames.UI.Animation
{
    public class UIAlphaAnimation : UIAnimation
    {
        [SerializeField] private CanvasGroup m_cg;
        [SerializeField] private float m_from = 0;
        [SerializeField] private float m_to = 1;
        [SerializeField] private bool m_changeInteractableStatus = true;
        [SerializeField] private bool m_alwaysInteractable = false;

        public override void AnimateWithPercentage(float percentage)
        {
            base.AnimateWithPercentage(percentage);
            m_cg.alpha = Mathf.Lerp(m_from, m_to, GetCurveValue(percentage));
        }

        public override void SkipAnimation(bool show)
        {
            base.SkipAnimation(show);
            SetInteraction();
        }

        protected override void UpdateShowingValue(bool value)
        {
            base.UpdateShowingValue(value);
            SetInteraction();
        }

        public void SetInteraction()
        {
            if (m_alwaysInteractable)
            {
                m_cg.blocksRaycasts = m_cg.interactable = true;
                return;
            }
            if (m_changeInteractableStatus)
            {
                m_cg.blocksRaycasts = m_cg.interactable = IsShowing;
                return;
            }
        }
    }
}