﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RadioactiveGames.UI.Animation
{
    public class UIPositionAnimation : UIAnimation
    {
        [SerializeField] private RectTransform m_rt;
        [SerializeField] private Vector2 m_from;
        [SerializeField] private Vector2 m_to;

        public override void AnimateWithPercentage(float percentage)
        {
            base.AnimateWithPercentage(percentage);
            m_rt.anchoredPosition = Vector2.Lerp(m_from, m_to, GetCurveValue(percentage));
        }
    }
}