﻿using UnityEngine;

namespace RadioactiveGames.UI.Animation
{
    public class UIZoomAnimation : UIAnimation
    {
        [SerializeField] private RectTransform m_rt;
        [SerializeField] private Vector3 m_from = Vector3.zero;
        [SerializeField] private Vector3 m_to = Vector3.one;

        public override void AnimateWithPercentage(float percentage)
        {
            base.AnimateWithPercentage(percentage);
            m_rt.localScale = Vector3.Lerp(m_from, m_to, GetCurveValue(percentage));
        }
    }
}