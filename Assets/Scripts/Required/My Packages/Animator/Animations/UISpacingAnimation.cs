﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace RadioactiveGames.UI.Animation
{
    public class UISpacingAnimation : UIAnimation
    {
        [SerializeField] private float m_from = 0;
        [SerializeField] private float m_to = 10;
        [SerializeField] private HorizontalOrVerticalLayoutGroup m_layoutGroup;
        [SerializeField] private List<RectTransform> m_layoutsToRebuild;

        protected override void UpdateShowingValue(bool value)
        {
            base.UpdateShowingValue(value);
            Rebuild();
        }

        public override void AnimateWithPercentage(float percentage)
        {
            base.AnimateWithPercentage(percentage);
            m_layoutGroup.spacing = Mathf.Lerp(m_from, m_to, GetCurveValue(percentage));
            Rebuild();
        }

        public override void SkipAnimation(bool show)
        {
            base.SkipAnimation(show);
            Rebuild();
        }

        private void Rebuild()
        {
            for (int i = 0; i < m_layoutsToRebuild.Count; i++)
            {
                LayoutRebuilder.ForceRebuildLayoutImmediate(m_layoutsToRebuild[i]);
            }
        }
    }
}