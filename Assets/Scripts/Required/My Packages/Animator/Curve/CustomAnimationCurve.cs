using UnityEngine;

namespace RadioactiveGames.UI.Animation
{
    [System.Serializable]
    public class CustomAnimationCurve

    {
        /// <summary>
        /// Equation to use when animating (or a custom curve)
        /// </summary>
        public AnimationsCurvesDB.Equations equation = AnimationsCurvesDB.Equations.QuadEaseOut;

        /// <summary>
        /// When equation is set to custom curve, this curve will be used
        /// </summary>
        public AnimationCurve customCurve;

        /// <summary>
        /// Gets the value from a equation or the custom curve
        /// </summary>
        /// <param name="t">Current time</param>
        /// <returns></returns>
        public float Evaluate(float t)
        {
            if (equation == AnimationsCurvesDB.Equations.Custom)
                return customCurve.Evaluate(t);

            return (float)AnimationsCurvesDB.GetValue(equation, t);
        }
    }
}