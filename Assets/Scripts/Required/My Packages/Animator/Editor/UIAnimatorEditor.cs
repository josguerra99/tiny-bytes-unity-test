﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;
using RadioactiveGames.EditorTools;

namespace RadioactiveGames.UI.Animation
{
    [CanEditMultipleObjects, CustomEditor(typeof(UIAnimator))]
    public class UIAnimatorEditor : CustomEditorBase
    {
        private ReorderableList m_reorderableList;

        public void SetupSpawnHelperList()
        {
            var listProperty = serializedObject.FindProperty("animations");
            m_reorderableList = new ReorderableList(serializedObject, listProperty, true, true, true, true);

            m_reorderableList.drawHeaderCallback += (Rect rect) =>
            {
                Rect lineRect = rect;
                EditorGUI.LabelField(lineRect, "ANIMATIONS", EditorHelper.CenteredStyle(10, true));
            };

            //Dibujar el elemento de la lista
            m_reorderableList.drawElementCallback += (Rect rect, int index, bool isActive, bool isFocused) =>
            {
                float lineHeight = EditorGUIUtility.singleLineHeight;
                float oldWidth = EditorGUIUtility.labelWidth;
                //Elemento de la lista
                var prop = listProperty.GetArrayElementAtIndex(index);
                var linerect = rect;
                EditorGUI.PropertyField(rect, prop, new GUIContent());

                EditorGUIUtility.labelWidth = oldWidth;
            };

            m_reorderableList.elementHeightCallback = (int index) =>
            {
                return EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing * 2f;
            };
        }

        private void OnEnable()
        {
            SetupSpawnHelperList();
        }

        private bool showAnimations = false;
        private bool showMore = false;

        public override void OnInspectorGUI()
        {
            base.OnInspectorGUI();
            serializedObject.Update();

            EditorHelper.DrawSection("Values", () =>
            {
                EditorGUILayout.PropertyField(serializedObject.FindProperty("initialValue"));
                EditorGUILayout.PropertyField(serializedObject.FindProperty("currentUpdateType"));
                EditorGUILayout.PropertyField(serializedObject.FindProperty("useUnscaledDeltaTime"));
            }, true);

            EditorHelper.DrawSection("Animations", () =>
            {
                m_reorderableList.DoLayoutList();
            }, ref showAnimations, false);

            EditorHelper.DrawSection("More", () =>
            {
                EditorGUILayout.PropertyField(serializedObject.FindProperty("objectToDisable"));
            }, ref showMore, true);

            EditorHelper.Divider();

            if (Application.isPlaying)
            {
                EditorHelper.DrawSection("Status Info", () =>
                {
                    EditorGUI.BeginDisabledGroup(true);
                    EditorGUILayout.Toggle("Is Showing", (serializedObject.targetObject as UIAnimator).IsShowing);
                    EditorGUILayout.Toggle("Is Animating", (serializedObject.targetObject as UIAnimator).Animating);
                    EditorGUI.EndDisabledGroup();
                });
            }

            if (GUILayout.Button("Toggle"))
            {
                (serializedObject.targetObject as UIAnimator).Toggle();
            }

            serializedObject.ApplyModifiedProperties();
        }
    }
}