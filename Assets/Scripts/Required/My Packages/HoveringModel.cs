﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoveringModel : MonoBehaviour
{
    public float amplitude = 0.7f;
    public float frequency = 0.25f;
    private Vector3 startPos;
    private bool startWaving = false;
    private float startTime = 0f;    // Update is called once per frame
    public bool useLocal = false;
    public Vector3 direction = Vector3.up;

    public void Awake()
    {
        //this.ExecuteLater(() =>
        //{
        startPos = useLocal ? transform.localPosition : transform.position;
        startWaving = true;
        startTime = Time.time;
        //}, Random.Range(0.02f, frequency));
    }

    private void Update()
    {
        float y = amplitude * Mathf.Sin((Time.time - startTime) * frequency * 2 * Mathf.PI);
        if (startWaving)
        {
            var pos = startPos + direction * y;
            if (useLocal) transform.localPosition = pos;
            else transform.position = pos;
        }
    }
}