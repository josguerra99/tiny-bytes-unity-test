using UnityEngine;


/// <summary>
/// Base class for creating a singleton of an scriptable object
/// </summary>
/// <typeparam name="T"></typeparam>
public class ScriptableSingleton<T> : ScriptableObject where T : ScriptableObject
{
    private static T _instance = null;

    public static T Instance
    {
        get
        {
            if (_instance == null)
            {
                var resources = Resources.LoadAll("Singletons/", typeof(T));

                if (resources.Length > 0)
                    _instance = (T)resources[0];
            }

            if (_instance == null)
            {
                _instance = CreateInstance<T>();
                Debug.Log($"Singleton for {typeof(T)} has been created in memory " +
                    $" you may want to create an scriptable object inside Resources/Singletons/{typeof(T)}.asset");
            }
            return _instance;
        }
    }
}