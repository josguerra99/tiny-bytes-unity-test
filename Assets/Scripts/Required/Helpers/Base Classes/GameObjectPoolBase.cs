using UnityEngine;
using UnityEngine.Pool;

/// <summary>
/// Pool of game objects
/// </summary>
/// <typeparam name="T">Component to use</typeparam>
public class GameObjectPoolBase<T> where T : Component
{
    private readonly ObjectPool<T> _pool;

    /// <summary>
    /// Prefab to instantiate
    /// </summary>
    private T _prefab;

    /// <summary>
    /// Transform to store the gameObjects
    /// </summary>
    private Transform _container;

    public ObjectPool<T> Pool => _pool;

    public GameObjectPoolBase(T prefab)
    {
        _prefab = prefab;
        _pool = new ObjectPool<T>(CreateFunction, GetFunction, ReleaseFunction, DestroyFunction, false, 5, 1000);
        _container = new GameObject($"Pool [{_prefab.name}]").transform;
    }

    /// <summary>
    /// Handles the object destruction
    /// </summary>
    /// <param name="obj"></param>
    protected virtual void DestroyFunction(T obj)
    {
        if (obj != null)
            Object.Destroy(obj.gameObject);
    }

    /// <summary>
    /// Retrieves an object that is being used and sends it back to the pool
    /// </summary>
    /// <param name="obj"></param>
    protected void ReleaseFunction(T obj)
    {
        obj.gameObject.SetActive(false);
    }

    /// <summary>
    /// Gets an object from the pool
    /// </summary>
    /// <param name="obj"></param>
    protected void GetFunction(T obj)
    {
        obj.gameObject.SetActive(true);
    }

    /// <summary>
    /// Handles the creation of object (when a pool needs a new object it will be called this method
    /// to obtain a newly created object)
    /// </summary>
    /// <returns></returns>
    protected virtual T CreateFunction()
    {
        var item = UnityEngine.Object.Instantiate(_prefab);
        item.transform.SetParent(_container);
        return item;
    }

    /// <summary>
    /// Must be called after this pool won't be used anymore
    /// </summary>
    public virtual void Dispose()
    {
        if (_pool != null)
            _pool.Dispose();

        if (_container != null)
            Object.Destroy(_container.gameObject);
    }
}