using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR

using UnityEditor;

#endif

/// <summary>
/// An scriptable object that contains other scriptable objects inside
/// itself, it also implements an IData interface so you can create
/// a ScriptableDatabase using this container
/// </summary>
/// <typeparam name="T">Type of scriptable object to contain</typeparam>
public class ContainerScriptableObject<T> : ScriptableObject, Utilities.IData where T : ScriptableObject
{
    [SerializeField] private string _id;
    [SerializeField] protected List<T> _items = new List<T>();

    /// <summary>
    /// Gets the child scriptable object that is at index
    /// </summary>
    /// <param name="index"></param>
    /// <returns></returns>
    public T GetAt(int index) => _items[index];

    public int Count => _items == null ? 0 : _items.Count;

    #region ID

    public string Id => _id;

    private int _idHash = -1;

    public int IdHash
    {
        get
        {
            if (_idHash == -1)
                _idHash = Animator.StringToHash(Id);
            return _idHash;
        }
    }

    #endregion ID

#if UNITY_EDITOR

    /// <summary>
    /// Adds a new item
    /// </summary>
    public virtual void Add(T assetToCreateInstance = null)
    {
        T asset;

        if (_items.Count > 0)
            asset = Instantiate(_items[_items.Count - 1]);
        else
        {
            if (assetToCreateInstance == null)
                asset = CreateInstance<T>();
            else
                asset = assetToCreateInstance;
        }

        asset.name = $"{Id}.{_items.Count}";
        _items.Add(asset);
        AssetDatabase.AddObjectToAsset(asset, this);
        AssetDatabase.SaveAssets();
        EditorUtility.SetDirty(this);
        EditorUtility.SetDirty(asset);
    }

    /// <summary>
    /// Removes an item from this container
    /// </summary>
    /// <param name="asset">Asset to remove</param>
    public void Remove(T asset)
    {
        var index = _items.FindIndex(x => x == asset);
        if (index == -1)
            return;

        _items.RemoveAt(index);
        Undo.DestroyObjectImmediate(asset);
        AssetDatabase.SaveAssets();

        for (int i = 0; i < _items.Count; i++)
        {
            var assetToRename = _items[i];
            assetToRename.name = $"{Id}.{i}";
            AssetDatabase.SaveAssets();
            EditorUtility.SetDirty(assetToRename);
        }
    }

#endif
}