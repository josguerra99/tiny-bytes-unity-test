using UnityEngine;
using UnityEditor;

public abstract class ContainerScriptableObjectEditor<T> : Editor where T : ScriptableObject
{
    protected abstract ContainerScriptableObject<T> Script { get; }
    private SerializedProperty _items;

    protected virtual void OnEnable()
    {
        _items = serializedObject.FindProperty("_items");
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        serializedObject.Update();

        GUILayout.Space(EditorGUIUtility.singleLineHeight * 2);

        for (int i = 0; i < _items.arraySize; i++)
        {
            GUILayout.BeginHorizontal();
            EditorGUILayout.PropertyField(_items.GetArrayElementAtIndex(i));

            int index = i;
            if (GUILayout.Button("Remove"))
            {
                Script.Remove(Script.GetAt(index));
            }

            GUILayout.EndHorizontal();
        }

        GUILayout.Space(EditorGUIUtility.singleLineHeight);

        if (Script.Count == 0)
        {
            DrawAddButtonsSections();
        }
        else
        {
            if (GUILayout.Button("Add Item"))
            {
                Script.Add();
            }
        }

        serializedObject.ApplyModifiedProperties();
    }

    protected virtual void DrawAddButtonsSections()
    {
        DrawAddButton<T>("Add Item");
    }

    protected virtual void DrawAddButton<T1>(string text) where T1 : T
    {
        if (GUILayout.Button(text))
        {
            Script.Add(CreateInstance<T1>());
        }
    }
}