using System.Collections.Generic;
using UnityEngine;

namespace Utilities
{
    /// <summary>
    /// Scriptable object that contains a list and a dictionary,
    /// its also a singleton for easy access from any script
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class ScriptableDBBase<T> : ScriptableSingleton<ScriptableDBBase<T>> where T : IData
    {
        [SerializeField] protected List<T> _list = new List<T>();
        [System.NonSerialized] private Dictionary<int, T> _db = null;

        public Dictionary<int, T> DB
        {
            get
            {
                if (_db == null)
                {
                    _db = new Dictionary<int, T>();
                    for (int i = 0; i < _list.Count; i++)
                    {
                        var item = _list[i];
                        _db.Add(item.IdHash, item);
                    }
                }
                return _db;
            }
        }

        public T GetItem(int hash)
        {
            if (DB.ContainsKey(hash))
            {
                return DB[hash];
            }

            return default;
        }

        public T GetItem(string id)
        {
            return GetItem(Animator.StringToHash(id));
        }

        public bool Contains(int hash)
        {
            return DB.ContainsKey(hash);
        }

        public bool Contains(string id)
        {
            return Contains(Animator.StringToHash(id));
        }

        public List<T> List => _list;
    }

    public interface IData
    {
        string Id { get; }
        int IdHash { get; }
    }
}