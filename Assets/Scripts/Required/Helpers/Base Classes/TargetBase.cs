using UnityEngine;

public abstract class TargetBase : MonoBehaviour
{
    /// <summary>
    /// Current Health
    /// </summary>
    public float Health { get; protected set; }

    /// <summary>
    /// Max Health
    /// </summary>
    public abstract float MaxHealth { get; }

    /// <summary>
    /// Event raised when this target takes any damage
    /// </summary>
    public System.Action<float, float> OnTakeDamage { get; set; }

    /// <summary>
    /// Event raised when this target is healed
    /// </summary>
    public System.Action<float, float> OnHeal { get; set; }

    /// <summary>
    /// Event raised when this target dies (healths reaches 0)
    /// </summary>
    public System.Action OnDeath { get; set; }

    /// <summary>
    /// Initializes this target
    /// </summary>
    public virtual void Initialize()
    {
        Health = MaxHealth;
    }

    /// <summary>
    /// Heals this target
    /// </summary>
    /// <param name="amount">Amount of health</param>
    public void Heal(float amount)
    {
        float previousHealth = Health;
        Health += amount;
        OnHeal?.Invoke(previousHealth, Health);
        if (Health >= MaxHealth)
        {
            Health = MaxHealth;
        }
    }

    /// <summary>
    /// Damages the target
    /// </summary>
    /// <param name="damage">Health to remove</param>
    public void TakeDamage(float damage)
    {
        float previousHealth = Health;
        Health -= damage;
        OnTakeDamage?.Invoke(previousHealth, Health);
        if (Health <= 0)
        {
            HandleDeath();
        }
    }

    protected virtual void HandleDeath()
    {
        OnDeath?.Invoke();
    }
}