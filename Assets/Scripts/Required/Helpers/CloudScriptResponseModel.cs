[FullSerializer.fsObject("cloudScriptResponseModel1")]
public struct CloudScriptResponseModel
{
    public string Message { get; set; }
    public string Error { get; set; }
}