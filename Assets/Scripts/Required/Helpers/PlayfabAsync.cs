using System.Collections.Generic;
using PlayFab;
using PlayFab.CloudScriptModels;
using PlayFab.ClientModels;
using System.Threading.Tasks;
using System;

namespace Data
{
    /// <summary>
    /// Helper class that converts Playfab's methods (that uses delegates) into
    /// task based methods
    /// </summary>
    public class PlayfabAsync
    {
        private static void ExecuteCloudFunction(string FunctionName, Dictionary<string, object> FunctionParameter, Action<ExecuteFunctionResult> callback, Action<PlayFabError> errorCallback)
        {
            var request = new ExecuteFunctionRequest()
            {
                Entity = new PlayFab.CloudScriptModels.EntityKey()
                {
                    Id = PlayFabSettings.staticPlayer.EntityId,
                    Type = PlayFabSettings.staticPlayer.EntityType
                },
                FunctionName = FunctionName,
                FunctionParameter = FunctionParameter,
                GeneratePlayStreamEvent = false
            };

            PlayFabCloudScriptAPI.ExecuteFunction(request, callback, errorCallback);
        }

        public static Task<GetUserInventoryResult> GetUserInvenotry()
        {
            var t = new TaskCompletionSource<GetUserInventoryResult>();
            PlayFabClientAPI.GetUserInventory(
                new GetUserInventoryRequest(),
                result => t.TrySetResult(result),
                error => t.TrySetException(new Exception(error.ErrorMessage))
                );

            return t.Task;
        }

        public static Task<GetUserDataResult> GetUserReadOnlyData(List<string> Keys)
        {
            var t = new TaskCompletionSource<GetUserDataResult>();
            PlayFabClientAPI.GetUserReadOnlyData(
                new GetUserDataRequest() { Keys = Keys },
                result => t.TrySetResult(result),
                error => t.TrySetException(new Exception(error.ErrorMessage))
                );

            return t.Task;
        }

        public static Task<GetCatalogItemsResult> GetCatalogItems(string CatalogVersion)
        {
            var t = new TaskCompletionSource<GetCatalogItemsResult>();

            PlayFabClientAPI.GetCatalogItems(
                new GetCatalogItemsRequest() { CatalogVersion = CatalogVersion },
                result => t.TrySetResult(result),
                error => t.TrySetException(new Exception(error.ErrorMessage))
                );

            return t.Task;
        }

        public static Task<LoginResult> LoginWithAndroidDeviceID(LoginWithAndroidDeviceIDRequest request)
        {
            var t = new TaskCompletionSource<LoginResult>();

            PlayFabClientAPI.LoginWithAndroidDeviceID(
                request,
                result => t.TrySetResult(result),
                error => t.TrySetException(new Exception(error.ErrorMessage))
                );

            return t.Task;
        }

        public static Task<UpdateUserTitleDisplayNameResult> UpdateUserTitleDisplayName(string displayName)
        {
            var t = new TaskCompletionSource<UpdateUserTitleDisplayNameResult>();

            PlayFabClientAPI.UpdateUserTitleDisplayName(
                new UpdateUserTitleDisplayNameRequest() { DisplayName = displayName },
                result => t.TrySetResult(result),
                error => t.TrySetException(new Exception(error.ErrorMessage))
                );

            return t.Task;
        }

        public static Task<GetPlayerCombinedInfoResult> GetPlayerCombinedInfo(GetPlayerCombinedInfoRequestParams requestParams)
        {
            var t = new TaskCompletionSource<GetPlayerCombinedInfoResult>();
            PlayFabClientAPI.GetPlayerCombinedInfo(
                new GetPlayerCombinedInfoRequest() { InfoRequestParameters = requestParams },
                result => t.TrySetResult(result),
                error => t.TrySetException(new Exception(error.ErrorMessage))
                );

            return t.Task;
        }

        public static Task<GetLeaderboardAroundPlayerResult> GetLeaderboardAroundPlayer(int count)
        {
            var t = new TaskCompletionSource<GetLeaderboardAroundPlayerResult>();

            PlayFabClientAPI.GetLeaderboardAroundPlayer(
                new GetLeaderboardAroundPlayerRequest()
                {
                    StatisticName = PlayfabConsts.ScoreLeaderboardId,
                    MaxResultsCount = count
                },
                result => t.TrySetResult(result),
                error => t.TrySetException(new Exception(error.ErrorMessage))
                );

            return t.Task;
        }

        public static Task<GetLeaderboardResult> GetLeaderboard(int count)
        {
            var t = new TaskCompletionSource<GetLeaderboardResult>();

            PlayFabClientAPI.GetLeaderboard(
                new GetLeaderboardRequest()
                {
                    StatisticName = PlayfabConsts.ScoreLeaderboardId,
                    MaxResultsCount = count
                },
                result => t.TrySetResult(result),
                error => t.TrySetException(new Exception(error.ErrorMessage))
                );

            return t.Task;
        }

        public static Task<ExecuteFunctionResult> ExecuteCloudFunction(string FunctionName, Dictionary<string, object> FunctionParameter)
        {
            var t = new TaskCompletionSource<ExecuteFunctionResult>();
            ExecuteCloudFunction(
                FunctionName,
                FunctionParameter,
                result =>
                {
                    try
                    {
                        var response = Helper.Deserialize<CloudScriptResponseModel>(result.FunctionResult.ToString());

                        if (response.Error != null)
                        {
                            t.TrySetException(new Exception(response.Error));
                            return;
                        }

                        t.TrySetResult(new ExecuteFunctionResult()
                        {
                            FunctionName = result.FunctionName,
                            FunctionResult = response.Message
                        });
                    }
                    catch (Exception ex)
                    {
                        t.TrySetException(ex);
                    }
                },
                error => t.TrySetException(new Exception(error.ErrorMessage))
                );

            return t.Task;
        }
    }
}