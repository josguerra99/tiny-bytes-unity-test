using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

public class SceneChanger
{
    public static ISwitchSceneAnimations Animation { get; set; }

    public static void LoadScene(string name, Action onDone)
    {
        CoroutineExecuter.Instance.StartCoroutine(_LoadScene(name, onDone));
    }

    private static IEnumerator _LoadScene(string name, Action onDone)
    {
        yield return null;

        AsyncOperation asyncLoad = SceneManager.LoadSceneAsync(name, LoadSceneMode.Single);
        while (!asyncLoad.isDone)
        {
            yield return null;
        }

        onDone?.Invoke();
    }

    /// <summary>
    /// Loads a new scene and if an animation (loading screen) is available
    /// it will use it
    /// </summary>
    /// <param name="name">Scene name to load</param>
    /// <param name="asyncOut">Lets the player control when to hide the loading screen (useful if you want to load more thing before hiding the loading screen)</param>
    public static void LoadSceneWithLoadingScreen(string name, Action<Action> asyncOut = null)
    {
        if (Animation == null)
        {
            LoadScene(name, null);
            return;
        }

        Animation.In(() =>
        {
            LoadScene(name, () =>
            {
                if (asyncOut == null)
                {
                    Animation.Out(null);
                }
                else
                {
                    asyncOut?.Invoke(() => Animation.Out(null));
                }
            });
        });
    }

    public static void LoadingOut() => Animation.Out(null);

    public interface ISwitchSceneAnimations
    {
        void In(Action callback);

        void Out(Action callback);
    }
}