using UnityEngine;

public class SwipeManager : MonoBehaviour
{
    public static SwipeManager Instance { get; private set; }

    [SerializeField] private float _swipeThreshold = 20f;

    private int _currentFingerIndex = -1;
    private Vector2 _startPosition;
    private Vector2 _finalPosition;

    public System.Action<Vector2, Vector2> OnSwipeLeft { get; set; }
    public System.Action<Vector2, Vector2> OnSwipeRight { get; set; }
    public System.Action<Vector2, Vector2> OnSwipeUp { get; set; }
    public System.Action<Vector2, Vector2> OnSwipeDown { get; set; }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(Instance);
        }
        else if (Instance != this)
        {
            Destroy(this);
        }
    }

    private void Update()
    {
#if UNITY_EDITOR

        if (Input.GetMouseButtonDown(0))
        {
            _finalPosition = Input.mousePosition;
            _startPosition = Input.mousePosition;
            _currentFingerIndex = 0;
        }

        //Detects swipe after finger is released
        if (_currentFingerIndex == 0 && Input.GetMouseButtonUp(0))
        {
            _finalPosition = Input.mousePosition;
            CheckSwipe();
        }

#else
        foreach (Touch touch in Input.touches)
        {
            if (touch.phase == TouchPhase.Began)
            {
                _finalPosition = touch.position;
                _startPosition = touch.position;
                _currentFingerIndex = touch.fingerId;
            }

            //Detects swipe after finger is released
            if (_currentFingerIndex == touch.fingerId && touch.phase == TouchPhase.Ended)
            {
                _finalPosition = touch.position;
                CheckSwipe();
            }
        }

#endif
    }

    private void CheckSwipe()
    {
        var yDelta = Mathf.Abs(_finalPosition.y - _startPosition.y);
        var xDelta = Mathf.Abs(_finalPosition.x - _startPosition.x);

        //Check if Vertical swipe
        if (yDelta > _swipeThreshold && yDelta > xDelta)
        {
            if (_finalPosition.y - _startPosition.y > 0)
            {
                SwipeUp();
            }
            else if (_finalPosition.y - _startPosition.y < 0)
            {
                SwipeDown();
            }
        }
        else if (xDelta > _swipeThreshold && xDelta > yDelta)
        {
            if (_finalPosition.x - _startPosition.x > 0)
            {
                SwipeRight();
            }
            else if (_finalPosition.x - _startPosition.x < 0)
            {
                SwipeLeft();
            }
        }

        _currentFingerIndex = -1;
    }

    private void SwipeUp()
    {
        OnSwipeUp?.Invoke(_startPosition, _finalPosition);
    }

    private void SwipeDown()
    {
        OnSwipeDown?.Invoke(_startPosition, _finalPosition);
    }

    private void SwipeLeft()
    {
        OnSwipeLeft?.Invoke(_startPosition, _finalPosition);
    }

    private void SwipeRight()
    {
        OnSwipeRight?.Invoke(_startPosition, _finalPosition);
    }
}