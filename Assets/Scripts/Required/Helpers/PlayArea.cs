using UnityEngine;

/// <summary>
/// Handles the area where the game takes place
/// </summary>
[RequireComponent(typeof(BoxCollider2D))]
public class PlayArea : MonoBehaviour
{
    /// <summary>
    /// A box the represents where is the play area
    /// </summary>
    [SerializeField] private BoxCollider2D _playArea;

    /// <summary>
    /// If true, this will scale up this gameObject to fit into
    /// the play area
    /// </summary>
    [SerializeField] private bool _scaleToScreenSize = false;

    /// <summary>
    /// How much padding should be left when using scaling the play area to the screen size
    /// </summary>
    [SerializeField, Tooltip("Values from 0 to 1")] private Vector2 _paddingPercentage;

    public static PlayArea Instance { get; private set; }

    private void Reset()
    {
        _playArea = GetComponent<BoxCollider2D>();
    }

    private void Awake()
    {
        //Creates a singleton instance for this script
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(this);
            return;
        }

        if (_playArea == null)
            _playArea = GetComponent<BoxCollider2D>();

        _playArea.isTrigger = true;

        if (_scaleToScreenSize)
            ScaleToScreenSize();
    }

    /// <summary>
    /// Scales the current area to fit inside the screen
    /// </summary>
    private void ScaleToScreenSize()
    {
        float aspectRatio = (float)Screen.width / Screen.height;
        float size = Camera.main.orthographicSize;

        float width = 2f * aspectRatio * size;
        float height = 2f * size;
        width *= (1f - _paddingPercentage.x);
        height *= (1f - _paddingPercentage.y);

        _playArea.transform.SetPositionAndRotation(Vector3.zero, Quaternion.identity);
        _playArea.transform.localScale = Vector3.one;

        _playArea.offset = Vector2.zero;
        _playArea.size = new Vector2(width, height);
    }

    /// <summary>
    /// Clamps a position to a position that is inside the play area
    /// </summary>
    /// <param name="position">Real position</param>
    /// <returns>Position inside the play area</returns>
    public Vector2 ClosestPoint(Vector2 position)
    {
        return _playArea.bounds.ClosestPoint(position);
    }

    /// <summary>
    /// Checks if a position is inside the play area or not
    /// </summary>
    /// <param name="position">Position to test</param>
    /// <returns></returns>
    public bool IsInsidePlayArea(Vector2 position)
    {
        return _playArea.bounds.Contains(position);
    }

    /// <summary>
    /// Gets the distance (squared) from a point to the play area bounds
    /// </summary>
    /// <param name="position">Position to test</param>
    /// <returns>Squared Distance</returns>
    public float SqrDistanceToBounds(Vector2 position)
    {
        return _playArea.bounds.SqrDistance(position);
    }

    /// <summary>
    /// Gets the bounds of the play area
    /// </summary>
    public Bounds Bounds => _playArea.bounds;
}