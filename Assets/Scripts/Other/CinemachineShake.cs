using Cinemachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CinemachineShake : MonoBehaviour
{
    public static CinemachineShake Instance { get; private set; }
    public CinemachineImpulseSource lightSource;
    public CinemachineImpulseSource strongSource;
    public CinemachineImpulseSource shootSource;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(this);
        }
    }

    public static void Shake(Force force)
    {
        switch (force)
        {
            case Force.Light:
                Instance.lightSource.GenerateImpulse();
                break;

            case Force.Strong:
                Instance.strongSource.GenerateImpulse();

                break;

            case Force.Shoot:
                Instance.shootSource.GenerateImpulse();

                break;

            default:
                Instance.lightSource.GenerateImpulse();
                break;
        }
    }

    public enum Force
    { Light, Strong, Shoot };
}