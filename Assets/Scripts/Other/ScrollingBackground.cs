using UnityEngine;

/// <summary>
/// Changes the uv coordinates of a material,
/// this makes an effect of an scrolling background
/// when the texture wrap mode is set to Repeat
/// </summary>
public class ScrollingBackground : MonoBehaviour
{
    [SerializeField] private Renderer _renderer;

    [SerializeField] private Vector2 _speed;

    /// <summary>
    /// Material to use (this will be obtained from the _renderer)
    /// </summary>
    private Material mat;

    /// <summary>
    /// Speed and direction of movement
    /// </summary>
    public Vector2 Speed => _speed;

    private void Awake()
    {
        if (_renderer == null)
            _renderer = GetComponent<MeshRenderer>();

        mat = _renderer.material;
    }

    private void Update()
    {
        mat.mainTextureOffset += Speed * Time.deltaTime;
    }
}