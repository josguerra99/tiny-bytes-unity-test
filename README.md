
# Not a space shooter

## Cloud Script
You can check the CloudScript project [here](https://gitlab.com/josguerra99/tiny-bytes-cloud-script)

## Download
You can download the game from [here](https://drive.google.com/file/d/1BJJyoVsYG6pGEdLV6FtCR-2FVc95bcp1/view?usp=sharing)

## Tutorial

You can look how to play the game [here](https://sand-telescope-16e.notion.site/How-to-Play-0b27da6e13934b93acfe826f13c9a8ca)


## Game architecture

Please check the explanation of the game architecture 
[here](https://sand-telescope-16e.notion.site/Explanation-of-the-game-s-arquitecture-d2b7eac086cc461a8a480d5fe9a8d496)



## Acknowledgements

 - [Spaceship and Turret Models](https://maxparata.itch.io/voxel-spaceships)
 - [Explosion VFX](https://assetstore.unity.com/packages/vfx/particles/cartoon-fx-free-109565)
 - [Music](https://www.youtube.com/watch?v=Zv1FyQn2kQA)
 - Icons and Fonts - Icons and Fonts are from Google Fonts
 - Sound Effects - I mostly used sound effects from previous projects I made.

